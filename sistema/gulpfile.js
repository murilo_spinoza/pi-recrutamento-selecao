var gulp = require('gulp');
var gulpDeploy = require('./gulp-deploy');

//Seta o deploy
var config = {
	host: '',
	port: 22,
	username: '',
	projectPath: '',
	projectName: '',
	key: ''
};

//Faz setup do deploy no servidor
gulp.task('deploy:setup', function() {
	//Instancia e executa o setup
	var deploy = new gulpDeploy(gulp, config);
	return deploy.setup();
});

//Faz o deploy para stage
gulp.task('deploy:stag', function() {
	return false;
});

//Faz o deploy para produção
gulp.task('deploy:prod', function() {
	//Seta o env e instancia
	config['env'] = 'production';
	var deploy = new gulpDeploy(gulp, config);

	//Executa
	return deploy.deploy();
});

//Faz o rollback
gulp.task('deploy:rollback', function() {
	//Seta o env e instancia
	config['env'] = 'production';
	var deploy = new gulpDeploy(gulp, config);

	//Executa
	return deploy.rollback();
});
