module.exports = {
	extends: ['stylelint-config-sass-guidelines', 'stylelint-config-concentric-order'],
	rules: {
		indentation: 3,
		'max-nesting-depth': 6,
		'selector-class-pattern': [
			'^(?:u|is|has)-[a-z][a-zA-Z0-9]*$|^(?!u|is|has)[a-zA-Z][a-zA-Z0-9]*(?:-[a-z][a-zA-Z0-9]*)*(?:__[a-z][a-zA-Z0-9]*)*(?:-[a-z][a-zA-Z0-9]*)?(?:--[a-z][a-zA-Z0-9]*)*(?:-[a-z][a-zA-Z0-9]*)?$',
			{
				message: 'O seletores devem seguir a metodologia BEM. (selector-class-pattern)'
			}
		],
		'order/properties-alphabetical-order': null,
		'order/order': [
			{
				type: 'at-rule',
				name: 'include'
			},
			'declarations'
		]
	}
};
