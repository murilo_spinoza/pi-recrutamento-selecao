export const environment = {
	production: false,
	token : 'token',
	idKey : 'user_id',
	api : 'http://localhost:4000',
	title: 'Recrutamento e Seleção',
	email: 'contato@contato.com.br',
	name: 'Recrutamento e Seleção'
};
