export const environment = {
	production: true,
	token: 'token',
	idKey: 'user_id',
	api: 'http://recrutamentoselecao.conquistapioneira.com.br:4000',
	title: 'Recrutamento e Seleção',
	email: 'contato@contato.com.br',
	name: 'Recrutamento e Seleção'
};
