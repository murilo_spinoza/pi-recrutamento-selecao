import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt, 'pt');

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthModule } from './@theme/components/auth/auth.module';
import { AuthGuard } from './@theme/components/auth/auth-guard.service';
import { APIModule } from '@core/api/api.module';
import { ToasterModule, ToasterService } from 'angular2-toaster';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
	align: 'left',
	allowNegative: false,
	decimal: ',',
	precision: 2,
	prefix: 'R$ ',
	suffix: '',
	thousands: '.'
};

@NgModule({
	declarations: [AppComponent],
	imports: [
		ToasterModule,
		AuthModule,
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		HttpClientModule,
		CoreModule,
		APIModule,
		NgbModule.forRoot(),
		ThemeModule.forRoot(),
	],
	exports: [HttpClientModule],
	providers: [
		AuthGuard,
		ToasterService,
		{ provide: APP_BASE_HREF, useValue: '/' },
		{ provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
		{ provide: LOCALE_ID, useValue: 'pt_BR' }
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
