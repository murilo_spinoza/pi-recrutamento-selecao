import { NgModule } from '@angular/core';

import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';

import { PagesComponent } from './pages.component';
import { TypographyComponent } from './typography/typography.component';

const PAGES_COMPONENTS = [
	PagesComponent,
	TypographyComponent
];

@NgModule({
	imports: [
		PagesRoutingModule,
		ThemeModule,
		DashboardModule,
		MiscellaneousModule,
	],
	declarations: [
		...PAGES_COMPONENTS,
	],
})
export class PagesModule {}
