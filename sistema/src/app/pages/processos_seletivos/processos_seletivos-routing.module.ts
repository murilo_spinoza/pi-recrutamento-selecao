import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessoComponent } from './processo/processo.component';
import { ProcessosListaComponent } from './lista/processos-lista.component';

const routes: Routes = [
	{ path: '', component: ProcessosListaComponent },
	{ path: 'lista', component: ProcessosListaComponent },
	{ path: 'processo/:contratacaoId', component: ProcessoComponent },
	{ path: 'processo/:contratacaoId/:etapa', component: ProcessoComponent }
];

export const ProcessosSeletivosRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
