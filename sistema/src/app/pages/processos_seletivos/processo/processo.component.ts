import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContratacoesAPI } from '@core/api/contratacoes.api';
import { ProcessosSeletivosAPI } from '@core/api/processos_seletivos.api';
import { MyToasterService } from '@core/services/my-toaster.service';
import { Location } from '@angular/common';
import { BrMaskService } from '@theme/services/br-mask.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContratacoesModalComponent } from '../../contratacoes/modal/contratacoes-modal.component';
import { CandidatosModalComponent } from 'app/pages/candidatos/modal/candidatos-modal.component';
import { ICandidatoPontos } from '@core/interfaces/candidatos.interfaces';
import { CandidatosAnotacoesComponent } from 'app/pages/candidatos/anotacoes/candidatos-anotacoes.component';

@Component({
	selector: 'app-processo',
	templateUrl: './processo.component.html',
	styleUrls: ['./processo.component.scss']
})
export class ProcessoComponent implements OnInit {
	public etapas = [
		{ Nome: 'Triagem', Alias: 'triagem' },
		{ Nome: 'Testes', Alias: 'testes' },
		{ Nome: 'Entrevistas', Alias: 'entrevistas' },
		{ Nome: 'Aprovados', Alias: 'aprovados' }
	];

	public contratacaoId: number;
	public etapa: string;
	public carregando = true;
	public contratacao;
	public testes = [];
	public mediaTestes;

	public tipoRequisitoOpcoes: { [key: string]: string } = {};

	public candidatos = [];

	constructor(
		private __activatedRoute: ActivatedRoute,
		private __location: Location,
		private __contratacaoAPI: ContratacoesAPI,
		private __processosSeletivosAPI: ProcessosSeletivosAPI,
		private __toaster: MyToasterService,
		private __router: Router,
		private __brMask: BrMaskService,
		private __modal: NgbModal,
		private __myToaster: MyToasterService
	) {
		this.contratacaoId = +this.__activatedRoute.snapshot.params['contratacaoId'];
		if (!this.__activatedRoute.snapshot.params['etapa']) {
			this.etapa = 'triagem';
			this.__location.go('/paginas/processos_seletivos/processo/' + this.contratacaoId + '/' + this.etapa);
		} else {
			this.etapa = this.__activatedRoute.snapshot.params['etapa'];
		}
	}

	ngOnInit() {
		if (!this.contratacaoId || typeof this.contratacaoId !== 'number') {
			return this.__router.navigate(['paginas', 'contratacoes']);
		}

		this.__init();
	}

	private async __init() {
		try {
			const dados = await this.__contratacaoAPI.setForm(this.contratacaoId, 'visualizar');
			console.log('dados', dados);
			this.tipoRequisitoOpcoes = dados.TipoRequisitoOpcoes;
			dados.Contratacao.Salario = this.__brMask.create_money(dados.Contratacao.Salario);
			this.contratacao = dados.Contratacao;
			this.testes = dados.TestesOpcoes;
			this.mediaTestes = dados.MediaTestes;
			this.__buscaCandidatos();
		} catch (err) {
			this.__toaster.showErrorToast('Processo seletivo não buscado', err && err.message ? err.message : err);
		}
		this.carregando = false;
	}

	private async __buscaCandidatos() {
		switch (this.etapa) {
			case 'triagem':
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosTriagem(this.contratacaoId);
				break;
			case 'testes':
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosTestes(this.contratacaoId);
				break;
			case 'entrevistas':
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosEntrevistas(this.contratacaoId);
				break;
			case 'aprovados':
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosAprovados(this.contratacaoId);
				break;
		}
	}

	public async alteraEtapa(dados: { indice: number; alias: string }) {
		this.carregando = true;
		this.etapa = dados.alias;
		this.__location.go('/paginas/processos_seletivos/processo/' + this.contratacaoId + '/' + this.etapa);
		await this.__buscaCandidatos();
		this.carregando = false;
	}

	public async notaFase(fase: 'Testes' | 'Entrevistas', candidato: any) {
		this.carregando = true;
		try {
			let nota: any;
			while (!nota || typeof +nota !== 'number' || +nota < 0 || +nota > 10) {
				nota = prompt('Informe a nota (0 a 10) de ' + candidato.Nome);
				if (nota === null) {
					return;
				}
			}

			await this.__processosSeletivosAPI.notaFase(fase, this.contratacaoId, candidato.CandidatoId, nota);
			await this.__buscaCandidatos();
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao salvar a Análise', err);
		}
		this.carregando = false;
	}

	public visualizar() {
		const activeModal = this.__modal.open(ContratacoesModalComponent, { size: 'lg', container: 'nb-layout' });

		activeModal.componentInstance.contratacaoId = this.contratacaoId;
	}

	public curriculo(candidatoId: number, pontos: ICandidatoPontos) {
		const activeModal = this.__modal.open(CandidatosModalComponent, { size: 'lg', container: 'nb-layout' });
		activeModal.componentInstance.candidatoId = candidatoId;
		activeModal.componentInstance.pontos = pontos;
	}

	public anotacoes(candidatoId: number) {
		const activeModal = this.__modal.open(CandidatosAnotacoesComponent, { size: 'lg', container: 'nb-layout' });
		activeModal.componentInstance.candidatoId = candidatoId;
	}

	public async analisaTriagem(aprovada: boolean, candidato: any) {
		if (candidato.Analise) {
			return;
		}

		this.carregando = true;
		try {
			if (confirm('Você está prestes a ' + (aprovada ? 'aprovar' : 'reprovar') + ' "' + candidato.Nome + '"')) {
				let reprova: string;
				if (!aprovada) {
					while (!reprova) {
						reprova = prompt('Justifique a reprova');
						if (reprova === null) {
							return;
						}
					}
				}
				await this.__processosSeletivosAPI.analisaFase('Triagem', this.contratacaoId, candidato.CandidatoId, reprova);
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosTriagem(this.contratacaoId);
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao salvar a Análise', err);
		}
		this.carregando = false;
	}

	public async analisaTestes(aprovada: boolean, candidato: any) {
		if (candidato.Analise) {
			return;
		}

		this.carregando = true;
		try {
			if (confirm('Você está prestes a ' + (aprovada ? 'aprovar' : 'reprovar') + ' "' + candidato.Nome + '"')) {
				let reprova: string;
				if (!aprovada) {
					while (!reprova) {
						reprova = prompt('Justifique a reprova');
						if (reprova === null) {
							return;
						}
					}
				}
				await this.__processosSeletivosAPI.analisaFase('Testes', this.contratacaoId, candidato.CandidatoId, reprova);
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosTestes(this.contratacaoId);
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao salvar a Análise', err);
		}
		this.carregando = false;
	}

	public async analisaEntrevistas(aprovada: boolean, candidato: any) {
		if (candidato.Analise) {
			return;
		}

		this.carregando = true;
		try {
			if (confirm('Você está prestes a ' + (aprovada ? 'aprovar' : 'reprovar') + ' "' + candidato.Nome + '"')) {
				let reprova: string;
				if (!aprovada) {
					while (!reprova) {
						reprova = prompt('Justifique a reprova');
						if (reprova === null) {
							return;
						}
					}
				}
				await this.__processosSeletivosAPI.analisaFase('Entrevistas', this.contratacaoId, candidato.CandidatoId, reprova);
				this.candidatos = await this.__processosSeletivosAPI.contratacaoCandidatosEntrevistas(this.contratacaoId);
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao salvar a Análise', err);
		}
		this.carregando = false;
	}

	public async concluiTriagem() {
		this.carregando = true;
		try {
			if (
				confirm(
					'Atenção!\nOs candidatos não poderão se candidatar e os que estão na triagem, que não foram analisados, serão reprovados.'
				)
			) {
				const dados = await this.__processosSeletivosAPI.concluiFase('Triagem', this.contratacaoId);

				let etapa = 'testes';

				if (dados.Finalizado) {
					etapa = 'aprovados';
					this.contratacao.Status = 'Finalizado';
				}

				this.contratacao.Etapa = etapa;

				this.alteraEtapa({ indice: 1, alias: etapa });
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao concluir a triagem', err);
		}
		this.carregando = false;
	}

	public async concluiTestes() {
		this.carregando = true;
		try {
			if (confirm('Atenção!\nOs candidatos que estão na fase de testes, que não foram analisados, serão reprovados.')) {
				const dados = await this.__processosSeletivosAPI.concluiFase('Testes', this.contratacaoId);

				let etapa = 'entrevistas';

				if (dados.Finalizado) {
					etapa = 'aprovados';
					this.contratacao.Status = 'Finalizado';
				}

				this.contratacao.Etapa = etapa;

				this.alteraEtapa({ indice: 1, alias: etapa });
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao concluir a fase de testes', err);
		}
		this.carregando = false;
	}

	public async concluiEntrevistas() {
		this.carregando = true;
		try {
			if (confirm('Atenção!\nOs candidatos que estão na fase de entrevistas, que não foram analisados, serão reprovados.')) {
				const dados = await this.__processosSeletivosAPI.concluiFase('Entrevistas', this.contratacaoId);

				const etapa = 'aprovados';

				if (dados.Finalizado) {
					this.contratacao.Status = 'Finalizado';
				}

				this.contratacao.Etapa = etapa;

				this.alteraEtapa({ indice: 1, alias: etapa });
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao concluir a fase de entrevistas', err);
		}
		this.carregando = false;
	}

	public async concluiAprovados() {
		this.carregando = true;
		try {
			if (confirm('Atenção!\nCancele se houver candidatos não contatados.')) {
				await this.__processosSeletivosAPI.concluiFase('Aprovados', this.contratacaoId);

				this.contratacao.Status = 'Finalizado';
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao concluir a fase de aprovados', err);
		}
		this.carregando = false;
	}
}
