import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';
import { Subscription } from 'rxjs';
import { DatagridEventEmitter } from '@core/services/datagrid-event-emitter';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ButtonRenderComponent } from '@theme/components/datagrid/render-components/button-render/button-render.component';
import { Router } from '@angular/router';

@Component({
	selector: 'app-processos-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class ProcessosListaComponent extends BaseListComponent implements OnInit, OnDestroy {
	public routine = 'processos_seletivos';
	public primaryKey = 'ContratacaoId';

	public canDelete = false;
	public formPage = [];

	public tableColumns = {
		ContratacaoId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		Cargo: {
			title: 'Cargo',
			type: 'string',
			filter: {}
		},
		Contratacoes: {
			title: 'Contratações',
			type: 'string',
			filter: false
		},
		Etapa: {
			title: 'Etapa',
			type: 'string',
			filter: {
				type: 'list',
				config: {
					selectText: '--Selecione--',
					list: [
						{ value: 'triagem', title: 'Triagem' },
						{ value: 'testes', title: 'Testes' },
						{ value: 'entrevistas', title: 'Entrevistas' },
						{ value: 'aprovados', title: 'Aprovados' }
					]
				}
			}
		},
		Status: {
			title: 'Status',
			type: 'string',
			filter: {
				type: 'list',
				config: {
					selectText: '--Selecione--',
					list: [{ value: 'E', title: 'Em andamento' }, { value: 'F', title: 'Finalizado' }]
				}
			}
		},
		Acompanhar: {
			title: 'Acompanhar',
			type: 'custom',
			class: 'custom-button text-center',
			renderComponent: ButtonRenderComponent,
			defaultValue: {
				Icon: 'ion-eye',
				Class: 'primary',
				Text: '',
				Action: 'Acompanhar'
			},
			filter: false,
			sort: false
		}
	};

	private __optionClick: Subscription;

	constructor(public datagridEventEmitter: DatagridEventEmitter, private __router: Router, protected _injector: Injector) {
		super(_injector);
	}

	async _init() {
		this.__optionClick = this.datagridEventEmitter.click.subscribe(async (contratacao: any) => {
			switch (contratacao.renderAction) {
				case 'Acompanhar':
					this.exibeCandidato(contratacao);
					break;
			}
		});
	}

	ngOnDestroy() {
		this.__optionClick.unsubscribe();
	}

	exibeCandidato(contratacao: any) {
		this.__router.navigate(['paginas', 'processos_seletivos', 'processo', contratacao.ContratacaoId, contratacao.Etapa.toLowerCase()]);
	}
}
