import { NgModule } from '@angular/core';
import { ProcessoComponent } from './processo/processo.component';
import { ProcessosSeletivosRoutingModule } from './processos_seletivos-routing.module';
import { ThemeModule } from '@theme/theme.module';
import { ContratacoesModalModule } from '../contratacoes/modal/contratacoes-modal.module';
import { ProcessosListaComponent } from './lista/processos-lista.component';
import { CandidatosModalModule } from '../candidatos/modal/candidatos-modal.module';
import { CandidatosAnotacoesModule } from '../candidatos/anotacoes/candidatos-anotacoes.module';

@NgModule({
	imports: [ProcessosSeletivosRoutingModule, ContratacoesModalModule, CandidatosModalModule, CandidatosAnotacoesModule, ThemeModule],
	declarations: [ProcessoComponent, ProcessosListaComponent]
})
export class ProcessosSeletivosModule {}
