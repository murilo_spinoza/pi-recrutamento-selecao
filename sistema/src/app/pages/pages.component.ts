import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbMenuService, NbSidebarService } from '@nebular/theme';
import { RotinasService, RotinasDelegate } from '../@core/services/rotinas.service';
import { MyToasterService } from '../@core/services/my-toaster.service';
import { environment } from '../../environments/environment';

@Component({
	selector: 'app-pages',
	templateUrl: './pages.component.html'
})
export class PagesComponent implements OnInit, RotinasDelegate {

	public menu: NbMenuItem[] = [];
	public loading = true;

	constructor(
		protected rotinasService: RotinasService,
		public sidebarService: NbSidebarService,
		public menuService: NbMenuService,
		public myToaster: MyToasterService
	) {

		menuService.onItemSelect().subscribe(() => {
			if (window.innerWidth < 992) {
				sidebarService.collapse('menu-sidebar');
			}
		});
	}

	async ngOnInit() {
		this.rotinasService.delegate = this;
		await this.rotinasService.getMenu();
	}

	public async onMenuChange() {
		try {
			// Quando tiver necessidade de ter um item de tipografia
			const necessario = false;
			if (!environment.production && necessario) {
				this.menu = [
					{
						title: 'Home',
						icon: 'nb-home',
						link: '/paginas/dashboard'
					},
					{
						title: 'DEVELOPER',
						group: true,
					},
					{
						title: 'Tipografia',
						icon: 'ion-social-sass',
						link: '/paginas/typography'
					},
					{
						title: 'RECURSOS',
						group: true,
					}
				];
			} else { // PRODUÇÃO
				this.menu = [
					{
						title: 'Home',
						icon: 'nb-home',
						link: '/paginas/dashboard'
					}
				];
			}

			// Busca o menu disponível para o usuário logado
			await this.rotinasService.buscaRealizada;

			// Inicia o objeto das classificacoes
			const rotinasClassificacoes: any = this.rotinasService.rotinasClassificacoes;
			// Inicia o objeto das rotinas
			const rotinas = this.rotinasService.rotinas;

			// Para cada id de classificacao
			for (const classificacaoId of this.rotinasService.rotinasClassificacoesOrdem) {
				// Recebe os valores do registro
				const classificacao = rotinasClassificacoes[classificacaoId];

				// Monta um novo grupo de menu com a classificação
				const novoGrupo: NbMenuItem = {
					title: classificacao.Classificacao,
					icon: classificacao.Icone,
					children: []
				};

				// Percorre as rotinas da classificacao
				for (const rotina of classificacao['Rotinas']) {

					// Só adiciona ao menu se for do tipo 'ApareceMenu' e tiver permissão para consulta
					if (rotinas[rotina].ApareceMenu === 'S' && this.rotinasService.verificaPermissao(rotina, 'Consulta')) {

						// Adiciona o item ao grupo
						novoGrupo.children.push({
							title: rotinas[rotina].Rotina,
							link: '/paginas/' + rotinas[rotina].Link
						});
					}
				}

				// Se o grupo tiver pelo menos um item
				if (novoGrupo.children.length) {
					// É incluido no menu
					this.menu.push(novoGrupo);
				}
			}

			// Adiciona items extras no menu
			// this.menu = Object.assign([], this.menu, this.rotinasService.items);
			this.menu = this.menu.concat(this.rotinasService.items);

			this.loading = false;
		} catch (err) {
			console.error(err);
			this.myToaster.showErrorToast('Menu não carregado', err);
		}
	}
}
