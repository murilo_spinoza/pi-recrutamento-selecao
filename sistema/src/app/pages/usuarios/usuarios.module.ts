import { NgModule } from '@angular/core';
import { UsuariosComponent } from './usuarios.component';
import { UsuariosFormComponent } from './form/usuarios-form.component';
import { UsuariosListaComponent } from './lista/usuarios-lista.component';
import { UsuariosRoutingModule } from './usuarios-routing.module';
// import { TextMaskModule } from 'angular2-text-mask';// https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
	imports: [
		UsuariosRoutingModule,
		// TextMaskModule,
		ThemeModule
	],
	declarations: [
		UsuariosComponent,
		UsuariosFormComponent,
		UsuariosListaComponent
	]
})

export class UsuariosModule { }
