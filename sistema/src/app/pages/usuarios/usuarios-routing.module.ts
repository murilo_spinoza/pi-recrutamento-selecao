import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosComponent } from './usuarios.component';
import { UsuariosFormComponent } from './form/usuarios-form.component';
import { UsuariosListaComponent } from './lista/usuarios-lista.component';

const routes: Routes = [{
    path: '',
    component: UsuariosComponent,
    children: [
        {
            path: 'form/:id',
            component: UsuariosFormComponent
        },
        {
            path: 'form',
            component: UsuariosFormComponent
        },
        {
            path: 'lista',
            component: UsuariosListaComponent
        },
        {
            path: '',
            redirectTo: 'lista',
            pathMatch: 'full'
        }
    ]
}];

export const UsuariosRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
