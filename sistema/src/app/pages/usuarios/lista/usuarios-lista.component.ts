import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-usuarios-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class UsuariosListaComponent extends BaseListComponent {
	public routine = 'usuarios';
	public primaryKey = 'UsuarioId';
	public tableColumns = {
		UsuarioId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},

		Nome: {
			title: 'Nome',
			type: 'string',
			filter: {}
		},

		Email: {
			title: 'E-mail',
			type: 'string',
			filter: {}
		},

		Grupo: {
			title: 'Grupo',
			type: 'string',
			filter: {}
		},

		Cargo: {
			title: 'Cargo',
			type: 'string',
			filter: {}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
