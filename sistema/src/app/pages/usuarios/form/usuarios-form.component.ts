import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsuariosAPI } from '@core/api/usuarios.api';
import { SessionStorageService } from '@core/services/session-storage.service';

import { BaseFormComponent } from '@theme/components/base/base-form.component';
import { BrMaskService } from '@theme/services/br-mask.service';

@Component({
	selector: 'app-usuarios-form',
	templateUrl: './usuarios-form.component.html',
	styleUrls: ['./usuarios-form.component.scss']
})
export class UsuariosFormComponent extends BaseFormComponent {
	// Verifica se algum user logado
	public userLogado = false;

	// Variaveis para armazenamento dos textos de selects
	public ativoOpcoes: any = [];
	public usuarioGrupoOpcoes: any = [];
	public cargoOpcoes: any = [];

	constructor(
		protected _activatedRoute: ActivatedRoute,
		private __usuariosAPI: UsuariosAPI,
		private __session: SessionStorageService,
		public brMaskService: BrMaskService,
		protected _injector: Injector
	) {
		super(_injector, _activatedRoute, 'usuarios');

		// verifica se o user cadastrado é o mesmo do formulário
		this.userLogado = this.__session.getUserId() === +(this._activatedRoute.snapshot.params['id'] || 0);

		this.form = this._formBuilder.group({
			id: [],
			UsuarioGrupoId: [null, [Validators.required]],
			CargoId: [null],
			Nome: [null, [Validators.required, Validators.maxLength(80)]],
			Telefone: [null, [Validators.required, Validators.maxLength(20)]],
			Email: [null, [Validators.required, Validators.maxLength(70), Validators.email]],
			Senha: [],
			SenhaAtual: [],
			ConfirmaSenha: [],
			Ativo: [null, [Validators.required]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async _init() {
		// Faz uma requisição à API para preencher o formulário
		const data = await this.__usuariosAPI.setForm(this.form.value.id);

		// Recebe as opções dos campos de select
		this.ativoOpcoes = data.AtivoOpcoes;
		this.usuarioGrupoOpcoes = data.UsuarioGrupoOpcoes;
		this.cargoOpcoes = data.CargoOpcoes;

		// Recebe os dados do registro
		this._baseRegisterInit(data.Usuario);

		// Formata o número de telefone
		this.form.controls.Telefone.setValue(this.brMaskService.create_phone(this.form.value.Telefone));
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Valida senha
		if (this.form.value.ConfirmaSenha !== this.form.value.Senha && (this.form.value.ConfirmaSenha || this.form.value.Senha)) {
			throw 'As senhas não coincidem';
		}

		// Recebe os dados do formulário
		const value = this.form.value;

		// Lista de campos com máscara
		const camposMascarados = ['Telefone'];

		// Regex para retirar a máscara dos campos
		for (const campo of camposMascarados) {
			value[campo] = value[campo]
				.toString()
				.replace(/\(|\)|\s|\/|\.|\-/g, '')
				.trim();
		}

		// Chama a API para salvar os dados
		const data = await this.__usuariosAPI.save(value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
