import { Component } from '@angular/core';

@Component({
	selector: 'app-quadro-vagas',
	template: '<router-outlet></router-outlet>'
})
export class QuadroVagasComponent {

	constructor() { }
}
