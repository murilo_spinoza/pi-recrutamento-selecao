import { NgModule } from '@angular/core';
import { QuadroVagasComponent } from './quadro_vagas.component';
import { QuadroVagasFormComponent } from './form/quadro_vagas-form.component';
import { QuadroVagasListaComponent } from './lista/quadro_vagas-lista.component';
import { QuadroVagasRoutingModule } from './quadro_vagas-routing.module';
import { ThemeModule } from '@theme/theme.module';

@NgModule({
	imports: [
		QuadroVagasRoutingModule,
		ThemeModule
	],
	declarations: [
		QuadroVagasComponent,
		QuadroVagasFormComponent,
		QuadroVagasListaComponent
	]
})

export class QuadroVagasModule { }
