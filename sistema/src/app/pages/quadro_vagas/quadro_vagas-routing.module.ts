import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuadroVagasComponent } from './quadro_vagas.component';
import { QuadroVagasFormComponent } from './form/quadro_vagas-form.component';
import { QuadroVagasListaComponent } from './lista/quadro_vagas-lista.component';

const routes: Routes = [{
	path: '',
	component: QuadroVagasComponent,
	children: [
		{
			path: 'form/:id',
			component: QuadroVagasFormComponent
		},
		{
			path: 'form',
			component: QuadroVagasFormComponent
		},
		{
			path: 'lista',
			component: QuadroVagasListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const QuadroVagasRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
