import { Component, OnInit, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-quadro-vagas-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class QuadroVagasListaComponent extends BaseListComponent implements OnInit {
	public routine = 'quadro_vagas';
	public primaryKey = 'QuadroVagaId';
	public tableColumns = {
		QuadroVagaId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		Cargo: {
			title: 'Cargo',
			type: 'string',
			filter: {}
		},
		Saldo: {
			title: 'Saldo de vagas',
			type: 'html',
			filter: false,
			valuePrepareFunction: (saldo, row) => {
				const cor = saldo > 0 ? '#090' : saldo < 0 ? '#900' : '#181818';
				const ocupadas = row.Vagas - saldo + (row.Vagas - saldo > 1 ? ' vagas ocupadas' : ' vaga ocupada');
				return `<font color="${cor}"><span width="40px">${saldo}</span>&nbsp;&nbsp;&nbsp; <small>(${ocupadas} de ${
					row.Vagas
				})</small></font>`;
			}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
