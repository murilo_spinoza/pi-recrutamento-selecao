import { Component, ViewChild, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseFormComponent, ICampoOpcoes } from '@theme/components/base/base-form.component';
import { BrMaskService } from '@theme/services/br-mask.service';
import { QuadroVagasAPI } from '@core/api/quadro_vagas.api';

export interface IHabilidadesEscolhidas extends ICampoOpcoes {
	Nivel?: number;
	TipoRequisito?: string;
}

@Component({
	selector: 'app-quadro-vagas-form',
	templateUrl: './quadro_vagas-form.component.html',
	styleUrls: ['./quadro_vagas-form.component.scss']
})
export class QuadroVagasFormComponent extends BaseFormComponent {
	public tipoRequisitoOpcoes: Array<ICampoOpcoes> = [];
	public cargosOpcoes: Array<ICampoOpcoes> = [];
	public habilidadesOpcoes: Array<ICampoOpcoes> = [];

	public habilidadesEscolhidas: Array<IHabilidadesEscolhidas> = [];

	@ViewChild('selectHabilidades') selectHabilidades: any;

	constructor(
		protected _activatedRoute: ActivatedRoute,
		private __quadroVagasAPI: QuadroVagasAPI,
		private __brMaskService: BrMaskService,
		protected _injector: Injector
	) {
		super(_injector, _activatedRoute, 'quadro_vagas');

		this.form = this._formBuilder.group({
			id: [],
			CargoId: [null, [Validators.required]],
			Descricao: [null, [Validators.required, Validators.maxLength(500)]],
			Salario: [null, [Validators.required]],
			Vagas: [null, [Validators.required]],
			// Saldo: [null],
			Habilidades: this._formBuilder.group({})
		});
	}

	public async _init() {
		// Faz uma requisição à API para preencher o formulário
		const data = await this.__quadroVagasAPI.setForm(this.form.value.id);

		// Recebe as opções dos campos de select
		this.tipoRequisitoOpcoes = data.TipoRequisitoOpcoes;

		// Recebe as listas de opções
		this.habilidadesOpcoes = data.HabilidadesOpcoes;
		this.cargosOpcoes = data.CargosOpcoes;

		this.habilidadesEscolhidas = [];

		for (const i of data.Habilidades) {
			this.habilidadesEscolhidas.push(this.habilidadesOpcoes[i]);
		}

		this.selectHabilidades.writeValue(this.habilidadesEscolhidas);

		this.gera_habilidades();

		// Recebe os dados do registro
		this._baseRegisterInit(data.QuadroVaga);

		// Formata o salário
		this.form.controls.Salario.setValue(this.__brMaskService.create_money(this.form.value.Salario));
	}

	/**
	 * gera_habilidades
	 *
	 * Método responsável por converter as habilidades selecionadas em valores do form
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 04/2019
	 * @param object opcional contendo uma opção clicada
	 * @param number opcional com o nivel selecionado
	 * @return void
	 */
	public gera_habilidades(item?: any, nivel?: any, tipoRequisito?: any) {
		// Verifica se algum item vai ser atualizado
		if (item) {
			// Atualiza o valor do item
			item.Nivel = nivel;

			if (tipoRequisito && tipoRequisito.target && tipoRequisito.target.value) {
				item.TipoRequisito = tipoRequisito.target.value;
			}
		}

		// Variável para conter os valores das habilidades
		const habilidades: { [key: number]: any } = {};

		// Percorre as habilidades escolhidas
		for (const habilidade of this.habilidadesEscolhidas) {
			// Cria um formControl para o id da habilidade
			habilidades[habilidade.key] = [{ Nivel: habilidade.Nivel || 1, TipoRequisito: habilidade.TipoRequisito || 'P' }];
		}

		// Atualiza o formGroup de Habilidades
		this.form.setControl('Habilidades', this._formBuilder.group(habilidades));
	}

	/**
	 * _save
	 *
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	protected async _save() {
		// Recebe os dados do formulário
		const value = this.form.value;

		// Limpa os pontos de milhar
		value.Salario = value.Salario.toString()
			.replace(/\(|\)|\s|\/|\.|\-/g, '')
			.trim();
		value.Salario = +value.Salario.toString().replace(',', '.');

		// Percorre os campos com máscara
		for (const campo of []) {
			// Regex para retirar a máscara dos campos
			value[campo] = value[campo]
				.toString()
				.replace(/\(|\)|\s|\/|\.|\-/g, '')
				.trim();
		}

		let data: any;

		try {
			// Chama a API para salvar os dados
			data = await this.__quadroVagasAPI.save(value);
		} catch (error) {
			if (error.CargoId && (error.CargoId as string).indexOf('já existe em nosso banco') >= 0) {
				error.CargoId = 'Já existe uma vaga cadastrada para este cargo';
			}

			throw error;
		}

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}

	public newRegister() {
		super.newRegister();

		this.habilidadesEscolhidas = [];
		this.selectHabilidades.writeValue(this.habilidadesEscolhidas);
		this.gera_habilidades();
	}

	/**
	 * atualizaHabilidades
	 *
	 * Método responsável por atualizar o valor das habilidades
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	public atualizaHabilidades(habilidades: Array<{ key: number; value: string; Nivel?: number }>) {
		// Recebe a lista de habilidades escolhidas
		this.habilidadesEscolhidas = habilidades;

		// Percorre as habilidades
		for (const habilidade of this.habilidadesEscolhidas) {
			// Se não tiver a chave de Nivel
			if (!habilidade.Nivel) {
				// Cria com o valor inicial
				habilidade.Nivel = 1;
			}
		}

		this.gera_habilidades();
	}
}
