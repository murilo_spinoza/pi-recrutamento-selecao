import { Component, ViewChild, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent, ICampoOpcoes } from '@theme/components/base/base-form.component';
import { BrMaskService } from '@theme/services/br-mask.service';
import { QuadroVagasAPI } from '@core/api/quadro_vagas.api';
import { ContratacoesAPI } from '@core/api/contratacoes.api';

export interface IHabilidadesEscolhidas extends ICampoOpcoes {
	Nivel?: number;
	TipoRequisito?: string;
}

export interface ITestesEscolhidos extends ICampoOpcoes {
	MinimoPontos?: number;
}

@Component({
	selector: 'app-contratacoes-form',
	templateUrl: './contratacoes-form.component.html',
	styleUrls: ['./contratacoes-form.component.scss']
})
export class ContratacoesFormComponent extends BaseFormComponent {
	public pcdOpcoes: Array<ICampoOpcoes> = [];
	public quadroVagaOpcoes: Array<ICampoOpcoes> = [];
	public cursosOpcoes: Array<ICampoOpcoes> = [];
	public testesOpcoes: Array<ICampoOpcoes> = [];
	public statusOpcoes: Array<ICampoOpcoes> = [];
	public habilidadesOpcoes: Array<ICampoOpcoes> = [];
	public tipoRequisitoOpcoes: Array<ICampoOpcoes> = [];

	public habilidadesEscolhidas: Array<IHabilidadesEscolhidas> = [];
	public testesEscolhidos: Array<ITestesEscolhidos> = [];

	@ViewChild('selectHabilidades') selectHabilidades: any;
	@ViewChild('selectTestes') selectTestes: any;

	constructor(
		protected _activatedRoute: ActivatedRoute,
		private __contratacoesAPI: ContratacoesAPI,
		private __quadroVagasAPI: QuadroVagasAPI,
		private __brMaskService: BrMaskService,
		private __router: Router,
		protected _injector: Injector
	) {
		super(_injector, _activatedRoute, 'contratacoes');

		this.form = this._formBuilder.group({
			id: [],
			QuadroVagaId: [null, [Validators.required]],
			CargoId: [null, [Validators.required]],
			UsuarioSolicitanteId: [null],
			UsuarioAvaliadorId: [null],
			Descricao: [null, [Validators.required, Validators.maxLength(500)]],
			Motivo: [null, [Validators.maxLength(250)]],
			Rejeicao: [null, [Validators.maxLength(250)]],
			Salario: [null, [Validators.required]],
			Contratacoes: [null, [Validators.required]],
			PontosTriagem: [0],
			Pcd: ['N', [Validators.required]],
			Status: [null, [Validators.required]],

			Habilidades: this._formBuilder.group({}),
			Escolaridades: [],
			Testes: this._formBuilder.group({})
		});
	}

	protected async _init() {
		// Faz uma requisição à API para preencher o formulário
		const data = await this.__contratacoesAPI.setForm(this.form.value.id);

		if (data.Contratacao && this.form.value.id && data.Contratacao.Status !== 'P' && data.Contratacao.Status !== 'R') {
			this.__router.navigate(['paginas', 'contratacoes']);
			this._myToaster.showErrorToast('Edição não permitida', 'O fluxo desta contratação já foi iniciado');
			return;
		}

		// Recebe as opções dos campos de select
		this.tipoRequisitoOpcoes = data.TipoRequisitoOpcoes;

		// Recebe as listas de opções
		this.pcdOpcoes = data.PcdOpcoes;
		this.quadroVagaOpcoes = data.QuadroVagaOpcoes;
		this.statusOpcoes = data.StatusOpcoes;
		this.cursosOpcoes = data.CursosOpcoes;
		this.testesOpcoes = data.TestesOpcoes;

		this.testesEscolhidos = [];

		for (const i of data.Testes) {
			this.testesEscolhidos.push(this.testesOpcoes[i]);
		}

		this.selectTestes.writeValue(this.testesEscolhidos);

		this.gera_testes();

		this.__setForm(data, data.Contratacao);
	}

	private __setForm(data, register) {
		this.habilidadesOpcoes = data.HabilidadesOpcoes;

		this.habilidadesEscolhidas = [];

		for (const i of data.Habilidades) {
			this.habilidadesEscolhidas.push(this.habilidadesOpcoes[i]);
		}

		this.selectHabilidades.writeValue(this.habilidadesEscolhidas);

		this.gera_habilidades();

		// Recebe os dados do registro
		this._baseRegisterInit(register);

		// Formata o salário
		this.form.controls.Salario.setValue(this.__brMaskService.create_money(this.form.value.Salario));
	}

	public async quadroVagaSelecionado(event: number) {
		for (const quadroVaga of this.quadroVagaOpcoes) {
			if (+quadroVaga.key === +event) {
				this.form.controls.CargoId.setValue(quadroVaga.value);
			}
		}

		const data = await this.__quadroVagasAPI.setForm(event);

		this.__setForm(data, data.QuadroVaga);
	}

	/**
	 * gera_habilidades
	 *
	 * Método responsável por converter as habilidades selecionadas em valores do form
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 04/2019
	 * @param object opcional contendo uma opção clicada
	 * @param number opcional com o nivel selecionado
	 * @return void
	 */
	public gera_habilidades(item?: any, nivel?: any, tipoRequisito?: any) {
		// Verifica se algum item vai ser atualizado
		if (item) {
			// Atualiza o valor do item
			item.Nivel = nivel;

			if (tipoRequisito && tipoRequisito.target && tipoRequisito.target.value) {
				item.TipoRequisito = tipoRequisito.target.value;
			}
		}

		// Variável para conter os valores das habilidades
		const habilidades: { [key: number]: any } = {};

		// Percorre as habilidades escolhidas
		for (const habilidade of this.habilidadesEscolhidas) {
			// Cria um formControl para o id da habilidade
			habilidades[habilidade.key] = [{ Nivel: habilidade.Nivel || 1, TipoRequisito: habilidade.TipoRequisito || 'P' }];
		}

		// Atualiza o formGroup de Habilidades
		this.form.setControl('Habilidades', this._formBuilder.group(habilidades));
	}

	/**
	 * gera_testes
	 *
	 * Método responsável por converter as testes selecionados em valores do form
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 04/2019
	 * @param object opcional contendo uma opção clicada
	 * @param number opcional com o mínimo de pontos selecionado
	 * @return void
	 */
	public gera_testes(item?: any, minimoPontos?: any) {
		// Verifica se algum item vai ser atualizado
		if (item && minimoPontos) {
			// Atualiza o valor do item
			item.MinimoPontos = minimoPontos.target.value;
		}

		// Variável para conter os valores dos testes
		const testes: { [key: number]: any } = {};

		// Percorre as testes escolhidos
		for (const teste of this.testesEscolhidos) {
			// Cria um formControl para o id do teste
			testes[teste.key] = [{ MinimoPontos: teste.MinimoPontos || 1 }];
		}

		// Atualiza o formGroup de testes
		this.form.setControl('Testes', this._formBuilder.group(testes));
	}

	/**
	 * _save
	 *
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	protected async _save() {
		// Recebe os dados do formulário
		const value = this.form.value;

		// Limpa os pontos de milhar
		value.Salario = value.Salario.toString()
			.replace(/\(|\)|\s|\/|\.|\-/g, '')
			.trim();
		value.Salario = +value.Salario.toString().replace(',', '.');

		// Percorre os campos com máscara
		for (const campo of []) {
			// Regex para retirar a máscara dos campos
			value[campo] = value[campo]
				.toString()
				.replace(/\(|\)|\s|\/|\.|\-/g, '')
				.trim();
		}

		let data: any;

		try {
			// Chama a API para salvar os dados
			data = await this.__contratacoesAPI.save(value);
		} catch (error) {
			if (error.CargoId && (error.CargoId as string).indexOf('já existe em nosso banco') >= 0) {
				error.CargoId = 'Já existe uma vaga cadastrada para este cargo';
			}

			throw error;
		}

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}

	public newRegister() {
		super.newRegister();

		this.habilidadesEscolhidas = [];
		this.selectHabilidades.writeValue(this.habilidadesEscolhidas);
		this.gera_habilidades();

		this.testesEscolhidos = [];
		this.selectTestes.writeValue(this.testesEscolhidos);
		this.gera_testes();

		this.form.controls.Pcd.setValue('N');
	}

	/**
	 * atualizaHabilidades
	 *
	 * Método responsável por atualizar o valor das habilidades
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	public atualizaHabilidades(habilidades: Array<IHabilidadesEscolhidas>) {
		// Recebe a lista de habilidades escolhidas
		this.habilidadesEscolhidas = habilidades;

		// Percorre as habilidades
		for (const habilidade of this.habilidadesEscolhidas) {
			// Se não tiver a chave de Nivel
			if (!habilidade.Nivel) {
				// Cria com o valor inicial
				habilidade.Nivel = 1;
			}
		}

		this.gera_habilidades();
	}

	/**
	 * atualizaTestes
	 *
	 * Método responsável por atualizar o valor das habilidades
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	public atualizaTestes(testes: Array<ITestesEscolhidos>) {
		// Recebe a lista de testes escolhidos
		this.testesEscolhidos = testes;

		// Percorre as testes
		for (const teste of this.testesEscolhidos) {
			// Se não tiver a chave de MinimoPontos
			if (!teste.MinimoPontos) {
				// Cria com o valor inicial
				teste.MinimoPontos = 1;
			}
		}

		this.gera_testes();
	}

	public ngSelectRemoveItem(clearFn: any, item: any) {
		clearFn(item);
	}
}
