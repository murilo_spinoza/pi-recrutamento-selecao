import { NgModule } from '@angular/core';
import { ThemeModule } from '@theme/theme.module';
import { ContratacoesModalComponent } from './contratacoes-modal.component';
import { CandidatosModalModule } from 'app/pages/candidatos/modal/candidatos-modal.module';

@NgModule({
	imports: [ThemeModule, CandidatosModalModule],
	declarations: [ContratacoesModalComponent],
	entryComponents: [ContratacoesModalComponent]
})
export class ContratacoesModalModule {}
