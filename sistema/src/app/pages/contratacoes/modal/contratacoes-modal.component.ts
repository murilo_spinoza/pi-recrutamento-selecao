import { Component, OnInit } from '@angular/core';
import { ContratacoesAPI } from '@core/api/contratacoes.api';
import { MyToasterService } from '@core/services/my-toaster.service';
import { BrMaskService } from '@theme/services/br-mask.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CandidatosModalComponent } from 'app/pages/candidatos/modal/candidatos-modal.component';
import { ICandidatoTriagem, ICandidatoPontos } from '@core/interfaces/candidatos.interfaces';
import { SessionStorageService } from '@core/services/session-storage.service';

@Component({
	selector: 'contratacoes-modal',
	templateUrl: './contratacoes-modal.component.html',
	styleUrls: ['./contratacoes-modal.component.scss']
})
export class ContratacoesModalComponent implements OnInit {
	public contratacaoId: number;

	public contratacao: any = {};

	public habilidades = [];
	public testes = [];
	public escolaridades = [];
	public candidatos: ICandidatoTriagem[];
	public mediaTestes = 0;

	public loading = true;

	public exibeRequisitos = false;
	public exibeTestes = false;
	public exibeEscolaridades = false;
	public exibeCandidatos = false;

	constructor(
		private __contratacaoApi: ContratacoesAPI,
		private __myToaster: MyToasterService,
		private __brmaskService: BrMaskService,
		private __activeModal: NgbActiveModal,
		private __modal: NgbModal,
		public sessionStorage: SessionStorageService
	) {}

	ngOnInit() {
		this.__buscaDados();
	}

	private async __buscaDados() {
		try {
			const dados = await this.__contratacaoApi.setForm(this.contratacaoId, 'visualizar');

			dados.Contratacao.Salario = this.__brmaskService.create_money(dados.Contratacao.Salario);

			this.contratacao = dados.Contratacao;
			this.habilidades = dados.HabilidadesOpcoes;
			this.testes = dados.TestesOpcoes;
			this.escolaridades = dados.Escolaridades;
			this.mediaTestes = dados.MediaTestes;
			this.candidatos = dados.Triagem;
		} catch (err) {
			this.__myToaster.showErrorToast('Informações sobre o contratação não obtidas', err);
		}

		this.loading = false;
	}

	public fechaModal() {
		this.__activeModal.close();
	}

	public visualizar(candidatoId: number, pontos: ICandidatoPontos) {
		const activeModal = this.__modal.open(CandidatosModalComponent, { size: 'lg', container: 'nb-layout' });
		activeModal.componentInstance.candidatoId = candidatoId;
		activeModal.componentInstance.pontos = pontos;
		activeModal.result
			.then(() => {
				document.body.classList.add('modal-open');
			})
			.catch(err => {
				document.body.classList.add('modal-open');
			});
	}

	public async analisar(aprovada: boolean) {
		this.loading = true;
		try {
			if (confirm('Você está prestes a ' + (aprovada ? 'aprovar' : 'rejeitar') + ' esta contratação')) {
				let justificativa: string;
				if (!aprovada) {
					while (!justificativa) {
						justificativa = prompt('Justifique a rejeição da contratação');
						if (justificativa === null) {
							return;
						}
					}
				}
				await this.__contratacaoApi.analisar(this.contratacaoId, aprovada ? 'A' : 'R', justificativa);
				this.__myToaster.showSuccessToast('Análise realizada com sucesso');
				this.__activeModal.close();
			}
		} catch (err) {
			this.__myToaster.showErrorToast('Erro ao salvar a Análise', err);
		}
		this.loading = false;
	}
}
