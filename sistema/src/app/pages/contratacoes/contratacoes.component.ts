import { Component } from '@angular/core';

@Component({
	selector: 'app-contratacoes',
	template: '<router-outlet></router-outlet>'
})
export class ContratacoesComponent {
}
