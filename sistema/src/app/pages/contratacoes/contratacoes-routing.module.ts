import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContratacoesComponent } from './contratacoes.component';
import { ContratacoesFormComponent } from './form/contratacoes-form.component';
import { ContratacoesListaComponent } from './lista/contratacoes-lista.component';

const routes: Routes = [{
	path: '',
	component: ContratacoesComponent,
	children: [
		{
			path: 'form/:id',
			component: ContratacoesFormComponent
		},
		{
			path: 'form',
			component: ContratacoesFormComponent
		},
		{
			path: 'lista',
			component: ContratacoesListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const ContratacoesRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
