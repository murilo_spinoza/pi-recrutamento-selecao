import { Component, OnInit, OnDestroy, ViewChild, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';
import { Subscription } from 'rxjs';
import { DatagridEventEmitter } from '@core/services/datagrid-event-emitter';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContratacoesModalComponent } from 'app/pages/contratacoes/modal/contratacoes-modal.component';
import { ButtonRenderComponent } from '@theme/components/datagrid/render-components/button-render/button-render.component';
import { DatagridComponent } from '@theme/components/datagrid/datagrid.component';

@Component({
	selector: 'app-contratacoes-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class ContratacoesListaComponent extends BaseListComponent implements OnInit, OnDestroy {
	@ViewChild('datagrid') datagrid: DatagridComponent;
	public routine = 'contratacoes';
	public primaryKey = 'ContratacaoId';

	public tableColumns = {
		ContratacaoId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		Cargo: {
			title: 'Cargo',
			type: 'string',
			filter: {}
		},
		Contratacoes: {
			title: 'Contratações',
			type: 'string',
			filter: false
		},
		Status: {
			title: 'Status',
			type: 'string',
			filter: {
				type: 'list',
				config: {
					selectText: '--Selecione--',
					list: [{ value: 'R', title: 'Rejeitada' }, { value: 'P', title: 'Análise Pendente' }]
				}
			}
		},
		Analisar: {
			title: 'Analisar',
			type: 'custom',
			class: 'custom-button text-center',
			renderComponent: ButtonRenderComponent,
			defaultValue: {
				Icon: 'ion-eye',
				Class: 'primary',
				Text: '',
				Action: 'Analisar'
			},
			filter: false,
			sort: false
		}
	};

	private __optionClick: Subscription;

	public barraManipulacao = (row: any, action: 'edit' | 'delete') => {
		if (row.Status !== 'Análise Pendente' && row.Status !== 'Rejeitada' && action === 'edit') {
			return 'O fluxo da contratação já foi iniciado';
		}
	};

	constructor(public datagridEventEmitter: DatagridEventEmitter, private __modal: NgbModal, protected _injector: Injector) {
		super(_injector);
	}

	async _init() {
		this.__optionClick = this.datagridEventEmitter.click.subscribe(async (contratacao: any) => {
			switch (contratacao.renderAction) {
				case 'Analisar':
					this.exibeCandidato(contratacao);
					break;
			}
		});
	}

	ngOnDestroy() {
		this.__optionClick.unsubscribe();
	}
	exibeCandidato(contratacao: any) {
		const activeModal = this.__modal.open(ContratacoesModalComponent, { size: 'lg', container: 'nb-layout' });

		activeModal.componentInstance.contratacaoId = contratacao.ContratacaoId;
		activeModal.result
			.then(() => {
				this.datagrid.source.refresh();
			})
			.catch(() => {
				this.datagrid.source.refresh();
			});
	}
}
