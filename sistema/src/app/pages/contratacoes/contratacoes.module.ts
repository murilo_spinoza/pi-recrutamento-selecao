import { NgModule } from '@angular/core';
import { ContratacoesComponent } from './contratacoes.component';
import { ContratacoesFormComponent } from './form/contratacoes-form.component';
import { ContratacoesListaComponent } from './lista/contratacoes-lista.component';
import { ContratacoesRoutingModule } from './contratacoes-routing.module';
import { ThemeModule } from '@theme/theme.module';
import { ContratacoesModalModule } from './modal/contratacoes-modal.module';
import { CandidatosModalModule } from '../candidatos/modal/candidatos-modal.module';

@NgModule({
	imports: [
		ContratacoesRoutingModule,
		ContratacoesModalModule,
		CandidatosModalModule,
		ThemeModule
	],
	declarations: [
		ContratacoesComponent,
		ContratacoesFormComponent,
		ContratacoesListaComponent
	]
})

export class ContratacoesModule { }
