import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseFormComponent } from '@theme/components/base/base-form.component';
import { TestesAPI } from '@core/api/testes.api';

@Component({
	selector: 'app-testes-form',
	templateUrl: './testes-form.component.html',
	styleUrls: ['./testes-form.component.scss']
})
export class TestesFormComponent extends BaseFormComponent {
	constructor(protected _activatedRoute: ActivatedRoute, private testesAPI: TestesAPI, protected _injector: Injector) {
		super(_injector, _activatedRoute, 'testes');

		this.form = this._formBuilder.group({
			id: [],
			Teste: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async _init() {
		// Faz uma requisição para buscar os dados
		const data = await this.testesAPI.setForm(this.form.value.id);

		// Recebe os dados do registro
		this._baseRegisterInit(data.Teste);
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Chama a API para salvar os dados
		const data = await this.testesAPI.save(this.form.value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
