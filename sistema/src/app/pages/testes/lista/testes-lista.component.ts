import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-testes-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class TestesListaComponent extends BaseListComponent {
	public routine = 'testes';
	public primaryKey = 'TesteId';
	public tableColumns = {
		TesteId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},

		Teste: {
			title: 'Teste',
			type: 'string',
			filter: {}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
