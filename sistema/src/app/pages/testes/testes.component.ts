import { Component } from '@angular/core';

@Component({
	selector: 'app-testes',
	template: '<router-outlet></router-outlet>'
})
export class TestesComponent {

	constructor() { }
}
