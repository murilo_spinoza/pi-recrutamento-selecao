import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';// https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

import { TestesComponent }   from './testes.component';
import { TestesFormComponent } from './form/testes-form.component';
import { TestesListaComponent } from './lista/testes-lista.component';
import { TestesRoutingModule } from './testes-routing.module';

import { NgSelectModule } from '@ng-select/ng-select';//https://github.com/ng-select/ng-select

@NgModule({
	imports: [
		TestesRoutingModule,
		TextMaskModule,
		ThemeModule,
		NgSelectModule
	],
	declarations: [
		TestesComponent,
		TestesFormComponent,
		TestesListaComponent,
	]
})

export class TestesModule { }
