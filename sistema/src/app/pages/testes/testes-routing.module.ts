import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestesComponent } from './testes.component';
import { TestesFormComponent } from './form/testes-form.component';
import { TestesListaComponent } from './lista/testes-lista.component';

const routes: Routes = [{
	path: '',
	component: TestesComponent,
	children: [
		{
			path: 'form/:id',
			component: TestesFormComponent
		},
		{
			path: 'form',
			component: TestesFormComponent
		},
		{
			path: 'lista',
			component: TestesListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const TestesRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
