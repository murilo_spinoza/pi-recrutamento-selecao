import { NgModule } from '@angular/core';
import { ThemeModule } from '@theme/theme.module';
import { CandidatosModalComponent } from './candidatos-modal.component';

@NgModule({
	imports: [
		ThemeModule
	],
	declarations: [
		CandidatosModalComponent
	],
	entryComponents: [
		CandidatosModalComponent
	]
})
export class CandidatosModalModule { }
