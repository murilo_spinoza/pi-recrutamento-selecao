import { Component, OnInit } from '@angular/core';
import { CandidatosAPI } from '@core/api/candidatos.api';
import { MyToasterService } from '@core/services/my-toaster.service';
import { BrMaskService } from '@theme/services/br-mask.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ICandidatoPontos } from '@core/interfaces/candidatos.interfaces';

@Component({
	selector: 'candidatos-modal',
	templateUrl: './candidatos-modal.component.html',
	styleUrls: ['./candidatos-modal.component.scss']
})
export class CandidatosModalComponent implements OnInit {
	public candidatoId: number;

	public candidato: any = {};

	public pontos: ICandidatoPontos[];

	public redesSociais = [];
	public habilidades = [];
	public escolaridades = [];
	public experiencias = [];

	public loading = true;

	constructor(
		private __candidatoApi: CandidatosAPI,
		private __myToaster: MyToasterService,
		private __brmaskService: BrMaskService,
		private __activeModal: NgbActiveModal
	) {}

	ngOnInit() {
		this.__buscaDados();
	}

	private async __buscaDados() {
		try {
			const dados = await this.__candidatoApi.setForm(this.candidatoId, 'visualizar');

			dados.Candidato.Telefone = this.__brmaskService.create_phone(dados.Candidato.Telefone);
			dados.Candidato.Cpf = this.__brmaskService.create_cpf(dados.Candidato.Cpf);

			this.candidato = dados.Candidato;
			this.redesSociais = dados.RedesSociaisOpcoes;
			this.habilidades = dados.HabilidadesOpcoes;
			this.escolaridades = dados.Escolaridades;
			this.experiencias = dados.Experiencias;
		} catch (err) {
			this.__myToaster.showErrorToast('Informações sobre o candidato não obtidas', err);
		}

		this.loading = false;
	}

	public fechaModal() {
		this.__activeModal.close();
	}
}
