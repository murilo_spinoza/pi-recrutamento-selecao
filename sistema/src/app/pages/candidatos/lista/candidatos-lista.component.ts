import { Component, ViewChild, Injector, OnDestroy } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';
import { ButtonRenderComponent } from '@theme/components/datagrid/render-components/button-render/button-render.component';
import { DatagridComponent } from '@theme/components/datagrid/datagrid.component';
import { DatagridEventEmitter } from '@core/services/datagrid-event-emitter';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CandidatosModalComponent } from '../modal/candidatos-modal.component';

@Component({
	selector: 'app-candidatos-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class CandidatosListaComponent extends BaseListComponent implements OnDestroy {
	@ViewChild('datagrid') datagrid: DatagridComponent;

	public routine = 'candidatos';
	public primaryKey = 'CandidatoId';
	public tableColumns = {
		CandidatoId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		Nome: {
			title: 'Nome',
			type: 'string',
			filter: {}
		},
		Email: {
			title: 'E-mail',
			type: 'string',
			filter: {}
		},
		Visualizar: {
			title: 'Visualizar',
			type: 'custom',
			class: 'custom-button text-center',
			renderComponent: ButtonRenderComponent,
			defaultValue: {
				Icon: 'ion-eye',
				Class: 'primary',
				Text: '',
				Action: 'Visualizar'
			},
			filter: false,
			sort: false
		}
	};

	private __optionClick: Subscription;

	constructor(public datagridEventEmitter: DatagridEventEmitter, private __modal: NgbModal, protected _injector: Injector) {
		super(_injector);
	}

	async _init() {
		this.__optionClick = this.datagridEventEmitter.click.subscribe(async (candidato: any) => {
			switch (candidato.renderAction) {
				case 'Visualizar':
					this.exibeCandidato(candidato);
					break;
			}
		});
	}

	ngOnDestroy() {
		this.__optionClick.unsubscribe();
	}
	exibeCandidato(candidato: any) {
		const activeModal = this.__modal.open(CandidatosModalComponent, { size: 'lg', container: 'nb-layout' });

		activeModal.componentInstance.candidatoId = candidato.CandidatoId;
	}
}
