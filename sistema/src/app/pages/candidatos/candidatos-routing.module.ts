import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CandidatosComponent } from './candidatos.component';
import { CandidatosFormComponent } from './form/candidatos-form.component';
import { CandidatosListaComponent } from './lista/candidatos-lista.component';

const routes: Routes = [{
	path: '',
	component: CandidatosComponent,
	children: [
		{
			path: 'form/:id',
			component: CandidatosFormComponent
		},
		{
			path: 'form',
			component: CandidatosFormComponent
		},
		{
			path: 'lista',
			component: CandidatosListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const CandidatosRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
