import { Component, ViewChild, Injector } from '@angular/core';
import { FormGroup, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseFormComponent, ICampoOpcoes } from '@theme/components/base/base-form.component';
import { BrMaskService } from '@theme/services/br-mask.service';
import { CandidatosAPI } from '@core/api/candidatos.api';

export interface IHabilidadesEscolhidas extends ICampoOpcoes {
	Nivel?: number;
}

@Component({
	selector: 'app-candidatos-form',
	templateUrl: './candidatos-form.component.html',
	styleUrls: ['./candidatos-form.component.scss']
})
export class CandidatosFormComponent extends BaseFormComponent {
	public pcdOpcoes: Array<ICampoOpcoes> = [];
	public sexoOpcoes: Array<ICampoOpcoes> = [];
	public ativoOpcoes: Array<ICampoOpcoes> = [];
	public cargosOpcoes: Array<ICampoOpcoes> = [];
	public cursosOpcoes: Array<ICampoOpcoes> = [];
	public validadoOpcoes: Array<ICampoOpcoes> = [];
	public habilidadesOpcoes: Array<ICampoOpcoes> = [];
	public redesSociaisOpcoes: Array<ICampoOpcoes> = [];
	public periodoExperienciaOpcoes: Array<ICampoOpcoes> = [];

	public habilidadesEscolhidas: Array<IHabilidadesEscolhidas> = [];

	@ViewChild('selectHabilidades') selectHabilidades: any;

	constructor(
		protected _activatedRoute: ActivatedRoute,
		private __candidatosAPI: CandidatosAPI,
		public __brMaskService: BrMaskService,
		protected _injector: Injector
	) {
		super(_injector, _activatedRoute, 'candidatos');

		this.form = this._formBuilder.group({
			id: [],
			Nome: [null, [Validators.required, Validators.maxLength(80)]],
			Telefone: [null, [Validators.required, Validators.maxLength(20)]],
			Cpf: [null, [Validators.required, Validators.maxLength(15)]],
			Rg: [null, [Validators.required, Validators.maxLength(9)]],
			Nascimento: [null, [Validators.required]],
			Email: [null, [Validators.required, Validators.maxLength(70), Validators.email]],
			Senha: [],
			ConfirmaSenha: [],
			Ativo: ['S', [Validators.required]],
			Sexo: ['F', [Validators.required]],
			Pcd: ['N', [Validators.required]],
			Validado: ['S', [Validators.required]],
			Deficiencia: [],
			Endereco: [null, [Validators.required]],
			Complemento: [],
			Observacoes: [],
			RedesSociais: this._formBuilder.group({}),
			Habilidades: this._formBuilder.group({}),
			Escolaridades: this._formBuilder.array([]),
			Experiencias: this._formBuilder.array([])
		});
	}

	public async _init() {
		// Faz uma requisição à API para preencher o formulário
		const data = await this.__candidatosAPI.setForm(this.form.value.id);

		// Recebe as opções dos campos de select
		this.ativoOpcoes = data.AtivoOpcoes;
		this.sexoOpcoes = data.SexoOpcoes;
		this.validadoOpcoes = data.ValidadoOpcoes;
		this.pcdOpcoes = data.PcdOpcoes;

		// Atualiza o formGroup de Redes Sociais
		this.form.setControl('RedesSociais', this.__generateGroupFromOptions(data.RedesSociais, data.RedesSociaisOpcoes));

		// Recebe as listas de opções
		this.redesSociaisOpcoes = data.RedesSociaisOpcoes;
		this.habilidadesOpcoes = data.HabilidadesOpcoes;
		this.cargosOpcoes = data.CargosOpcoes;
		this.cursosOpcoes = data.CursosOpcoes;
		this.periodoExperienciaOpcoes = data.PeriodoExperienciaOpcoes;

		this.habilidadesEscolhidas = [];

		for (const i of data.Habilidades) {
			this.habilidadesEscolhidas.push(this.habilidadesOpcoes[i]);
		}

		this.selectHabilidades.writeValue(this.habilidadesEscolhidas);

		this.gera_habilidades();

		for (const escolaridade of data.Escolaridades) {
			this.gera_escolaridades(escolaridade);
		}
		for (const experiencia of data.Experiencias) {
			this.gera_experiencias(experiencia);
		}

		// Recebe os dados do registro
		this._baseRegisterInit(data.Candidato);

		// Formata o número de telefone e CPF
		this.form.controls.Telefone.setValue(this.__brMaskService.create_phone(this.form.value.Telefone));
		this.form.controls.Cpf.setValue(this.__brMaskService.create_cpf(this.form.value.Cpf));
	}

	private __generateGroupFromOptions(values: any = {}, options: Array<{ key: number; value: string }>): FormGroup {
		// Variável para conter os campos de cada opção
		const controls: any = {};

		// Percorre as opções recebidas
		for (const option of options) {
			// Adiciona o novo índice ao objeto
			controls[option.key] = [values[option.key] || null];
		}

		// Retorna o formGroup gerado
		return this._formBuilder.group(controls);
	}

	public gera_experiencias(param?: any) {
		param = param || {};
		const group = this._formBuilder.group({
			CargoId: [param.CargoId || null, [Validators.required]],
			Empresa: [param.Empresa || null, [Validators.required]],
			Periodo: [param.Periodo || null, [Validators.required]],
			Jornada: [param.Jornada || null, [Validators.required]],
			Entrada: [param.Entrada || null, [Validators.required]],
			Saida: [param.Saida || null],
			Atualmente: [param.Saida === null ? true : null]
		});

		(this.form.controls.Experiencias as FormArray).push(group);

		this.disableAutocomplete();
	}

	public gera_escolaridades(param?: any) {
		param = param || {};

		const group = this._formBuilder.group({
			CursoId: [param.CursoId || null, [Validators.required]],
			Instituicao: [param.Instituicao || null, [Validators.required]],
			Periodo: [param.Periodo || null, [Validators.required]],
			Inicio: [param.Inicio || null, [Validators.required]],
			Termino: [param.Termino || null, [Validators.required]]
		});

		(this.form.controls.Escolaridades as FormArray).push(group);

		this.disableAutocomplete();
	}

	/**
	 * gera_habilidades
	 *
	 * Método responsável por converter as habilidades selecionadas em valores do form
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 04/2019
	 * @param object opcional contendo uma opção clicada
	 * @param number opcional com o nivel selecionado
	 * @return void
	 */
	public gera_habilidades(item?: any, nivel?: any) {
		// Verifica se algum item vai ser atualizado
		if (item) {
			// Atualiza o valor do item
			item.Nivel = nivel;
		}

		// Variável para conter os valores das habilidades
		const habilidades: { [key: number]: any } = {};

		// Percorre as habilidades escolhidas
		for (const habilidade of this.habilidadesEscolhidas) {
			// Cria um formControl para o id da habilidade
			habilidades[habilidade.key] = [habilidade.Nivel || 1];
		}

		// Atualiza o formGroup de Habilidades
		this.form.setControl('Habilidades', this._formBuilder.group(habilidades));
	}

	toggleEnabledSaida(index) {
		const controls = ((this.form.controls.Experiencias as FormArray).controls[index] as FormGroup).controls;

		if (controls.Atualmente.value) {
			controls.Saida.setValue(null);
			controls.Saida.disable();
		} else {
			controls.Saida.enable();
		}
	}

	/**
	 * _save
	 *
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	protected async _save() {
		// Valida senha
		if (this.form.value.ConfirmaSenha !== this.form.value.Senha && (this.form.value.ConfirmaSenha || this.form.value.Senha)) {
			throw 'As senhas não coincidem';
		}

		// Recebe os dados do formulário
		const value = this.form.value;

		// Percorre os campos com máscara
		for (const campo of ['Telefone', 'Cpf']) {
			// Regex para retirar a máscara dos campos
			value[campo] = value[campo]
				.toString()
				.replace(/\(|\)|\s|\/|\.|\-/g, '')
				.trim();
		}

		// Chama a API para salvar os dados
		const data = await this.__candidatosAPI.save(value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}

	public newRegister() {
		super.newRegister();

		this.habilidadesEscolhidas = [];
		this.selectHabilidades.writeValue(this.habilidadesEscolhidas);
		this.gera_habilidades();

		// Atualiza o formGroup de Escolaridades
		this.form.setControl('Escolaridades', this._formBuilder.array([]));

		// Atualiza o formGroup de Experiencias
		this.form.setControl('Experiencias', this._formBuilder.array([]));

		this.form.controls.Ativo.setValue('S');
		this.form.controls.Sexo.setValue('F');
		this.form.controls.Pcd.setValue('N');
		this.form.controls.Validado.setValue('S');
	}

	/**
	 * atualizaHabilidades
	 *
	 * Método responsável por atualizar o valor das habilidades
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	public atualizaHabilidades(habilidades: Array<{ key: number; value: string; Nivel?: number }>) {
		// Recebe a lista de habilidades escolhidas
		this.habilidadesEscolhidas = habilidades;

		// Percorre as habilidades
		for (const habilidade of this.habilidadesEscolhidas) {
			// Se não tiver a chave de Nivel
			if (!habilidade.Nivel) {
				// Cria com o valor inicial
				habilidade.Nivel = 1;
			}
		}

		this.gera_habilidades();
	}

	removeAt(formArray: FormArray, i: number) {
		formArray.removeAt(i);
	}
}
