import { Component, OnInit } from '@angular/core';
import { CandidatosAPI } from '@core/api/candidatos.api';
import { MyToasterService } from '@core/services/my-toaster.service';
import { BrMaskService } from '@theme/services/br-mask.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'candidatos-anotacoes',
	templateUrl: './candidatos-anotacoes.component.html',
	styleUrls: ['./candidatos-anotacoes.component.scss']
})
export class CandidatosAnotacoesComponent implements OnInit {
	public candidatoId: number;

	public candidato: any = {};

	public observacoes: string;

	public loading = true;

	constructor(
		private __candidatoApi: CandidatosAPI,
		private __myToaster: MyToasterService,
		private __brmaskService: BrMaskService,
		private __activeModal: NgbActiveModal
	) {}

	ngOnInit() {
		this.__buscaDados();
	}

	private async __buscaDados() {
		try {
			const dados = await this.__candidatoApi.setForm(this.candidatoId, 'visualizar');

			dados.Candidato.Telefone = this.__brmaskService.create_phone(dados.Candidato.Telefone);
			dados.Candidato.Cpf = this.__brmaskService.create_cpf(dados.Candidato.Cpf);

			this.candidato = dados.Candidato;
			this.observacoes = this.candidato.Observacoes;
		} catch (err) {
			this.__myToaster.showErrorToast('Informações sobre o candidato não obtidas', err);
		}

		this.loading = false;
	}

	public async salvar() {
		if (this.loading) {
			return;
		}

		this.loading = true;

		try {
			await this.__candidatoApi.salvaObservacoes(this.candidatoId, this.observacoes);
			this.__myToaster.showSuccessToast('Observações salvas com sucesso');
		} catch (err) {
			this.__myToaster.showErrorToast('Observações sobre o candidato não obtidas', err);
		}

		this.loading = false;
	}

	public fechaModal() {
		this.__activeModal.close();
	}
}
