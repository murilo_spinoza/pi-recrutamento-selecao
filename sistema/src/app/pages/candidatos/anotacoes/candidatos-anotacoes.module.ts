import { NgModule } from '@angular/core';
import { ThemeModule } from '@theme/theme.module';
import { CandidatosAnotacoesComponent } from './candidatos-anotacoes.component';

@NgModule({
	imports: [ThemeModule],
	declarations: [CandidatosAnotacoesComponent],
	entryComponents: [CandidatosAnotacoesComponent]
})
export class CandidatosAnotacoesModule {}
