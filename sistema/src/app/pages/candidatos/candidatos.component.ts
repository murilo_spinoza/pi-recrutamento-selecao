import { Component } from '@angular/core';

@Component({
	selector: 'app-candidatos',
	template: '<router-outlet></router-outlet>'
})
export class CandidatosComponent {

	constructor() { }
}
