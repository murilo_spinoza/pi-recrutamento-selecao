import { NgModule } from '@angular/core';
import { CandidatosComponent } from './candidatos.component';
import { CandidatosFormComponent } from './form/candidatos-form.component';
import { CandidatosListaComponent } from './lista/candidatos-lista.component';
import { CandidatosModalModule } from './modal/candidatos-modal.module';
import { CandidatosRoutingModule } from './candidatos-routing.module';
import { ThemeModule } from '@theme/theme.module';

@NgModule({
	imports: [
		CandidatosRoutingModule,
		CandidatosModalModule,
		ThemeModule
	],
	declarations: [
		CandidatosComponent,
		CandidatosFormComponent,
		CandidatosListaComponent
	]
})

export class CandidatosModule { }
