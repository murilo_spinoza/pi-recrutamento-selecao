import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuariosGruposComponent } from './usuarios_grupos.component';
import { UsuariosGruposFormComponent } from './form/usuarios_grupos-form.component';
import { UsuariosGruposListaComponent } from './lista/usuarios_grupos-lista.component';
import { UsuariosGruposRoutingModule } from './usuarios_grupos-routing.module';
import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
	imports: [
		UsuariosGruposRoutingModule,
		CommonModule,
		ThemeModule
	],
	declarations: [
		UsuariosGruposComponent,
		UsuariosGruposFormComponent,
		UsuariosGruposListaComponent
	]
})
export class UsuariosGruposModule { }
