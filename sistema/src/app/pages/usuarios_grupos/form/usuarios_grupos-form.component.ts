import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsuariosGruposAPI } from '@core/api/usuarios_grupos.api';
import { BaseFormComponent } from '@theme/components/base/base-form.component';

@Component({
	selector: 'app-usuarios-grupos-form-component',
	templateUrl: './usuarios_grupos-form.component.html',
	styleUrls: ['./usuarios_grupos-form.component.scss']
})
export class UsuariosGruposFormComponent extends BaseFormComponent {
	public permissoesDefault: any = {};
	public rotinasClassificacoes: any = [];

	public permissoes: any = {};

	constructor(protected _activatedRoute: ActivatedRoute, private usuariosGruposAPI: UsuariosGruposAPI, protected _injector: Injector) {
		super(_injector, _activatedRoute, 'usuarios_grupos');

		this.form = this._formBuilder.group({
			id: [],
			Grupo: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	protected async _init() {
		const data = await this.usuariosGruposAPI.setForm(this.form.value.id);

		this.permissoesDefault = data.PermissoesDefault;

		for (const rotinaId in this.permissoesDefault) {
			if (data.UsuarioGrupo && data.UsuarioGrupo.Permissoes && data.UsuarioGrupo.Permissoes[rotinaId]) {
				this.permissoes[rotinaId] = Object.assign({}, data.UsuarioGrupo.Permissoes[rotinaId]);
			} else {
				this.permissoes[rotinaId] = Object.assign({}, this.permissoesDefault[rotinaId]);
			}
		}

		this.rotinasClassificacoes = data.RotinasClassificacoes;

		// Recebe os dados do usuario
		this._baseRegisterInit(data.UsuarioGrupo);
	}

	/**
	 * newRegister
	 *
	 * Método para limpar o formulário e zerar os erros
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {void}
	 * @return {void}
	 */
	public newRegister() {
		this.form.reset();

		for (const rotinaId in this.permissoesDefault) {
			this.permissoes[rotinaId] = Object.assign({}, this.permissoesDefault[rotinaId]);
		}

		this.submitted = false;
		this.errors = {};
	}

	protected async _save() {
		const data = this.form.value;

		data.Permissoes = this.permissoes;

		const id = await this.usuariosGruposAPI.save(data);

		this.form.controls.id.setValue(id);
	}

	teste(change) {
		console.log(change);
	}
}
