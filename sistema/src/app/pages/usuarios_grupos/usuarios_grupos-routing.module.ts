import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosGruposComponent } from './usuarios_grupos.component';
import { UsuariosGruposFormComponent } from './form/usuarios_grupos-form.component';
import { UsuariosGruposListaComponent } from './lista/usuarios_grupos-lista.component';

const routes: Routes = [{
	path: '',
	component: UsuariosGruposComponent,
	children: [
		{
			path: 'form/:id',
			component: UsuariosGruposFormComponent
		},
		{
			path: 'form',
			component: UsuariosGruposFormComponent
		},
		{
			path: 'lista',
			component: UsuariosGruposListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const UsuariosGruposRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
