import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-usuarios-grupos-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class UsuariosGruposListaComponent extends BaseListComponent {
	public routine = 'usuarios_grupos';
	public primaryKey = 'UsuarioGrupoId';
	public tableColumns = {
		UsuarioGrupoId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		Grupo: {
			title: 'Grupo',
			type: 'string',
			filter: {}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
