import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-users-groups',
	template: '<router-outlet></router-outlet>'
})
export class UsuariosGruposComponent implements OnInit {

	constructor() { }

	ngOnInit() { }

}
