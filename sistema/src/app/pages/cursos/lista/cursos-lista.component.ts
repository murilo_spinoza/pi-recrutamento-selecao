import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-cursos-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class CursosListaComponent extends BaseListComponent {

	public routine = 'cursos';
	public primaryKey = 'CursoId';

	public tableColumns = {
		CursoId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		GrauEscolaridade: {
			title: 'Grau de Escolaridade',
			type: 'string',
			filter: {}
		},
		Curso: {
			title: 'Curso',
			type: 'string',
			filter: {}
		}
	};
	
	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
