import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';// https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

import { CursosComponent }   from './cursos.component';
import { CursosFormComponent } from './form/cursos-form.component';
import { CursosListaComponent } from './lista/cursos-lista.component';
import { CursosRoutingModule } from './cursos-routing.module';

import { NgSelectModule } from '@ng-select/ng-select';//https://github.com/ng-select/ng-select

@NgModule({
	imports: [
		CursosRoutingModule,
		TextMaskModule,
		ThemeModule,
		NgSelectModule
	],
	declarations: [
		CursosComponent,
		CursosFormComponent,
		CursosListaComponent,
	]
})

export class CursosModule { }
