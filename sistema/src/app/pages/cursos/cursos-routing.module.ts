import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursosComponent } from './cursos.component';
import { CursosFormComponent } from './form/cursos-form.component';
import { CursosListaComponent } from './lista/cursos-lista.component';

const routes: Routes = [{
	path: '',
	component: CursosComponent,
	children: [
		{
			path: 'form/:id',
			component: CursosFormComponent
		},
		{
			path: 'form',
			component: CursosFormComponent
		},
		{
			path: 'lista',
			component: CursosListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const CursosRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
