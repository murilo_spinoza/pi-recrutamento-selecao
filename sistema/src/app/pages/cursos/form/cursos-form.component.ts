import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseFormComponent, ICampoOpcoes } from '@theme/components/base/base-form.component';
import { CursosAPI } from '@core/api/cursos.api';

@Component({
	selector: 'app-cursos-form',
	templateUrl: './cursos-form.component.html',
	styleUrls: ['./cursos-form.component.scss']
})
export class CursosFormComponent extends BaseFormComponent {
	public grauEscolaridadeOpcoes: Array<ICampoOpcoes> = [];

	constructor(protected _activatedRoute: ActivatedRoute, private __cursosAPI: CursosAPI, protected _injector: Injector) {
		super(_injector, _activatedRoute, 'cursos');

		this.form = this._formBuilder.group({
			id: [],
			GrauEscolaridadeId: [null, [Validators.required]],
			Curso: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async _init() {
		// Faz uma requisição para buscar os dados
		const data = await this.__cursosAPI.setForm(this.form.value.id);

		// Recebe as opções para o campo de tipo de curso
		this.grauEscolaridadeOpcoes = data.GrausEscolaridades;

		// Recebe os dados do registro
		this._baseRegisterInit(data.Curso);
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Chama a API para salvar os dados
		const data = await this.__cursosAPI.save(this.form.value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
