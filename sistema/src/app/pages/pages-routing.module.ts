import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TypographyComponent } from './typography/typography.component';

const routes: Routes = [{
	path: '',
	component: PagesComponent,
	children: [
		{
			path: 'dashboard',
			component: DashboardComponent,
		},
		{
			path: 'logs',
			loadChildren: './logs/logs.module#LogsModule'
		},
		{
			path: 'cargos',
			loadChildren: './cargos/cargos.module#CargosModule'
		},
		{
			path: 'contratacoes',
			loadChildren: './contratacoes/contratacoes.module#ContratacoesModule'
		},
		{
			path: 'cursos',
			loadChildren: './cursos/cursos.module#CursosModule'
		},
		{
			path: 'habilidades',
			loadChildren: './habilidades/habilidades.module#HabilidadesModule'
		},
		{
			path: 'redes_sociais',
			loadChildren: './redes_sociais/redes_sociais.module#RedesSociaisModule'
		},
		{
			path: 'quadro_vagas',
			loadChildren: './quadro_vagas/quadro_vagas.module#QuadroVagasModule'
		},
		{
			path: 'processos_seletivos',
			loadChildren: './processos_seletivos/processos_seletivos.module#ProcessosSeletivosModule'
		},
		{
			path: 'testes',
			loadChildren: './testes/testes.module#TestesModule'
		},
		{
			path: 'graus_escolaridades',
			loadChildren: './graus_escolaridades/graus_escolaridades.module#GrausEscolaridadesModule'
		},
		{
			path: 'candidatos',
			loadChildren: './candidatos/candidatos.module#CandidatosModule'
		},
		{
			path: 'usuarios',
			loadChildren: './usuarios/usuarios.module#UsuariosModule'
		},
		{
			path: 'usuarios_grupos',
			loadChildren: './usuarios_grupos/usuarios_grupos.module#UsuariosGruposModule'
		},
		{
			path: 'typography',
			component: TypographyComponent
		},
		{
			path: '',
			redirectTo: 'dashboard',
			pathMatch: 'full'
		},
	],
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})

export class PagesRoutingModule { }
