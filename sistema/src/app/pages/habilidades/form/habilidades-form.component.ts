import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HabilidadesAPI } from '@core/api/habilidades.api';
import { BaseFormComponent } from '@theme/components/base/base-form.component';

@Component({
	selector: 'app-habilidades-form',
	templateUrl: './habilidades-form.component.html',
	styleUrls: ['./habilidades-form.component.scss']
})
export class HabilidadesFormComponent extends BaseFormComponent {
	constructor(protected _activatedRoute: ActivatedRoute, private habilidadesAPI: HabilidadesAPI, protected _injector: Injector) {
		super(_injector, _activatedRoute, 'habilidades');

		this.form = this._formBuilder.group({
			id: [],
			Habilidade: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async _init() {
		// Faz uma requisição para buscar os dados
		const data = await this.habilidadesAPI.setForm(this.form.value.id);

		// Recebe os dados do registro
		this._baseRegisterInit(data.Habilidade);
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Chama a API para salvar os dados
		const data = await this.habilidadesAPI.save(this.form.value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
