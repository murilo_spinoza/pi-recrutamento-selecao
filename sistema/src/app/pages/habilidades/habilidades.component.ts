import { Component } from '@angular/core';

@Component({
	selector: 'app-habilidades',
	template: '<router-outlet></router-outlet>'
})
export class HabilidadesComponent {

	constructor() { }
}
