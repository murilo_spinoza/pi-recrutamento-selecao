import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-habilidades-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class HabilidadesListaComponent extends BaseListComponent {
	public routine = 'habilidades';
	public primaryKey = 'HabilidadeId';

	public tableColumns = {
		HabilidadeId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},

		Habilidade: {
			title: 'Habilidade',
			type: 'string',
			filter: {}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
