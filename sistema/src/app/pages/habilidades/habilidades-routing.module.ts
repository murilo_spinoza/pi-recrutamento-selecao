import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HabilidadesComponent } from './habilidades.component';
import { HabilidadesFormComponent } from './form/habilidades-form.component';
import { HabilidadesListaComponent } from './lista/habilidades-lista.component';

const routes: Routes = [{
	path: '',
	component: HabilidadesComponent,
	children: [
		{
			path: 'form/:id',
			component: HabilidadesFormComponent
		},
		{
			path: 'form',
			component: HabilidadesFormComponent
		},
		{
			path: 'lista',
			component: HabilidadesListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const HabilidadesRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
