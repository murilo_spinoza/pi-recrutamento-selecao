import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';// https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

import { HabilidadesComponent }   from './habilidades.component';
import { HabilidadesFormComponent } from './form/habilidades-form.component';
import { HabilidadesListaComponent } from './lista/habilidades-lista.component';
import { HabilidadesRoutingModule } from './habilidades-routing.module';

import { NgSelectModule } from '@ng-select/ng-select';//https://github.com/ng-select/ng-select

@NgModule({
	imports: [
		HabilidadesRoutingModule,
		TextMaskModule,
		ThemeModule,
		NgSelectModule
	],
	declarations: [
		HabilidadesComponent,
		HabilidadesFormComponent,
		HabilidadesListaComponent,
	]
})

export class HabilidadesModule { }
