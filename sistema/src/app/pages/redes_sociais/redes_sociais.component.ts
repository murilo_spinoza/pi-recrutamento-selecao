import { Component } from '@angular/core';

@Component({
	selector: 'app-redes-sociais',
	template: '<router-outlet></router-outlet>'
})
export class RedesSociaisComponent {

	constructor() { }
}
