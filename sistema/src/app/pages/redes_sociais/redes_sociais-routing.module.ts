import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedesSociaisComponent } from './redes_sociais.component';
import { RedesSociaisFormComponent } from './form/redes_sociais-form.component';
import { RedesSociaisListaComponent } from './lista/redes_sociais-lista.component';

const routes: Routes = [{
	path: '',
	component: RedesSociaisComponent,
	children: [
		{
			path: 'form/:id',
			component: RedesSociaisFormComponent
		},
		{
			path: 'form',
			component: RedesSociaisFormComponent
		},
		{
			path: 'lista',
			component: RedesSociaisListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const RedesSociaisRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
