import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask'; // https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

import { RedesSociaisComponent } from './redes_sociais.component';
import { RedesSociaisFormComponent } from './form/redes_sociais-form.component';
import { RedesSociaisListaComponent } from './lista/redes_sociais-lista.component';
import { RedesSociaisRoutingModule } from './redes_sociais-routing.module';

import { NgSelectModule } from '@ng-select/ng-select'; // https://github.com/ng-select/ng-select

@NgModule({
	imports: [
		RedesSociaisRoutingModule,
		TextMaskModule,
		ThemeModule,
		NgSelectModule
	],
	declarations: [
		RedesSociaisComponent,
		RedesSociaisFormComponent,
		RedesSociaisListaComponent,
	]
})

export class RedesSociaisModule { }
