import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-redes-sociais-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class RedesSociaisListaComponent extends BaseListComponent {
	public routine = 'redes_sociais';
	public primaryKey = 'RedeSocialId';

	public tableColumns = {
		RedeSocialId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},

		RedeSocial: {
			title: 'Rede Social',
			type: 'string',
			filter: {}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
