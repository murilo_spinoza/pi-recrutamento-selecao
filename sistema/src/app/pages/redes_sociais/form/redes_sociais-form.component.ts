import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RedesSociaisAPI } from '@core/api/redes_sociais.api';
import { BaseFormComponent } from '@theme/components/base/base-form.component';

@Component({
	selector: 'app-redes-sociais-form',
	templateUrl: './redes_sociais-form.component.html',
	styleUrls: ['./redes_sociais-form.component.scss']
})
export class RedesSociaisFormComponent extends BaseFormComponent {
	constructor(protected _activatedRoute: ActivatedRoute, private redes_sociaisAPI: RedesSociaisAPI, protected _injector: Injector) {
		super(_injector, _activatedRoute, 'redes_sociais');

		this.form = this._formBuilder.group({
			id: [],
			RedeSocial: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async _init() {
		// Faz uma requisição para buscar os dados
		const data = await this.redes_sociaisAPI.setForm(this.form.value.id);

		// Recebe os dados do registro
		this._baseRegisterInit(data.RedeSocial);
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Chama a API para salvar os dados
		const data = await this.redes_sociaisAPI.save(this.form.value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
