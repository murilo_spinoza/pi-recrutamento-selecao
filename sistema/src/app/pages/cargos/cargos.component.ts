import { Component } from '@angular/core';

@Component({
	selector: 'app-cargos',
	template: '<router-outlet></router-outlet>'
})
export class CargosComponent {

	constructor() { }
}
