import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CargosComponent } from './cargos.component';
import { CargosFormComponent } from './form/cargos-form.component';
import { CargosListaComponent } from './lista/cargos-lista.component';

const routes: Routes = [{
	path: '',
	component: CargosComponent,
	children: [
		{
			path: 'form/:id',
			component: CargosFormComponent
		},
		{
			path: 'form',
			component: CargosFormComponent
		},
		{
			path: 'lista',
			component: CargosListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const CargosRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
