import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';// https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

import { CargosComponent }   from './cargos.component';
import { CargosFormComponent } from './form/cargos-form.component';
import { CargosListaComponent } from './lista/cargos-lista.component';
import { CargosRoutingModule } from './cargos-routing.module';

import { NgSelectModule } from '@ng-select/ng-select';//https://github.com/ng-select/ng-select

@NgModule({
	imports: [
		CargosRoutingModule,
		TextMaskModule,
		ThemeModule,
		NgSelectModule
	],
	declarations: [
		CargosComponent,
		CargosFormComponent,
		CargosListaComponent,
	]
})

export class CargosModule { }
