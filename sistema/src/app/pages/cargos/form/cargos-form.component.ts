import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseFormComponent } from '@theme/components/base/base-form.component';
import { CargosAPI } from '@core/api/cargos.api';

@Component({
	selector: 'app-cargos-form',
	templateUrl: './cargos-form.component.html',
	styleUrls: ['./cargos-form.component.scss']
})
export class CargosFormComponent extends BaseFormComponent {
	constructor(protected _activatedRoute: ActivatedRoute, private __cargosAPI: CargosAPI, protected _injector: Injector) {
		super(_injector, _activatedRoute, 'cargos');

		this.form = this._formBuilder.group({
			id: [],
			Cargo: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _init() {
		// Faz uma requisição para buscar os dados
		const data = await this.__cargosAPI.setForm(this.form.value.id);

		// Recebe os dados do registro
		this._baseRegisterInit(data.Cargo);
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Chama a API para salvar os dados
		const data = await this.__cargosAPI.save(this.form.value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
