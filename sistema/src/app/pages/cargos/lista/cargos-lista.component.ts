import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-cargos-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})
export class CargosListaComponent extends BaseListComponent {

	public routine = 'cargos';
	public primaryKey = 'CargoId';
	public tableColumns = {
		CargoId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},

		Cargo: {
			title: 'Cargo',
			type: 'string',
			filter: {}
		}
	};

	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
