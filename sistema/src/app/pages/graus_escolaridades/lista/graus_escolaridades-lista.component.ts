import { Component, Injector } from '@angular/core';
import { BaseListComponent } from '@theme/components/base/base-list.component';

@Component({
	selector: 'app-graus-escolaridades-lista',
	templateUrl: '../../../@theme/components/base/base-list.component.html',
	styleUrls: ['../../../@theme/components/base/base-list.component.scss']
})

export class GrausEscolaridadesListaComponent extends BaseListComponent {

	public routine = 'graus_escolaridades';
	public primaryKey = 'GrauEscolaridadeId';

	public tableColumns = {
		GrauEscolaridadeId: {
			title: 'ID',
			type: 'number',
			filter: {}
		},
		GrauEscolaridade: {
			title: 'Grau de Escolaridade',
			type: 'string',
			filter: {}
		}
	};
	
	constructor(protected _injector: Injector) {
		super(_injector);
	}
}
