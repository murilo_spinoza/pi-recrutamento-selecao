import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GrausEscolaridadesComponent } from './graus_escolaridades.component';
import { GrausEscolaridadesFormComponent } from './form/graus_escolaridades-form.component';
import { GrausEscolaridadesListaComponent } from './lista/graus_escolaridades-lista.component';

const routes: Routes = [{
	path: '',
	component: GrausEscolaridadesComponent,
	children: [
		{
			path: 'form/:id',
			component: GrausEscolaridadesFormComponent
		},
		{
			path: 'form',
			component: GrausEscolaridadesFormComponent
		},
		{
			path: 'lista',
			component: GrausEscolaridadesListaComponent
		},
		{
			path: '',
			redirectTo: 'lista',
			pathMatch: 'full'
		}
	]
}];

export const GrausEscolaridadesRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
