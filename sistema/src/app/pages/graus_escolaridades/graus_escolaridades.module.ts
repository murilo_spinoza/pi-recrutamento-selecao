import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';// https://www.npmjs.com/package/angular2-text-mask
import { ThemeModule } from '../../@theme/theme.module';

import { GrausEscolaridadesComponent }   from './graus_escolaridades.component';
import { GrausEscolaridadesFormComponent } from './form/graus_escolaridades-form.component';
import { GrausEscolaridadesListaComponent } from './lista/graus_escolaridades-lista.component';
import { GrausEscolaridadesRoutingModule } from './graus_escolaridades-routing.module';

import { NgSelectModule } from '@ng-select/ng-select';//https://github.com/ng-select/ng-select

@NgModule({
	imports: [
		GrausEscolaridadesRoutingModule,
		TextMaskModule,
		ThemeModule,
		NgSelectModule
	],
	declarations: [
		GrausEscolaridadesComponent,
		GrausEscolaridadesFormComponent,
		GrausEscolaridadesListaComponent,
	]
})

export class GrausEscolaridadesModule { }
