import { Component } from '@angular/core';

@Component({
	selector: 'app-tipos-cursos',
	template: '<router-outlet></router-outlet>'
})
export class GrausEscolaridadesComponent {

	constructor() { }
}
