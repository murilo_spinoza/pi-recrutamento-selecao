import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseFormComponent } from '@theme/components/base/base-form.component';
import { GrausEscolaridadesAPI } from '@core/api/graus_escolaridades.api';

@Component({
	selector: 'app-graus-escolaridades-form',
	templateUrl: './graus_escolaridades-form.component.html',
	styleUrls: ['./graus_escolaridades-form.component.scss']
})
export class GrausEscolaridadesFormComponent extends BaseFormComponent {
	constructor(
		protected _activatedRoute: ActivatedRoute,
		private grausEscolaridadesAPI: GrausEscolaridadesAPI,
		protected _injector: Injector
	) {
		super(_injector, _activatedRoute, 'graus_escolaridades');

		this.form = this._formBuilder.group({
			id: [],
			GrauEscolaridade: [null, [Validators.required, Validators.maxLength(50)]]
		});
	}

	/**
	 * Carrega as informações para preencher o formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async _init() {
		// Faz uma requisição para buscar os dados
		const data = await this.grausEscolaridadesAPI.setForm(this.form.value.id);

		// Recebe os dados do registro
		this._baseRegisterInit(data.GrauEscolaridade);
	}

	/**
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @protected
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _save() {
		// Chama a API para salvar os dados
		const data = await this.grausEscolaridadesAPI.save(this.form.value);

		// Se vier valor para o id
		if (data.id) {
			// Armazena no campo do formulário
			this.form.controls.id.setValue(data.id);
		}
	}
}
