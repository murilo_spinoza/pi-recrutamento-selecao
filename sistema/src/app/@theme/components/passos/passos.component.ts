import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
// tslint:disable-next-line: component-selector
	selector: 'passos',
	templateUrl: './passos.component.html',
	styleUrls: ['./passos.component.scss']
})
export class PassosComponent implements OnInit {
	@Input() passos: Array<{ Nome: string; Alias: string }>;
	@Input() etapa: string;
	@Input() etapaAtual: string;
	@Output() selecionado: EventEmitter<{ indice: number; alias: string }> = new EventEmitter();

	public ordem: { [key: string]: number } = {};

	constructor() {}

	ngOnInit() {
		for (const i in this.passos) {
			this.ordem[this.passos[i].Alias] = +i;
		}
	}

	public selecao(indice: number, alias: string) {
		this.selecionado.emit({ indice, alias });
	}
}
