import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbSpinnerModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';

import { ContactFormComponent } from './contact-form.component';

@NgModule({
	imports: [CommonModule, NbCardModule, RouterModule, FormsModule, TextMaskModule, NbSpinnerModule],
	declarations: [ContactFormComponent],
	entryComponents: [ContactFormComponent]
})
export class ContactFormModule {}
