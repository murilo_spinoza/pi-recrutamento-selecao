import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { SessionStorageService } from '../../../@core/services/session-storage.service';
import { UsuariosAPI } from '../../../@core/api/usuarios.api';
import { MyToasterService } from '../../../@core/services/my-toaster.service';

@Component({
	selector: 'app-contact-form',
	templateUrl: './contact-form.component.html',
	styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent {

	public submitted = false;
	public submitting = false;
	public loading = false;

	// Variável base para o formulário, usada para limpar o formulário
	public dados = {
		Assunto: '',
		Nome: '',
		Email: '',
		Mensagem: '',
	};

	public erros = {
		Email: false
	};

	constructor(
		private __myToaster: MyToasterService,
		private __session: SessionStorageService,
		private __activeModal: NgbActiveModal,
		private __usuariosAPI: UsuariosAPI,
	) {

		this.dados.Nome = this.__session.getUserName();
		this.dados.Email = this.__session.getEmail();
	}

	/**
     * onSubmit
     *
     * Método chamado na tentativa de submissão do formulário
     *
     * @protected
     * @author Murilo Spinoza de Arruda
     * @since  03/2019
     */
	public async onSubmit(form: any) {
		this.submitted = true;
		this.submitting = true;
		this.loading = true;
		try {
			if (form.valid) {
				// Chama a API para salvar os dados
				await this.__usuariosAPI.contact(this.dados);

				this.__myToaster.showSuccessToast('Contato realizado com sucesso!', 'Em breve entraremos em contato.');

				// timeout para terminar o onSubmit antes de fechar o modal
				this.__activeModal.close();

			} else {
				this.__myToaster.showCompletionToast();
			}
			this.submitting = false;
		} catch (err) {
			this.submitting = false;
			this.__myToaster.showErrorToast('Contato não efetuado', err);
		}

		this.loading = false;
	}

	public close() {
		this.__activeModal.close();
	}
}
