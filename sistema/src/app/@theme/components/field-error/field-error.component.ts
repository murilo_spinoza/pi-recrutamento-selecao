import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'field-error',
	templateUrl: './field-error.component.html',
	styleUrls: ['./field-error.component.scss']
})
export class FieldErrorComponent implements OnChanges {

	@Input() submitted: boolean;
	@Input() label: string;
	@Input() control: FormControl;
	@Input() error: string;

	public fieldDanger: boolean = false;

	constructor() { }

	ngOnChanges(changes: SimpleChanges) {
		this.fieldDanger = !!((this.control.errors && this.submitted) || this.error);
	}

}
