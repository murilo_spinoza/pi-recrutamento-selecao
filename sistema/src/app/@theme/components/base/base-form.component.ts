import { OnInit, Injector } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RotinasService } from '@core/services/rotinas.service';
import { MyToasterService } from '@core/services/my-toaster.service';
import { FormBuilder, FormGroup } from '@angular/forms';

export interface ICampoOpcoes {
	key: number;
	value: string;
}

export abstract class BaseFormComponent implements OnInit {
	// Indica se o formulário houve uma submissão/tentativa de submissão do form
	public submitted = false;
	public submitting = false;

	// Objeto que armazena os erros recebidos da API
	public errors: any = {};

	// Dados do formulário
	public form: FormGroup;

	// Variável para mudar o comportamento quando for modal
	// Alterar os botões de rodapé nestes casos.
	public isModal = false;

	// Loading controller
	public loading = false;

	// Nome da rotina
	public rotina = '';
	public classificacao = '';

	protected _location: Location;
	protected _myToaster: MyToasterService;
	protected _formBuilder: FormBuilder;
	protected _rotinasService: RotinasService;

	constructor(protected _injector: Injector, protected _activatedRoute: ActivatedRoute, public routine: string) {
		this.__inject();
	}

	ngOnInit() {
		this._onInit();
		this.disableAutocomplete();
	}

	/**
	 * Realiza as injeções de dependência do base form
	 * para as classes que estenderem não precisarem
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	private __inject() {
		// Faz a injeção das dependências
		this._location = this._injector.get(Location);
		this._myToaster = this._injector.get(MyToasterService);
		this._formBuilder = this._injector.get(FormBuilder);
		this._rotinasService = this._injector.get(RotinasService);
	}

	private __disableAutocomplete() {
		// Obtém todos os inputs que não são de senha
		const elements = document.querySelectorAll("input:not([type='password'])");

		// Percorre os elementos
		for (const i in elements) {
			// Verifica se o elemento atual tem o método de definir o valor de um attributo
			if (elements[i].setAttribute) {
				// Define o autocomplete com um valor dinâmico para o navegador não sugerir nada
				elements[i].setAttribute('autocomplete', 'ignored-' + new Date().getTime());
			}
		}
	}

	public disableAutocomplete() {
		this.__disableAutocomplete();
		setTimeout(() => {
			this.__disableAutocomplete();
		}, 500);
	}

	protected async _onInit() {
		this.loading = true;

		try {
			// Espera a a busca de rotinas ser concluída
			await this._rotinasService.buscaRealizada;

			// Recebe a rotina referente a este controller
			this.rotina = this._rotinasService.rotinas[this.routine].Rotina;
			this.classificacao = this._rotinasService.retornaClassificacao(this.routine);

			// Recebe o id do registro dos campos ou não
			this.form.controls.id.setValue(+(this._activatedRoute.snapshot.params['id'] || 0));

			// Faz uma requisição à API para preencher o formulário
			await this._init();

			// Capta os erros
		} catch (error) {
			// Limpa o id
			this.form.controls.id.setValue(0);

			// Exibe o erro
			this._myToaster.showGetErrorToast(error);
		}

		this.loading = false;
	}

	/**
	 * _init
	 *
	 * Método buscar os dados e preencher o formulário
	 *
	 * @protected
	 * @abstract
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected abstract async _init(): Promise<void>;

	/**
	 * _save
	 *
	 * Método para realizar o salvar
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected abstract async _save(): Promise<void>;

	/**
	 * _baseRegisterInit
	 *
	 * Método básico do init do registro podendo ser chamado na implementação do init
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  registro object:any
	 */
	protected _baseRegisterInit(registro: any) {
		// Recebe os campos do formulário
		const fields = Object.keys(this.form.value);

		// Se vier dados
		if (registro) {
			// Percorre cada campo
			for (const field of fields) {
				// Verifica se o campo foi recebido
				if (registro[field]) {
					// Atribui o valor ao respectivo campo do formulário
					this.form.controls[field].setValue(registro[field]);
				}
			}
		} else {
			// Limpa o id
			this.form.controls.id.setValue(0);
		}
	}

	/**
	 * newRegister
	 *
	 * Método para limpar o formulário e zerar os erros
	 *
	 * @protected
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public newRegister() {
		// Limpa os dados
		this.form.reset();

		// Reseta a variavel de submissão
		this.submitted = false;
		this.errors = {};

		this._location.go('paginas/' + this.routine + '/form');
	}

	/**
	 * onSubmit
	 *
	 * Método chamado na tentativa de submissão do formulário
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async onSubmit() {
		// Atualiza o controle de submissão
		this.submitted = true;
		this.submitting = true;
		this.errors = {};

		this.loading = true;

		// Verifica se o formulário é válido
		if (this.form.valid) {
			try {
				await this._save();

				// Libera a nova submissão
				this.submitting = false;

				// Quando for modal, o sucesso é tratado dentro do próprio _save
				// Para permitir uma mensagem de sucesso mais específica - Ver bigmob > properties
				if (!this.isModal) {
					// Atualiza a url para conter o id do registro a ser editado
					this._location.go('paginas/' + this.routine + '/form/' + this.form.value.id);

					// Informa que foi sucesso
					this._myToaster.showSaveSuccessToast();
				}

				// Captura os erros
			} catch (error) {
				// Libera a nova submissão
				this.submitting = false;

				// Se for um objeto
				if (typeof error === 'object') {
					// Recebe os erros
					this.errors = error;

					// Se houver um erro geral
					if (error.Geral) {
						// Exibe uma mensagem com o erro
						this._myToaster.showSaveErrorToast(error.Geral);
						// Senão, o erro é exibido nos respectivos campos
					} else {
						this._myToaster.showCompletionToast();
					}
				} else if (error) {
					// Senão e se houver erro, mostra o erro
					this._myToaster.showSaveErrorToast(error);

					// Senão, mostra uma mensagem padrão
				} else {
					this._myToaster.showErrorToast('Não foi possível salvar os dados');
				}
			}
		} else {
			// Mostra a mensagem sobre erros de preenchimento
			this._myToaster.showCompletionToast();

			// Libera a nova submissão
			this.submitting = false;
		}

		// Oculta o loading
		this.loading = false;
	}
}
