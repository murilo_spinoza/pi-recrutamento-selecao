import { OnInit, Injector } from '@angular/core';
import { RotinasService } from '@core/services/rotinas.service';

export class BaseListComponent implements OnInit {
	// Rotina acessada pela listagem
	public routine = '';

	// Método api
	public method = '';

	// Chave primaria dos dados listados
	public primaryKey = '';

	// Permite exclusão -> Logs mesmo que tenham permissão não podem ser excluód
	public canDelete = true;

	// Permite geração para excel
	public canExport = true;

	// routine concatenada com uma barra
	public apiMethod = '';

	// Rota para o formulário -> passar [] para bloquear os botões de insert e update
	public formPage: any;

	// Nome da rotina
	public rotina = '';

	// Classificação da rotina
	public classificacao = '';

	// Colunas da tabela
	public tableColumns = {};

	// Função para validar se o registro pode ser editado ou não
	public barraManipulacao: any;

	// Botão de adicionar externo
	public internalAdd = false;

	// Botão de adicionar externo
	public showAdd = false;

	// Filtros Adicionais
	/*
		(T): (T)exto curto, text(A)rea, texto (F)ormatado
		Seleção (U)nica, seleção (M)últipla
		(D)ata
		(N)úmero Inteiro
		(P)onto flutuante
		(B)ooleano: sim ou não
		(V)alor: Moeda

		{
			Tipo: 'V',
			Field: 'PrecoVenda',
			Nome: 'Preço de Venda:',
			Opcoes?: [{key: '', value: ''}]
		}
	*/
	public filtrosAdicionais = [];

	// Mostra filtros
	public showFilters = false;

	// Armazena um objeto como model para os valores dos filtros
	public filtrosValores = {};

	// Armazena um objeto como model para os valores dos filtros
	public params = {};

	// Service de rotinas
	public rotinasService: RotinasService;

	constructor(protected _injector: Injector) {
		this.__inject();
	}

	/**
	 * Realiza as injeções de dependência do base list
	 * para as classes que estenderem não precisarem
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	private __inject() {
		// Busca o service de rotinas
		this.rotinasService = this._injector.get(RotinasService);
	}

	async ngOnInit(): Promise<void> {
		// Se o form não tiver sido passado, constroi um padrão
		if (!this.formPage) {
			this.formPage = ['paginas', this.routine, 'form'];
		}

		// Monta o endpoint da api
		if (this.method) {
			this.apiMethod = this.routine + '/' + this.method + '/';
		} else {
			this.apiMethod = this.routine + '/';
		}

		// Espera a busca das rotinas ter sido realizada
		await this.rotinasService.buscaRealizada;

		// Recebe o nome da rotina
		this.rotina = this.rotinasService.rotinas[this.routine].Rotina;

		// Recebe a classificação
		this.classificacao = this.rotinasService.retornaClassificacao(this.routine);

		// Executa o init da classe filha
		this._init();
	}

	_init() {}

	limparFiltros() {
		// Percorre cada campo dos filtros e reseta o valor
		for (const filtro in this.filtrosValores) {
			this.filtrosValores[filtro] = '';
		}
	}
}
