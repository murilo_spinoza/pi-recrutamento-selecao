import { Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DatagridComponent } from './datagrid.component';
import { NbCardModule, NbSpinnerModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
	imports: [
		CommonModule,
		Ng2SmartTableModule,
		NbCardModule,
		FormsModule,
		NbSpinnerModule
	],
	declarations: [
		DatagridComponent,
	],
	exports: [
		DatagridComponent,
	]
})
export class DatagridModule {
}
