import { Component, OnInit, Input, Injector } from '@angular/core';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx'; // https://www.npmjs.com/package/xlsx

import { MyServerDataSourceService } from '../../../@core/services/my-server-data-source.service';
import { RotinasService } from '../../../@core/services/rotinas.service';

@Component({
	selector: 'app-datagrid',
	templateUrl: './datagrid.component.html',
	styleUrls: ['./datagrid.component.scss']
})
export class DatagridComponent implements OnInit {
	// Example filter: https://github.com/akveo/ng2-smart-table/blob/master/src/app/pages/examples/filter/advanced-example-filters.component.ts

	// Colunas do grid
	@Input() columns: any = {};

	// Rotina da página atual
	@Input() routine;

	// Valida a permissão de edição ou exclusão
	@Input() barraManipulacao;

	// Método para busca de dados na api
	@Input() apiMethod = '';

	// Parâmetro para travar a exclusão
	@Input() canDelete = true;

	// Parâmetro para travar a exportação
	@Input() canExport = true;

	// Campo a ser usado na passagem de parametro para o form de edição
	@Input() primaryKey = '';

	// Navigate para o form de edição -> Se não for passado, não aparece botão de inclusão ou edição
	@Input() formPage = [];

	// Envia parametros de filtro extras para a api
	@Input() params: Object = {};

	// Número de registros por página
	public perPage = 20;

	// Nome da rotina na tela
	public rotina = '';

	// Loading
	public loading = false;

	// Configurações base do grid
	public settings: any = {
		mode: 'external',
		noDataMessage: 'Nenhum registro encontrado',
		actions: {
			add: false,
			edit: false,
			delete: false,
			columnTitle: 'Ações',
			position: 'right',
			class: 'btn-tn'
		},
		add: {
			addButtonContent: 'Cadastro'
		},
		edit: {
			editButtonContent: '<i class="nb-edit"></i>'
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>'
		},
		pager: {
			perPage: this.perPage
		}
	};

	// Opções de tamanhos de páginas
	public pageSizes = [
		{ key: 10, value: '10' },
		{ key: 20, value: '20' },
		{ key: 50, value: '50' },
		{ key: 100, value: '100' },
		{ key: -1, value: 'Todos' }
	];

	// Source
	public source: MyServerDataSourceService;

	constructor(private rotinasService: RotinasService, public router: Router, public injector: Injector) {}

	ngOnInit() {
		// Pode excluir ou editar?
		this.rotinasService.buscaRealizada.then(() => {
			// Verifica se possui formulário
			const hashForm = this.formPage.length;

			this.rotina = this.rotinasService.rotinas[this.routine].Rotina;

			// Define 90px de largura para a coluna da chave primária
			if (this.columns[this.primaryKey]) {
				this.columns[this.primaryKey].width = '90px';
			}

			// Verifica se tem formulário e permissão para incluir os botões de inclusão e edição
			this.settings.actions.add = hashForm && this.rotinasService.verificaPermissao(this.routine, 'Insere');
			this.settings.actions.edit = hashForm && this.rotinasService.verificaPermissao(this.routine, 'Edita');

			// Verifica se pode ser excluido e se há permissão
			this.settings.actions.delete = this.canDelete && this.rotinasService.verificaPermissao(this.routine, 'Exclui');

			// Seta as colunas
			this.settings.columns = this.columns;

			// Monta as configurações
			const dataSouceConf = {
				endPoint: this.apiMethod,
				dataKey: 'data',
				totalKey: 'x-total-count'
			};

			// Recebe os dados
			this.source = new MyServerDataSourceService(this.injector, dataSouceConf, this.params);

			// Atualiza as configurações
			this.settings = Object.assign({}, this.settings);
		});
	}

	/**
	 * onEdit
	 *
	 * Capta o clique no botão de edição
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param object
	 * @return void
	 */
	onEdit(event) {
		// Se houver uma função para barrar a manipulação
		if (this.barraManipulacao) {
			// Recebe seu retorno
			const rejeitado = this.barraManipulacao(event.data, 'edit');

			// Se vier uma mensagem
			if (rejeitado) {
				// Mostra a justificativa do barramento
				return this.source.myToaster.showErrorToast('Edição não permitida', rejeitado);
			}
		}

		// Recebe a rota do formulário
		const editNavigatePage = this.formPage;

		// Adiciona o id do registro
		editNavigatePage.push(event.data[this.primaryKey]);

		// Direciona para a tela de formulário
		this.router.navigate(editNavigatePage);
	}

	/**
	 * Capta o clique no botão de inclusão
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param event Evento do clique no botão de cadastro
	 */
	onAdd(event: any) {
		// Direciona para o formulário
		this.router.navigate(this.formPage);
	}

	/**
	 * Exclui um registro
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param event Evento do clique no botão de exclusão
	 */
	async onDelete(event: any) {
		// Se houver uma função para barrar a manipulação
		if (this.barraManipulacao) {
			// Recebe seu retorno
			const rejeitado = this.barraManipulacao(event.data, 'delete');

			// Se vier uma mensagem
			if (rejeitado) {
				// Mostra a justificativa do barramento
				return this.source.myToaster.showErrorToast('Exclusão não permitida', rejeitado);
			}
		}

		// Confirma a exclusão
		if (window.confirm('Deseja realmente excluir?')) {
			try {
				// Exibe o toast de carregamento
				this.loading = true;

				// Faz a requisição
				await this.source.datagridAPI.delete(this.apiMethod + event.data[this.primaryKey]);

				// Atualiza o grid
				this.source.refresh();
			} catch (error) {
				if (error.name === 'SequelizeForeignKeyConstraintError') {
					// Caso o erro seja de permição de chave estrangeira exibe erro.
					this.source.myToaster.showErrorToast('Este registro tem vínculos e não pode ser excluído.');
				} else {
					// Mostra o erro
					this.source.myToaster.showErrorToast('Exclusão não realizada', typeof error === 'string' ? error : null);
				}
			}

			this.loading = false;
		}
	}

	/**
	 * Altera o número de registros exibidos por página
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public setPerPage() {
		// Verifica se todos os registros são exibidos para esconder a páginação
		this.settings.pager.display = this.perPage > 0;

		// Atualiza o objeto para forçar a atualização do grid
		this.settings = Object.assign({}, this.settings);

		// Vai para a primeira página
		this.source.setPaging(1, this.perPage);
	}

	/**
	 * Monta a busca com todos os parâmetros, permitindo filtros adicionais
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public setParams(params: any) {
		this.params = params;

		// Percada cada filtro
		for (const key in this.params) {
			// Adiciona ele à fonte de dados
			this.source.params[key] = this.params[key];
		}

		// Atualiza os dados
		this.source.refresh();
	}

	/**
	 * Busca os dados para criar um xlsx
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	async exportXLS() {
		// Exibe o loading
		this.loading = true;

		try {
			// Busca os dados
			const dados = await this.source.dataXLS();

			// Joga os dados para a planilha
			const ws = XLSX.utils.json_to_sheet(dados);

			// Cria uma nova aba
			const wb = XLSX.utils.book_new();

			// Joga os dados das celulas para dentro da aba
			XLSX.utils.book_append_sheet(wb, ws, this.rotina);

			// Cria o arquivo com o nome "Rotina ano-mes-dia.xlsx"
			XLSX.writeFile(wb, this.rotina + ' ' + new Date().toISOString().substring(0, 10) + '.xlsx');

			// Capta o erro
		} catch (err) {
			// Mostra o toaster de erro
			this.source.myToaster.showErrorToast('Não foi possível gerar a planilha', err);
		}

		this.loading = false;
	}
}
