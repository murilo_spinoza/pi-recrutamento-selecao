import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonRenderComponent } from './button-render.component';

@NgModule({
  imports: [
  	CommonModule,
  ],
  declarations: [
  	ButtonRenderComponent
  ],
  entryComponents:[
  	ButtonRenderComponent
  ]
})
export class ButtonRenderModule { }