import { Component, Input, OnInit } from '@angular/core';
import { DatagridEventEmitter } from '../../../../../@core/services/datagrid-event-emitter';

@Component({
	template: `
		<button [class]="'renderButton ' + renderValue.Class" (click)="click()" *ngIf="renderValue.ShowTest(rowData)">
			<i class="{{renderValue.Icon}} {{ renderValue.ToggleIcon(rowData) }}" *ngIf="renderValue.Icon || renderValue.ToggleIcon(rowData) !== ''">
			</i>
			{{renderValue.Text}}
		</button>
	`,
	styleUrls: ['./button-render.component.scss']
})
export class ButtonRenderComponent implements OnInit {

	public renderValue;

	@Input() value: any;
	@Input() rowData: any;

	constructor(public datagridEventEmitter: DatagridEventEmitter) { }

	ngOnInit() {
		this.renderValue = this.value;
		if (!this.renderValue['ShowTest']) {
			this.renderValue['ShowTest'] = (row: any) => true;
		}

		if (!this.renderValue['ToggleIcon']) {
			this.renderValue['ToggleIcon'] = (row: any) => '';
		}
	}

	click() {
		this.rowData.renderAction = this.renderValue.Action;
		this.datagridEventEmitter.click.emit(this.rowData);
	}
}
