import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ContactFormComponent } from '../contact/contact-form.component';
import { SessionStorageService } from '../../../@core/services/session-storage.service';
import { UsuariosAPI } from '../../../@core/api/usuarios.api';

@Component({
	selector: 'ngx-header',
	styleUrls: ['./header.component.scss'],
	templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

	@Input() position = 'normal';

	public user: any = {};

	constructor(private sidebarService: NbSidebarService,
		private menuService: NbMenuService,
		private session: SessionStorageService,
		private router: Router,
		private usuariosAPI: UsuariosAPI,
		private modalService: NgbModal) {
	}

	ngOnInit() {
		this.user.Group = this.session.getUserGroup();
		this.user.Name = this.session.getUserName();
		this.user.UserId = this.session.getUserId();
	}

	public editProfile(fromSmallScreen = false) {

		if (fromSmallScreen)
			this.sidebarService.collapse('menu-sidebar');

		this.router.navigate(['paginas', 'usuarios', 'form', this.user.UserId]);
	}

	public goToHome() {
		this.router.navigate(['paginas']);
	}

	public toggleSidebar(): boolean {
		this.sidebarService.toggle(true, 'menu-sidebar');
		return false;
	}

	public contact() {
		const activeModal = this.modalService.open(ContactFormComponent, { size: 'lg', container: 'nb-layout', windowClass: 'contact-modal' });
	}

	public logout() {

		try {
			this.usuariosAPI.logout().then(() => {
				this.session.resetSession();
				this.router.navigate(['auth']);
			});
		} catch (err) {
			console.error('Erro no logout', err);
		}
	}
}
