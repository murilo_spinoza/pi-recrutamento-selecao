import { Routes } from '@angular/router';
import { AuthComponent } from './components/auth.component';
import { LoginComponent } from './components/login/login.component';
import { RequestPasswordComponent } from './components/request-password/request-password.component';

export const AuthRoutes: Routes = [
	{
		path: 'auth',
		component: AuthComponent,
		children: [
			{
				path: '',
				component: LoginComponent,
			},
			{
				path: 'login',
				component: LoginComponent,
			},
			{
				path: 'request-password',
				component: RequestPasswordComponent,
			},
		],
	},
];
