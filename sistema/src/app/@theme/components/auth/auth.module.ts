import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NbLayoutModule, NbCardModule } from '@nebular/theme';
import { AuthRoutes } from './auth.routes';
import { AuthComponent } from './components/auth.component';

import { AuthBlockComponent } from './components/auth-block/auth-block.component';
import { LoginComponent } from './components/login/login.component';
import { RequestPasswordComponent } from './components/request-password/request-password.component';

import { APIModule } from '@core/api/api.module';

const COMPONENTS = [AuthComponent, AuthBlockComponent, LoginComponent, RequestPasswordComponent];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(AuthRoutes), NbLayoutModule, NbCardModule, FormsModule, APIModule],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS]
})
export class AuthModule {}
