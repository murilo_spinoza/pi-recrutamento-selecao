import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { SessionStorageService } from '@core/services/session-storage.service';

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private router: Router, private session: SessionStorageService) { }

	canActivate() {
		if (this.session.getToken()) {
			return true;
		} else {
			this.router.navigate(['auth']);
			return false;
		}
	}
}
