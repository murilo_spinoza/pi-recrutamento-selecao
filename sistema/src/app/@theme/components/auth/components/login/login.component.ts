import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosAPI } from '@core/api/usuarios.api';
import { SessionStorageService } from '@core/services/session-storage.service';

@Component({
	selector: 'nb-login',
	styleUrls: ['./login.component.scss'],
	templateUrl: 'login.component.html',
})
export class LoginComponent {

	public showMessages: any = {};
	public errors: string[] = [];
	public user: any = {};
	public submitted = false;

	constructor(
		protected router: Router,
		protected usuariosAPI: UsuariosAPI,
		private session: SessionStorageService
	) { }

	login(form: any): void {
		this.errors = [];
		this.submitted = true;
		if (form.valid) {
			this.usuariosAPI.login(this.user).then((login) => {
				if (login.UsuarioGrupoId === 1) {
					this.session.setToken(login.Token);
					this.session.setUserId(login.UsuarioId);
					this.session.setUserName(login.Nome);
					this.session.setEmail(login.Email);
					this.session.setUserGroup(login.Grupo);
					this.session.setUserGroupId(login.UsuarioGrupoId);
					this.router.navigateByUrl('paginas');
				} else {
					this.showMessages.error = true;
					this.errors.push('Acesso negado');
					this.submitted = false;
				}
			}).catch(error => {
				console.log(error);
				this.showMessages.error = true;
				this.errors.push('E-mail ou senha inválidos');
				this.submitted = false;
			});
		} else {
			this.submitted = false;
		}
	}
}
