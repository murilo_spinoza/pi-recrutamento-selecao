import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosAPI } from '@core/api/usuarios.api';

@Component({
	selector: 'nb-request-password-page',
	styleUrls: ['./request-password.component.scss'],
	templateUrl: 'request-password.component.html',
})
export class RequestPasswordComponent {

	public submitted = false;
	public submitting = false;
	public typeMessage = '';
	public message = '';
	public email = '';

	constructor(
		protected router: Router,
		private usuariosAPI: UsuariosAPI
	) { }

	/**
	 * requestPass
	 *
	 * request password
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 */
	async requestPass(form: any) {

		// Verifica se o formulário é válido
		if (!form.valid) {
			return;
		}

		try {

			// Reinicia a mensagem de erro ou sucesso
			this.message = '';

			// Seta as variaveis de controle de requisição
			this.submitted = this.submitting = true;

			// Faz a requisição
			await this.usuariosAPI.forgetPassword(this.email);

			// Indica que a requisição terminou
			this.submitting = false;

			// Seta a mensagem de sucesso
			this.typeMessage = 'success';
			this.message = 'Verifique sua caixa de entrada para recuperar seu acesso.';

		} catch (error) {

			// Seta a mensagem de erro
			this.typeMessage = 'danger';
			this.message = error || 'Não foi possível conectar ao servidor';

			// Indica que a requisição terminou
			this.submitting = false;
		}
	}
}
