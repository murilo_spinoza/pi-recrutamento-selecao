import { Component } from '@angular/core';

@Component({
	selector: 'ngx-footer',
	styleUrls: ['./footer.component.scss'],
	template: `
		<span class="created-by">
			2019 ©
		</span>
		<div class="socials">
			<img src="assets/images/recruitment.svg" />
		</div>
	`
})

export class FooterComponent {}
