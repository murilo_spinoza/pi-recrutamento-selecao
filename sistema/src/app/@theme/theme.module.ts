import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactFormModule } from './components/contact/contact-form.module';
import { DatagridModule } from './components/datagrid/datagrid.module';
import { FileUploadModule } from 'ng2-file-upload';
import { NgSelectModule } from '@ng-select/ng-select'; // https://github.com/ng-select/ng-select
import { BrMaskerModule, BrMaskDirective } from 'br-mask'; // https://www.npmjs.com/package/br-mask
import { BrMaskService } from './services/br-mask.service';
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';

import {
	NbActionsModule,
	NbCardModule,
	NbLayoutModule,
	NbMenuModule,
	NbRouteTabsetModule,
	NbSearchModule,
	NbSidebarModule,
	NbTabsetModule,
	NbThemeModule,
	NbCalendarKitModule,
	NbCalendarModule,
	NbCalendarRangeModule,
	NbUserModule,
	NbCheckboxModule,
	NbPopoverModule,
	NbContextMenuModule,
	NbSpinnerModule
} from '@nebular/theme';

// import { NbSecurityModule } from '@nebular/security';

import {
	FooterComponent,
	HeaderComponent
} from './components';
import { PluralPipe, RoundPipe, TimingPipe } from './pipes';

import { ValidatorModule } from './directives/validator/Validator.module';
import { CORPORATE_THEME } from './styles/theme.corporate';

import { ButtonRenderModule } from './components/datagrid/render-components/button-render/button-render.module';

import { FieldErrorComponent } from './components/field-error/field-error.component';
import { PassosComponent } from './components/passos/passos.component';

const BASE_MODULES = [
	CommonModule,
	FormsModule,
	ReactiveFormsModule,
	ContactFormModule,
	DatagridModule,
	ButtonRenderModule,
	FileUploadModule,
	NgSelectModule,
	BrMaskerModule,
	HttpClientModule,
	AngularEditorModule,
];

const NB_MODULES = [
	NbCardModule,
	NbLayoutModule,
	NbTabsetModule,
	NbRouteTabsetModule,
	NbMenuModule,
	NbUserModule,
	NbActionsModule,
	NbSearchModule,
	NbSidebarModule,
	NbCheckboxModule,
	NbPopoverModule,
	NbContextMenuModule,
	NgbModule,
	NbCalendarModule,
	NbCalendarKitModule,
	NbCalendarRangeModule,
	ValidatorModule,
	NbSpinnerModule
	// NbSecurityModule, // *nbIsGranted directive
];

const COMPONENTS = [
	HeaderComponent,
	FooterComponent,
	FieldErrorComponent,
	PassosComponent
];

const ENTRY_COMPONENTS = [
];

const PIPES = [
	PluralPipe,
	RoundPipe,
	TimingPipe,
];

const NB_THEME_PROVIDERS = [
	...NbThemeModule.forRoot(
		{
			name: 'corporate',
		},
		[CORPORATE_THEME],
	).providers,
	...NbSidebarModule.forRoot().providers,
	...NbMenuModule.forRoot().providers,
	BrMaskDirective,
	BrMaskService
];

@NgModule({
	imports: [...BASE_MODULES, ...NB_MODULES],
	exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS, ...PIPES],
	declarations: [...COMPONENTS, ...PIPES],
	entryComponents: [...ENTRY_COMPONENTS],
})
export class ThemeModule {
	static forRoot(): ModuleWithProviders {
		return <ModuleWithProviders>{
			ngModule: ThemeModule,
			providers: [...NB_THEME_PROVIDERS],
		};
	}
}
