import { Injectable } from '@angular/core';
import { BrMaskDirective, BrMaskModel } from 'br-mask';

@Injectable({
	providedIn: 'root'
})
export class BrMaskService {

	constructor(public brMaskDirective: BrMaskDirective) { }

	/**
	 * create_phone
	 *
	 * Método chamado para aplicar a máscara em um determinado valor
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 * @param string número do telefone
	 * @return string telefone formatado
	 */
	public create_phone(phone: string): string {

		// Verifica se o que foi recebido é válido
		if (!phone || typeof phone !== 'string') {
			// Retorna o que foi recebido sem tratamento
			return phone;
		}

		// Instancia o model
		const maskModel: BrMaskModel = new BrMaskModel();

		// Indica que é um número de telefone
		maskModel.phone = true;

		// Formata o número recebido
		return this.brMaskDirective.writeCreateValue(phone, maskModel);
	}

	/**
	 * create_cpf
	 *
	 * Método chamado para aplicar a máscara em um determinado valor
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 * @param string número do cpf
	 * @return string cpf formatado
	 */
	public create_cpf(cpf: string): string {

		// Verifica se o que foi recebido é válido
		if (!cpf || typeof cpf !== 'string') {
			// Retorna o que foi recebido sem tratamento
			return cpf;
		}

		// Instancia o model
		const maskModel: BrMaskModel = new BrMaskModel();

		// Indica que é um número de telefone
		maskModel.person = true;

		// Formata o número recebido
		return this.brMaskDirective.writeCreateValue(cpf, maskModel);
	}

	/**
	 * create_money
	 *
	 * Método chamado para aplicar a máscara em um determinado valor
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 * @param string número do cpf
	 * @return string cpf formatado
	 */
	public create_money(number: any): string {

		// Instancia o model
		const maskModel: BrMaskModel = new BrMaskModel();

		// Indica que é um número de telefone
		maskModel.money = true;
		maskModel.moneyInitHasInt = true;
		maskModel.decimal = 2;
		maskModel.decimalCaracter = ',';
		maskModel.thousand = '.';

		// Formata o número recebido
		return this.brMaskDirective.writeCreateValue(number, maskModel);
	}
}
