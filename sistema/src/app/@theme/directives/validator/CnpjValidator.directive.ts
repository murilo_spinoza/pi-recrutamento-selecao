import {
    ReactiveFormsModule,
    NG_VALIDATORS,
    FormsModule,
    FormGroup,
    FormControl,
    ValidatorFn,
    Validator
} from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[cnpj-validator][ngModel][required]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: CnpjValidator,
            multi: true
        }
    ]
})

export class CnpjValidator implements Validator {

    @Input('required') required: boolean;
    validator: ValidatorFn;
    constructor() {
        this.validator = this.cnpjValidator();
    }

    public validate(c: FormControl) {
        return this.validator(c);
    }

    public cnpjValidator(): ValidatorFn {
        return (c: FormControl) => {

            let clean = (c.value) ? c.value.replace(/\.|\-|\//g, '') : '';
            let isValid = (!this.required && clean.length === 0) ? true : this.__checkCNPJ(clean);

            if (isValid) {
                return null;
            } else {
                return {
                    cnpjValidator: {
                        valid: false
                    }
                };
            }
        }
    }

	/*
	* validate
	* Valida o cnpj
	*
	* @author William Oliveira
	* @since 05/07/2018
	*
	* @params strCNPJ tipo any
	* @return retorna true se cnpj for válido e false caso contrário
	*/
    private __checkCNPJ(strCNPJ: any): boolean {

        if (!strCNPJ) return false;

        let b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
        let n;
        let i;

        if ((strCNPJ = strCNPJ.replace(/[^\d]/g, "")).length != 14)
            return false;

        if (/0{14}/.test(strCNPJ))
            return false;

        for (i = 0, n = 0; i < 12; n += strCNPJ[i] * b[++i]);
        if (strCNPJ[12] != (((n %= 11) < 2) ? 0 : 11 - n))
            return false;

        for (i = 0, n = 0; i <= 12; n += strCNPJ[i] * b[i++]);
        if (strCNPJ[13] != (((n %= 11) < 2) ? 0 : 11 - n))
            return false;


        return true;
    }
}