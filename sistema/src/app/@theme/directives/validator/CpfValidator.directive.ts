import {
    ReactiveFormsModule,
    NG_VALIDATORS,
    FormsModule,
    FormGroup,
    FormControl,
    ValidatorFn,
    Validator
} from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[cpf-validator][ngModel][required]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: CpfValidator,
            multi: true
        }
    ]
})

export class CpfValidator implements Validator {

    @Input('required') required: boolean;
    validator: ValidatorFn;

    constructor() {
        this.validator = this.cpfValidator();
    }

    public validate(c: FormControl) {
        return this.validator(c);
    }

    public cpfValidator(): ValidatorFn {
        return (c: FormControl) => {

            let clean = (c.value) ? c.value.replace(/\.|\-/g, '') : '';
            let isValid = (!this.required && clean.length === 0) ? true : this.__checkCPF(clean);

            if (isValid) {
                return null;
            } else {
                return {
                    cpfValidator: {
                        valid: false
                    }
                };
            }
        }
    }

	/**
     * __checkCPF
     *
     * Valida CPF
     * Exemplo e regras do site da receita federal
     *
     * @public
     * @author   Murilo Spinoza
     * @param    {string} cpf
     * @return   {Promise}
     */
    private __checkCPF(cpf: string): boolean {

        if (!cpf) return false;

        var sum: number = 0;
        var mod: number;

        // Verifica se todos os digitos não não iguais
        let checkRepeat: number = 0;
        let lastChar: string = '';
        for (let i = 0; i < cpf.length; i++) {
            if (cpf[i] == lastChar || lastChar == '')
                checkRepeat++;
            else
                checkRepeat = 0;

            lastChar = cpf[i];
        }
        if (checkRepeat == 11)
            return false;


        // Primeiro digito verificador
        for (let i = 1; i <= 9; i++)
            sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (11 - i);

        mod = (sum * 10) % 11;

        if ((mod == 10) || (mod == 11)) mod = 0;
        if (mod != parseInt(cpf.substring(9, 10), 10)) return false;

        // Segundo digito verificador
        sum = 0;
        for (let i = 1; i <= 10; i++)
            sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (12 - i);

        mod = (sum * 10) % 11;

        if ((mod == 10) || (mod == 11)) mod = 0;
        if (mod != parseInt(cpf.substring(10, 11), 10)) return false;
        return true;
    }
}