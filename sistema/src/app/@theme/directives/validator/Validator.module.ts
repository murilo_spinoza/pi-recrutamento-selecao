import { Injector, NgModule } from '@angular/core';
import { DateOnlyValidator } from './DateOnlyValidator.directive';
import { EmailValidator } from './EmailValidator.directive';
import { CpfValidator } from './CpfValidator.directive';
import { CnpjValidator } from './CnpjValidator.directive';

@NgModule({
	declarations: [
		DateOnlyValidator,
		EmailValidator,
		CpfValidator,
		CnpjValidator
	],
	exports: [
		DateOnlyValidator,
		EmailValidator,
		CpfValidator,
		CnpjValidator
	]
})
export class ValidatorModule {}