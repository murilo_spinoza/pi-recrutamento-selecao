import {
		ReactiveFormsModule,
		NG_VALIDATORS,
		FormsModule,
		FormGroup,
		FormControl,
		ValidatorFn,
		Validator
} from '@angular/forms';
import { Directive } from '@angular/core';
import * as moment from 'moment';

@Directive({
	selector: '[dateonly-validator][ngModel]',
	providers: [
		{
			provide: NG_VALIDATORS,
			useExisting: DateOnlyValidator,
			multi: true
		}
	]
})
export class DateOnlyValidator implements Validator {

	validator: ValidatorFn;
	constructor() {
		this.validator = this.dateOnlyValidator();
	}

	validate(c: FormControl) {
		return this.validator(c);
	}

	dateOnlyValidator(): ValidatorFn {
		return (c: FormControl) => {
			let dtFormat = moment(c.value, 'DD/MM/YYYY').format('YYYY-MM-DD');
			let isValid = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(dtFormat) && moment(dtFormat).isValid();
			if (isValid) {
				return null;
			} else {
				return {
					dateOnlyValidator: {
						valid: false
					}
				};
			}
		}
	}
}