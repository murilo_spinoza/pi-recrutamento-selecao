import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import {
	AuthGuard,
	AuthComponent,
	LoginComponent,
	RequestPasswordComponent
} from './@theme/components/auth';

const routes: Routes = [
	// routes
	{
		path: 'auth',
		component: AuthComponent,
		children: [
			{
				path: '',
				component: LoginComponent,
			},
			{
				path: 'login',
				component: LoginComponent,
			},
			{
				path: 'request-password',
				component: RequestPasswordComponent,
			},
		],
	},
	{ path: 'paginas', canActivate: [AuthGuard], loadChildren: './pages/pages.module#PagesModule' },
	{ path: '', redirectTo: 'auth', pathMatch: 'full' }
];

const config: ExtraOptions = {
	useHash: false,
};

@NgModule({
	imports: [RouterModule.forRoot(routes, config)],
	exports: [RouterModule],
})
export class AppRoutingModule { }
