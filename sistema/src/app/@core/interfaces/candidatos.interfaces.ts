export interface ICandidatoPontos {
	ContratacaoId?: number;
	CandidatoId?: number;
	UsuarioId?: number;
	Pontos?: number;
	Motivo?: string;
	Origem?: 'S' | 'F';
	Created: string;
}

export interface ICandidatoTriagem {
	CandidatoId?: number;
	Nome?: string;
	Email?: string;
	Idade?: number;
	Pcd?: string;
	Habilidades?: {
		HabilidadeId: number;
		Habilidade: string;
		Nivel: number;
	}[];
	Pontos?: number;
	CandidatoPontos?: ICandidatoPontos[];
}
