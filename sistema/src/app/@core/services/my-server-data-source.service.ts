import { LocalDataSource } from 'ng2-smart-table/lib/data-source/local/local.data-source';
import { ServerSourceConf } from 'ng2-smart-table/lib/data-source/server/server-source.conf';
import { getDeepFromObject } from 'ng2-smart-table/lib/helpers';

import { DatagridAPI } from '../api/datagrid.api';
import { MyToasterService } from './my-toaster.service';
import { Injectable, Injector } from '@angular/core';

export class MyServerDataSourceService extends LocalDataSource {
	// Configurações
	protected conf: ServerSourceConf;

	// Última contagem total de registros
	public lastRequestCount = 0;

	// Contagem de registros exibidos
	public rowsRequestCount = 0;

	// Carregando
	public loading = true;

	// Carregamento inicial realizado?
	public initialized = false;

	public datagridAPI: DatagridAPI;
	public myToaster: MyToasterService;

	constructor(public injector: Injector, conf: ServerSourceConf | { [key: string]: any } = {}, public params: any) {
		super();

		this.__inject();

		// Instancia um novo source config
		this.conf = new ServerSourceConf(conf);

		if (!this.conf.endPoint) {
			throw new Error('Endpoint não definido');
		}
	}

	/**
	 * Realiza as injeções de dependência para as
	 * classes que instanciarem não precisarem
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  04/2019
	 */
	private async __inject() {
		// Busca o provider da api do datagrid
		this.datagridAPI = this.injector.get(DatagridAPI);

		// Busca o provider de toasters
		this.myToaster = this.injector.get(MyToasterService);
	}

	/**
	 * count
	 *
	 * Usado internamente pelo grid para fazer a paginação, não remova kasndk
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @return int
	 */
	public count() {
		// Retorna a ultima contagem
		return this.lastRequestCount;
	}

	/**
	 * getElements
	 *
	 * Realiza a busca pelos registros
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @return int
	 */
	public async getElements(): Promise<any> {
		try {
			// Atualiza o estado do loading
			this.loading = true;

			// Faz a requisição
			const res = await this._requestElements();

			this.data = res.data;
			this.lastRequestCount = res.lastRequestCount;
			this.rowsRequestCount = res.rowsRequestCount;

			this.loading = false;
			this.initialized = true;

			return this.data;
		} catch (err) {
			this.myToaster.showErrorToast('Não foi possível buscar os registros', err);
		}
	}

	/**
	 * dataXLS
	 *
	 * Realiza a busca pelos registros para o xls
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @return int
	 */
	public async dataXLS(): Promise<any> {
		// Faz a requisição
		const res = await this._requestElements(true);

		// Retorna os dados
		return res.data;
	}

	/**
	 * Extracts array of data from server response
	 * @param res
	 * @returns {any}
	 */
	protected extractDataFromResponse(rawData: any): Array<any> {
		const data = !!this.conf.dataKey ? getDeepFromObject(rawData, this.conf.dataKey, []) : rawData;

		if (data instanceof Array) {
			return data;
		}

		throw new Error(`Os dados devem ser uma lista. Verifique a chave '${this.conf.dataKey}' da resposta da requisição.`);
	}

	/**
	 * Monta os parâmetros e realiza a requisição
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected async _requestElements(forXls: boolean = false) {
		const body = this._createRequestParams();

		let res: any;

		if (forXls) {
			res = await this.datagridAPI.dataXLS(this.conf.endPoint, body);
		} else {
			res = await this.datagridAPI.get(this.conf.endPoint, body);
		}

		const retorno: any = {};

		// Extrai os registros da requisição
		retorno.data = this.extractDataFromResponse(res);

		// Retira o total de linhas encontradas na ultima busca
		retorno.lastRequestCount = getDeepFromObject(res, this.conf.totalKey, 0);

		// Conta o número de linhas exibidas no grid
		retorno.rowsRequestCount = retorno.data.length;

		// Retira os formatadores de campos do retorno
		const formatter = getDeepFromObject(res, 'formatter', false);

		// Se houver um formatador de dados
		if (formatter) {
			// Parcorre cada formatador
			for (const format in formatter) {
				// Percorre cada indice
				for (const i in retorno.data) {
					// Se na linha houver este item
					if (retorno.data[i][format]) {
						// Recebe o valor do objeto ou devolve o dado normal
						retorno.data[i][format] = formatter[format][retorno.data[i][format]] || retorno.data[i][format];
					}
				}
			}
		}

		return retorno;
	}

	/**
	 * _createRequestParams
	 *
	 * Monta o objeto com a ordenação, filtragem e paginação
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	protected _createRequestParams() {
		const httpParams = {};

		// SORT
		if (this.sortConf) {
			this.sortConf.forEach(fieldConf => {
				httpParams[this.conf.sortFieldKey] = fieldConf.field;
				httpParams[this.conf.sortDirKey] = fieldConf.direction.toUpperCase();
			});
		}

		// FILTER
		if (this.filterConf.filters) {
			this.filterConf.filters.forEach((fieldConf: any) => {
				if (fieldConf['search']) {
					httpParams[this.conf.filterFieldKey.replace('#field#', fieldConf['field'])] = fieldConf['search'];
				}
			});

			for (const key in this.params) {
				if (this.params[key].toString().length) {
					httpParams[key] = this.params[key];
				}
			}
		}

		// PAGING
		if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
			httpParams[this.conf.pagerPageKey] = this.pagingConf['page'];
			httpParams[this.conf.pagerLimitKey] = this.pagingConf['perPage'];
		}

		return httpParams;
	}
}
