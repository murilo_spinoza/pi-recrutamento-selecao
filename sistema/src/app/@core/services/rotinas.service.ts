import { RotinasAPI } from '../api/rotinas.api';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class RotinasService {
	public rotinasClassificacoes = {};
	public rotinasClassificacoesOrdem = [];
	public buscaRealizada: Promise<any>;
	public rotinas = {};
	public items = [];
	public delegate: RotinasDelegate;

	constructor(private rotinasAPI: RotinasAPI) {}

	getMenu() {
		this.buscaRealizada = new Promise<any>((resolve, reject) => {
			// Busca o menu disponível para o usuário logado
			this.rotinasAPI
				.lista()
				.then(rotinas => {
					this.rotinasClassificacoes = {};
					this.rotinasClassificacoesOrdem = [];
					this.rotinas = {};
					this.items = [];

					// Para cada item de rotina buscado
					for (const item of rotinas) {
						// Se vier de rotinas trata menu
						if (!item.icon) {
							// Se a classificação nao tiver sido recebida ainda
							if (!this.rotinasClassificacoes[item.RotinaClassificacaoId]) {
								// Inicia o campo dentro da classificacao para armazenar as chaves das rotinas
								item.RotinaClassificacao['Rotinas'] = [];

								// armazena a classificação
								this.rotinasClassificacoes[item.RotinaClassificacaoId] = item.RotinaClassificacao;
							}

							// Exclui a classificação da rotina, deixando só o id dela
							delete item.RotinaClassificacao;

							// Recebe a permissão que vem na primeira posição da lista
							item.Permissao = item.Permissao[0];

							// Armazena a rotina com o link como chave
							this.rotinas[item.Link] = item;

							// Armazena o link da rotina na classificacao
							this.rotinasClassificacoes[item.RotinaClassificacaoId]['Rotinas'].push(item.Link);

							if (this.rotinasClassificacoesOrdem.indexOf(item.RotinaClassificacaoId) < 0) {
								this.rotinasClassificacoesOrdem.push(item.RotinaClassificacaoId);
							}
						} else {
							// Itens fixos
							this.items.push(item);
						}
					}

					// Dispara notificação de mudança no menu
					if (this.delegate) {
						this.delegate.onMenuChange();
					}

					resolve(true);
				})
				.catch(erro => {
					console.error(erro);
					reject(erro);
				});
		});
	}

	/**
	 * verificaPermissao
	 *
	 * Verifica se o usuário logado tem uma determinada permissão
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  01/2018
	 * @param  string
	 * @param  string [Consulta, Edita, Insere, Exclui]
	 * @return boolean
	 */
	verificaPermissao(area, tipo) {
		return this.rotinas[area] && this.rotinas[area].Permissao && this.rotinas[area].Permissao[tipo] === 'S';
	}

	/**
	 * retornaClassificacao
	 *
	 * Retorna o texto da classificação
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  01/2018
	 * @param  string
	 * @return object Promise
	 */
	retornaClassificacao(area) {
		if (!this.rotinas[area]) {
			return false;
		}

		const classificacaoId = this.rotinas[area]['RotinaClassificacaoId'];
		return this.rotinasClassificacoes[classificacaoId].Classificacao;
	}
}

export interface RotinasDelegate {
	onMenuChange(): any;
}
