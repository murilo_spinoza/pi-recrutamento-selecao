import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class DatagridEventEmitter {
	public click = new EventEmitter<any>();

	constructor() {}
}
