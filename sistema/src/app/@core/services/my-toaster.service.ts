import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class MyToasterService {
	constructor(private toasterService: ToasterService) {}

	private __normalConfig = {
		positionClass: 'toast-top-center',
		timeout: 9000000,
		newestOnTop: true,
		tapToDismiss: true,
		preventDuplicates: true,
		animation: 'slideUp',
		limit: 5,
		showCloseButton: true,
		bodyOutputType: BodyOutputType.TrustedHtml
	};

	public loading = false;

	/**
	 * Exibe um novo toast
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 * @param  type Cor do toaster
	 * @param  title Título da mensagem do toaster
	 * @param  body Mensagem do toaster
	 */
	public showToast(type: string, title: string, body: string) {
		const toast: Toast = {
			type: type,
			title: title,
			body: body
		};

		this.toasterService.popAsync(toast);
	}

	/**
	 * Exibe um novo toast do tipo success
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 * @param  title Título da mensagem do toaster
	 * @param  body Mensagem do toaster
	 */
	public showSuccessToast(title: string, body: string = '') {
		this.showToast('success', title, body);
	}

	/**
	 * Exibe um novo toast do tipo warning
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 * @param  title Título da mensagem do toaster
	 * @param  body Mensagem do toaster
	 */
	public showWarningToast(title: string, body: string = '') {
		this.showToast('warning', title, body);
	}

	/**
	 * Exibe um novo toast do tipo error
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 * @param  title Título da mensagem do toaster
	 * @param  body Mensagem do toaster
	 */
	public showErrorToast(title: string, body: string = '') {
		this.showToast('error', title, body);
	}

	/**
	 * Exibe um toast para indicar o erro de preenchimento do formulario
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 */
	public showCompletionToast() {
		this.showWarningToast('Por favor, verifique o preenchimento dos campos', '');
	}

	/**
	 * Exibe um toast para indicar um erro na busca de dados
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 * @param error Mensagem de erro
	 */
	public showGetErrorToast(error: string) {
		this.showErrorToast('Ocorreu um erro ao buscar os dados', error);
	}

	/**
	 * Exibe um toast para indicar o sucesso do salvar
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 */
	public showSaveSuccessToast() {
		this.showSuccessToast('Dados salvos com sucesso', '');
	}

	/**
	 * Exibe um toast para indicar o sucesso do salvar
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2018
	 * @param error Mensagem de erro
	 */
	public showSaveErrorToast(error: string) {
		this.showErrorToast('Dados não salvos', error);
	}
}
