import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class SessionStorageService {
	constructor() {}

	private __get(key: string) {
		return JSON.parse(localStorage.getItem(key));
	}

	private __set(key: string, value: string) {
		localStorage.setItem(key, JSON.stringify(value));
	}

	getToken() {
		return this.__get(environment.token);
	}

	setToken(value: any) {
		this.__set(environment.token, value);
	}

	getUserId() {
		return this.__get(environment.idKey);
	}

	setUserId(value: any) {
		this.__set(environment.idKey, value);
	}

	getUserName() {
		return this.__get('user_name');
	}

	setUserName(value: any) {
		this.__set('user_name', value);
	}

	getEmail() {
		return this.__get('user_email');
	}

	setEmail(value: any) {
		this.__set('user_email', value);
	}

	getUserGroupId() {
		return this.__get('group_id');
	}

	setUserGroupId(value: any) {
		this.__set('group_id', value);
	}

	getUserGroup() {
		return this.__get('user_group');
	}

	setUserGroup(value: any) {
		this.__set('user_group', value);
	}

	getLanguage() {
		return this.__get('language_default');
	}

	setLanguage(value: any) {
		this.__set('language_default', value);
	}

	resetSession() {
		localStorage.removeItem(environment.token);
		localStorage.removeItem(environment.idKey);
	}
}
