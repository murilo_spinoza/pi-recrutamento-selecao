import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbSecurityModule } from '@nebular/security';
// Services
import { AuthModule } from '../@theme/components/auth/auth.module';
import { APIModule } from './api/api.module';
import { DatagridEventEmitter } from './services/datagrid-event-emitter';
import { MyToasterService } from './services/my-toaster.service';
import { RotinasService } from './services/rotinas.service';
import { SessionStorageService } from './services/session-storage.service';

@NgModule({
	imports: [CommonModule],
	exports: [CommonModule, AuthModule, BrowserAnimationsModule, APIModule],
	providers: [
		NbSecurityModule.forRoot({
			accessControl: {
				guest: {
					view: '*'
				},
				user: {
					parent: 'guest',
					create: '*',
					edit: '*',
					remove: '*'
				}
			}
		}).providers,
		RotinasService,
		MyToasterService,
		SessionStorageService,
		DatagridEventEmitter,
		DatePipe
	],
	declarations: []
})
export class CoreModule {}
