import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class CursosAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'cursos');
	}
}
