import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class DatagridAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, '');
	}

	/**
	 * Faz a requisição para listagem dos registros
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  endPoint controller/method que complementam a url da api
	 * @param  data Dados que serão enviados na requisição
	 */
	public get(endPoint: string, data: { [key: string]: any }) {
		return this._call(endPoint, data, 'get');
	}

	/**
	 * dataXLS
	 *
	 * Faz a requisição de dados para o XLS
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  endPoint controller/method que complementam a url da api
	 * @param  data Dados que serão enviados na requisição
	 */
	public dataXLS(endPoint: string, data: { [key: string]: any }) {
		return this._call(endPoint + '/data_xls', data, 'get');
	}

	/**
	 * delete
	 *
	 * Faz a requisição para delete
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  endPoint controller/method que complementam a url da api
	 */
	delete(endPoint: string) {
		return this._call(endPoint, {}, 'delete');
	}
}
