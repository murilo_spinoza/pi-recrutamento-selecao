import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ContratacoesAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'contratacoes');
	}

	public analisar(contratacaoId: number, status: 'A' | 'R', rejeicao?: string) {
		return this._call(this._entity + '/analisar', {
			contratacaoId,
			status,
			rejeicao
		});
	}
}
