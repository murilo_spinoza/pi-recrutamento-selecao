import { NgModule } from '@angular/core';
import { AuthHttpModule } from '../utils/auth-http.module';
import { UsuariosAPI } from './usuarios.api';
import { RotinasAPI } from './rotinas.api';
import { DatagridAPI } from './datagrid.api';
import { UsuariosGruposAPI } from './usuarios_grupos.api';
import { CargosAPI } from './cargos.api';
import { CandidatosAPI } from './candidatos.api';
import { CursosAPI } from './cursos.api';
import { HabilidadesAPI } from './habilidades.api';
import { TestesAPI } from './testes.api';
import { RedesSociaisAPI } from './redes_sociais.api';
import { GrausEscolaridadesAPI } from './graus_escolaridades.api';
import { QuadroVagasAPI } from './quadro_vagas.api';
import { ProcessosSeletivosAPI } from './processos_seletivos.api';

@NgModule({
	imports: [AuthHttpModule],
	providers: [
		UsuariosAPI,
		RotinasAPI,
		UsuariosGruposAPI,
		CargosAPI,
		CandidatosAPI,
		CursosAPI,
		HabilidadesAPI,
		TestesAPI,
		RedesSociaisAPI,
		GrausEscolaridadesAPI,
		QuadroVagasAPI,
		DatagridAPI,
		ProcessosSeletivosAPI
	]
})
export class APIModule {}
