import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class CandidatosAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'candidatos');
	}

	public salvaObservacoes(candidatoId: number, observacoes?: string) {
		return this._call(this._entity + '/salvaObservacoes', {
			candidatoId,
			observacoes
		});
	}
}
