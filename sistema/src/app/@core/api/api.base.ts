import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { SessionStorageService } from '../services/session-storage.service';
import { Injector, Injectable } from '@angular/core';

export abstract class APIBase {
	protected _http: HttpClient;
	protected _router: Router;
	protected _session: SessionStorageService;
	protected _url = environment.api;

	/**
	 * Realiza as injeções de dependência
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	constructor(protected injector: Injector, protected _entity: string) {
		// Busca o provider _http
		this._http = this.injector.get(HttpClient);

		// Busca o provider _router
		this._router = this.injector.get(Router);

		// Busca o provider _session
		this._session = this.injector.get(SessionStorageService);
	}

	/**
	 * Analisa e trata o sucesso na requisição
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  body Retorno da requisição
	 */
	private __success(body: any): any {
		// Verifica se retornou um erro
		if (!body.status) {
			throw body.error;
		}

		return body.data;
	}

	/**
	 * Analisa e trata o erro na requisição
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  error Objeto retornado pela requisição
	 */
	private __error(error: any) {
		let errorMsg = error.message ? error.message : error.status ? error.status + '-' + error.statusText : 'Erro na requisição';

		if (error.status === 401) {
			errorMsg = 'Sessão encerrada';
			this._router.navigate(['auth']);
		}

		if (error.status === 403) {
			errorMsg = 'Você não tem permissão para realizar esta ação';
			this._router.navigate(['paginas', 'dashboard']);
		}

		throw errorMsg;
	}

	/**
	 * Realiza uma chamada a API
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  method Complemento da url da api
	 * @param  body Dados que serão enviados na requisição
	 * @param  requestMethod Tipo de requisição: get, post, put, delete
	 */
	protected async _call(method: string, body?: { [key: string]: any }, requestMethod = 'post') {
		// Variável para conter o cabeçalho da requisição
		const customHeaders: any = {
			'Content-Type': 'application/json'
		};

		// Verifica se o usuário está logado
		if (this._session.getUserId()) {
			// Passa o token de autenticação
			customHeaders['Authorization'] = 'Bearer ' + this._session.getToken();

			// Indica qual é o usuário logado
			customHeaders['usuario_id'] = String(this._session.getUserId());
		}

		// Monta o cabeçalho da requisição
		const httpOptions = {
			headers: new HttpHeaders(customHeaders)
		};

		// Monta a url
		let url = this._url + '/' + method;

		// Variável para conter as informações da requisição
		let request: Promise<any>;

		// Analisa o método
		// Monta a requisição/url/params para enviar a api com base no tipo de requisição
		switch (requestMethod.toLowerCase()) {
			case 'get':
				// Monta os parâmetros da requisição como query string
				let conector = '?';
				for (const key in body) {
					url += conector + key + '=' + body[key];
					conector = '&';
				}

				// Recebe o rerquest
				request = this._http.get(url, httpOptions).toPromise();
				break;

			case 'delete':
				// Recebe o rerquest
				request = this._http.delete(url, httpOptions).toPromise();
				break;

			case 'put':
				// Recebe o rerquest
				request = this._http.put(url, body, httpOptions).toPromise();
				break;

			case 'post':
				// Recebe o rerquest
				request = this._http.post(url, body, httpOptions).toPromise();
				break;

			default:
				throw 'Método ' + requestMethod + ' inválido';
		}

		// Variável para conter o retorno
		let response: any;

		try {
			// Recebe o retorno da api e trata
			response = await request;
		} catch (error) {
			this.__error(error);
		}

		// Devolve os dados obtidos
		return this.__success(response);
	}

	/**
	 * Realiza a busca de um item especifico e traz os dados necessários para a edição ou cadastro
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  id Id de um determinado registo ou zero
	 */
	public setForm(id: number, finalidade?: any) {
		return this._call(this._entity + '/set_form/', { id, finalidade }, 'get');
	}

	/**
	 * Faz uma requisição do tipo put ou post se tiver ou não id
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  data Dados a serem enviados na requisição
	 */
	public save(data: { [key: string]: any }) {
		return this._call(this._entity, data, data.id ? 'put' : 'post');
	}
}
