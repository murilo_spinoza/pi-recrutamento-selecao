import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ProcessosSeletivosAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'processos_seletivos');
	}

	public contratacaoCandidatosTriagem(contratacaoId: number) {
		return this._call(this._entity + '/contratacaoCandidatosTriagem', { contratacaoId }, 'get');
	}

	public contratacaoCandidatosTestes(contratacaoId: number) {
		return this._call(this._entity + '/contratacaoCandidatosTestes', { contratacaoId }, 'get');
	}

	public contratacaoCandidatosEntrevistas(contratacaoId: number) {
		return this._call(this._entity + '/contratacaoCandidatosEntrevistas', { contratacaoId }, 'get');
	}

	public contratacaoCandidatosAprovados(contratacaoId: number) {
		return this._call(this._entity + '/contratacaoCandidatosAprovados', { contratacaoId }, 'get');
	}

	public analisaFase(fase: string, contratacaoId: number, candidatoId: number, reprova: string) {
		if (reprova) {
			return this._call(this._entity + '/reprova' + fase, { contratacaoId, candidatoId, reprova });
		} else {
			return this._call(this._entity + '/aprova' + fase, { contratacaoId, candidatoId });
		}
	}

	public concluiFase(fase: string, contratacaoId: number) {
		return this._call(this._entity + '/conclui' + fase, { contratacaoId });
	}

	public notaFase(fase: 'Testes' | 'Entrevistas', contratacaoId: number, candidatoId: number, nota: number) {
		return this._call(this._entity + '/nota' + fase, { contratacaoId, candidatoId, nota });
	}
}
