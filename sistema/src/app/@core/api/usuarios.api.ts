import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class UsuariosAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'usuarios');
	}

	/**
	 * Realiza o login
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  user E-mail e senha do usuário que está efetuando login
	 */
	public login(user: any) {
		return this._call(this._entity + '/login', user);
	}

	/**
	 * Realiza o logout
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public logout() {
		return this._call(this._entity + '/logout', {});
	}

	/**
	 * Dispara o e-mail de contato
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  data Informações do formulário de contato
	 */
	public contact(data) {
		return this._call(this._entity + '/contact', data);
	}

	/**
	 * Recupera a senha do usuário
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param email E-mail de quem quer recuperar sua senha
	 */
	async forgetPassword(email: string) {
		return this._call(this._entity + '/forgot_password', { Email: email });
	}
}
