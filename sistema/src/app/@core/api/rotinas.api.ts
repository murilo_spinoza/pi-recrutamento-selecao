import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class RotinasAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'rotinas');
	}

	/**
	 * Busca o menu e permissões disponíveis relacionados ao usuário logado
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public lista() {
		return this._call(this._entity + '/lista', {}, 'get');
	}
}
