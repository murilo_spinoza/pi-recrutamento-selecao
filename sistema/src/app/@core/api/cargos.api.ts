import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class CargosAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'cargos');
	}
}
