import { APIBase } from './api.base';
import { Injectable, Injector } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class QuadroVagasAPI extends APIBase {
	constructor(protected injector: Injector) {
		super(injector, 'quadro_vagas');
	}
}
