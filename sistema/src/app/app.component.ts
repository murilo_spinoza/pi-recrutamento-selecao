import { Component } from '@angular/core';
import { NgSelectConfig } from '@ng-select/ng-select'; // https://github.com/ng-select/ng-select

@Component({
	selector: 'app-root',
	template: `
		<toaster-container></toaster-container>
		<router-outlet></router-outlet>
	`
})
export class AppComponent {

	constructor(private ngSelectConfig: NgSelectConfig) {
		this.ngSelectConfig.notFoundText = 'Nenhum item encontrado';
		this.ngSelectConfig.placeholder = 'Nenhum item selecionado';
	}
}
