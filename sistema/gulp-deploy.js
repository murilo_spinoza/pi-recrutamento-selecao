var tar     = require('gulp-tar');
var gzip    = require('gulp-gzip');
var gulpSSH = require('gulp-ssh');
var fs      = require('fs');
var exec    = require('child_process').exec;
var moment  = require('moment');
var pump    = require('pump');
var q       = require('q');

/**
 * GulpDeploy
 *
 * Automatização de deploy com o gulp
 *
 * @access public
 * @author Murilo Spinoza
 */
var GulpDeploy = (function () {

    //Configurações
    var params = {
        'host'            : '',
        'port'            : 22,
        'username'        : 'ubuntu',
        'projectPath'     : '',
        'projectRealPath' : '',
        'projectName'     : '',
        'key'             : '',
        'env'             : 'stage'
    };

    //Instância do gulp
    var gulp = false;

    //Instância do SSH
    var ssh = false;

    /**
     * GulpDeploy
     *
     * Construtor da classe
     *
     * @access public
     * @author Murilo Spinoza
     */
    function GulpDeploy(objGulp, config){
        gulp   = objGulp;
        params = config;

        //Monta os parâmetros do ssh
        var config = {
            host       : params['host'],
            port       : params['port'],
            username   : params['username'],
            privateKey : fs.readFileSync(params['key'])
        }

        //Monta o path do projeto
        params['projectRealPath'] = '/app/'+params['projectName']+'/releases/';

        //Inicia
        ssh = new gulpSSH({
            ignoreErrors: true,
            sshConfig: config
        });
    }

    /**
     * setup
     *
     * Executa um setup no servidor passado
     *
     * @access public
     * @author Murilo Spinoza
     * @return Object
     */
    GulpDeploy.prototype.setup = function() {
    
        //Executa a criação das pastas
        return ssh
            .shell(['mkdir -p '+params['projectRealPath']], {filePath: 'deploy.log'})
            .pipe(gulp.dest('logs'));
    }

    /**
     * targz
     *
     * Executa um tar e um gzip na pasta do projeto
     *
     * @access public
     * @author Murilo Spinoza
     * @return string
     */
    GulpDeploy.prototype.targz = function() {

        //Inicia a promessa
        var deferred = q.defer();

        //Executa o build
        console.log('Executando o build no Angular...')
        exec('ng build --prod --aot', {maxBuffer: 1024 * 1500}, function (err, stdout, stderr) {
            if(err){
                console.error(err);
                deferred.reject(false);
            }

            //Compacta tudo
            console.log('Compactando o projeto...')
            pump([
                 gulp.src(['dist/**/*']),
                 tar('dist.tar'),
                 gzip(),
                 gulp.dest('/var/tmp/') 
                ],
                false,
                deferred.resolve('/var/tmp/dist.tar.gz')
            );
        });
        

        //Retorna a promessa
        return deferred.promise;
    }

    /**
     * deploy
     *
     * Executa o deploy
     *
     * @access public
     * @author Murilo Spinoza
     * @return object
     */
    GulpDeploy.prototype.deploy = function() {

        //Inicia as variáveis
        var folderDest = params['projectRealPath']+moment().format('YYYYMDDHHmm');
        var fileDest   = folderDest+'/'+params['projectName']+'.tar.gz';

        //Cria a pasta
        return this.targz().then(function(projectFolder) {
                ssh.exec(['mkdir -p '+folderDest], {filePath: 'deploy.log'}).pipe(gulp.dest('logs')).on('end', function(err){

                //Deu ruim?
                if(err){
                    console.error(err);
                    return false;
                }

                //Envia o arquivo
                console.log('Enviando o projeto...');
                var strCommand = 'rsync -avz '+projectFolder+' --progress -e "ssh -i '+params['key']+'" '+params['username']+'@'+params['host']+':'+fileDest;
                exec(strCommand, function (err, stdout, stderr) {
                    if(err){
                        console.error(err);
                        return false;
                    }

                    //Descompacta, limpa tudo e cria o link
                    var untar    = 'tar -xvzf '+fileDest+' -C '+folderDest;
                    var clean    = 'rm '+fileDest;
                    var cleanLn  = 'rm '+params['projectPath']+params['projectName'];
                    var link     = 'ln -s '+folderDest+' '+params['projectPath']+params['projectName'];
                    
                    //Executa os comandos
                    ssh.shell([untar, clean, cleanLn, link], {filePath: 'deploy.log'})
                    .pipe(gulp.dest('logs')).on('end', function(err){
                        if(err){
                            console.error(err);
                            return false;
                        }

                        //Verifica quantos ficaram
                        ssh.exec(['ls -tr '+params['projectRealPath']], {filePath: 'deploy_list.log'}).pipe(gulp.dest('logs')).on('data', function(file){
                            
                            //Busca os itens
                            var itens = file.contents.toString().split('\n');

                            //Mais que 5?
                            if(itens.length > 5){
                            
                                //Pega o primeiro item
                                var older = itens[0];

                                //E apaga
                                ssh.exec(['rm -rf '+params['projectRealPath']+older], {filePath: 'deploy_rm_older.log'}).pipe(gulp.dest('logs'));
                            }
                        });

                    });
                    
                });
            });
        });
    }

    /**
     * rollback
     *
     * Deu ruim? Volta rápido tudo
     *
     * @access public
     * @author Murilo Spinoza
     * @return object
     */
    GulpDeploy.prototype.rollback = function() {

        //Lista os itens
        ssh.exec(['ls -t '+params['projectRealPath']], {filePath: 'deploy.log'}).pipe(gulp.dest('logs')).on('data', function(file){
            
            //Busca os itens
            var itens = file.contents.toString().split('\n');

            //Existe item para fazer rollback?
            if(typeof(itens[1]) == "undefined"){
                console.error('Não existe backup para rollback');
                return false;
            }
            
            //Ações
            var cleanLn  = 'rm '+params['projectPath']+params['projectName'];
            var link     = 'ln -s '+params['projectRealPath']+itens[1]+' '+params['projectPath']+params['projectName'];
            var cleanErr = 'rm -rf '+params['projectRealPath']+itens[0];
            
            //Executa
            ssh.shell([cleanLn, link, cleanErr], {filePath: 'deploy.log'}).pipe(gulp.dest('logs'));
        });
    }

    return GulpDeploy;
})();

module.exports = GulpDeploy;