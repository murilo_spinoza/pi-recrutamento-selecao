var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var ts = require('gulp-typescript');
var tslint = require('gulp-tslint');
var gulpDeploy = require('./gulp-deploy');

const prettier = require('gulp-plugin-prettier');

//Monta a compilação
var tsProject = ts.createProject('./tsconfig.json');
gulp.task('build', function() {
	//Copia os arquivos
	gulp.start('copy');

	var tsResult = gulp.src(['typings/**/*.ts', 'src/**/*.ts']).pipe(tsProject());
	return tsResult.js.pipe(gulp.dest('dist'));
});

//Copia os HTMLs
gulp.task('copy', function() {
	return gulp.src(['src/**/*.html', 'src/**/*.json']).pipe(gulp.dest('dist'));
});

//Cria o watch que restarta o nodemon a cada novo build
gulp.task('watch', function() {
	//Escuta os arquivos
	gulp.watch(['src/**/*.ts', 'src/**/*.html', 'src/**/*.json'], ['copy', 'build']);
	// gulp.watch(['src/**/*.ts', 'src/**/*.html', 'src/**/*.json'], ['test', 'copy', 'build']);

	//Inicia o servidor
	nodemon({
		script: 'dist/app.js',
		watch: ['dist/*.js']
	});
});

//Build como padrão
// gulp.task('default', ['test', 'copy', 'build']);
gulp.task('default', ['copy', 'build']);

//Só deixa passar se o código não estiver porco
gulp.task('test', function() {
	var options = {
		src: 'src/**/*.ts',
		tslintOptions: {
			configuration: './tslint.json'
		},
		reporter: 'prose',
		reportOptions: {
			emitError: true
		}
	};

	return gulp
		.src(options.src)
		.pipe(tslint(options.tslintOptions))
		.pipe(tslint.report(options.reporter, options.reportOptions));
});

// replace unformatted with formatted
gulp.task('format', () =>
	gulp
		.src(['./src/**/*.ts'])
		.pipe(prettier.format({ singleQuote: true, printWidth: 140, tabWidth: 3, useTabs: true }))
		.pipe(gulp.dest(file => file.base))
);

// throw error if there is unformatted file
gulp.task('format-check', () =>
	gulp
		.src(['./src/**/*.ts'])
		.pipe(prettier.format({ singleQuote: true, printWidth: 140, tabWidth: 3, useTabs: true }, { reporter: 'warning' }))
);

//Seta o deploy
var config = {
	host: '',
	port: 22,
	username: '',
	projectPath: '',
	projectName: '',
	key: ''
};

//Faz setup do deploy no servidor
gulp.task('deploy:setup', function() {
	//Instancia e executa o setup
	var deploy = new gulpDeploy(gulp, config);
	return deploy.setup();
});

//Faz o deploy para produção
gulp.task('deploy:prod', ['default'], function() {
	//Seta o env e instancia
	config['env'] = 'production';
	var deploy = new gulpDeploy(gulp, config);

	//Executa
	return deploy.deploy();
});

//Faz o rollback
gulp.task('deploy:rollback', function() {
	//Seta o env e instancia
	config['env'] = 'production';
	var deploy = new gulpDeploy(gulp, config);

	//Executa
	return deploy.rollback();
});
