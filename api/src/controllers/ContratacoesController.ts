/**
 * ContratacoesController
 *
 * Gerenciador de contratacao
 *
 * @author Murilo Spinoza de Arruda
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { Contratacoes } from '../models/Contratacoes';
import { ContratacoesRequisitos, IContratacaoRequisito } from '../models/ContratacoesRequisitos';
import { Candidatos, ICandidatoTriagem } from '../models/Candidatos';
import { ContratacoesCandidatosPontos } from '../models/ContratacoesCandidatosPontos';
import moment = require('moment');
export class ContratacoesController extends BaseController {
	/**
	 * _modelName
	 */
	protected _modelName = 'Contratacoes';
	protected _isProcessoSeletivo = false;

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'ContratacaoId', type: FieldTypeEnum.NUMBER },
			{ name: 'Salario', type: FieldTypeEnum.NUMBER },
			{ name: 'Contratacoes', type: FieldTypeEnum.NUMBER },
			{
				name: 'Status',
				type: FieldTypeEnum.ENUM,
				formatter: (this.model(this._modelName) as Contratacoes).STATUS_OPCOES
			},
			{
				name: 'Etapa',
				type: FieldTypeEnum.ENUM,
				formatter: (this.model(this._modelName) as Contratacoes).ETAPA_OPCOES
			},
			{
				name: 'Cargo',
				type: FieldTypeEnum.STRING,
				orderDefault: OrderEnum.ASC,
				join: 'Cargo'
			}
		];

		// Monta os includes
		const includes = [
			{
				model: this.model('Cargos').ORM,
				as: 'Cargo',
				attributes: ['Cargo'],
				required: true
			}
		];

		// Se for processo seletivo o status é só em andamento ou finalizado
		const whereDefault = { Status: this._isProcessoSeletivo ? ['E', 'F'] : ['P', 'R'] };

		try {
			this.showSuccess(await this._get(req, fields, includes, whereDefault), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * set_form
	 *
	 * Executa a busca de um item específico e devolve a lista para os campos estrangeiros, por exemplo
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async set_form(req: express.Request, res: express.Response) {
		const visualizacao = req.query['finalidade'] === 'visualizar';

		// Monta as listas associativas
		const itensOpcoes = {
			HabilidadesOpcoes: {
				model: 'Habilidades',
				params: {
					field: 'Habilidade',
					includes: [
						{
							model: this.model('ContratacoesRequisitos').ORM,
							as: 'ContratacaoRequisito',
							required: visualizacao,
							where: req.query['id'] ? { ContratacaoId: req.query['id'] } : {},
							attributes: ['Nivel', 'Tipo']
						}
					]
				}
			},
			TestesOpcoes: {
				model: 'Testes',
				params: {
					field: 'Teste',
					includes: [
						{
							model: this.model('ContratacoesTestes').ORM,
							as: 'ContratacaoTeste',
							required: visualizacao,
							where: req.query['id'] ? { ContratacaoId: req.query['id'] } : {},
							attributes: ['MinimoPontos']
						}
					]
				}
			},
			CursosOpcoes: {
				model: 'Cursos',
				params: {
					field: 'Curso',
					includes: [
						{
							model: this.model('GrausEscolaridades').ORM,
							as: 'GrauEscolaridade',
							required: true,
							attributes: ['GrauEscolaridade']
						}
					]
				}
			}
		};

		if (!visualizacao) {
			Object.assign(itensOpcoes, {
				TipoRequisitoOpcoes: {
					model: 'ContratacoesRequisitos',
					custom: 'associativeListAttr',
					params: 'TIPO_OPCOES'
				},
				QuadroVagaOpcoes: {
					model: 'QuadroVagas',
					params: {
						field: 'CargoId',
						includes: [
							{
								model: this.model('Cargos').ORM,
								as: 'Cargo',
								required: true,
								attributes: ['Cargo']
							}
						]
					}
				},
				PcdOpcoes: {
					model: this._modelName,
					custom: 'associativeListAttr',
					params: 'PCD_OPCOES'
				},
				StatusOpcoes: {
					model: this._modelName,
					custom: 'associativeListAttr',
					params: 'STATUS_OPCOES'
				}
			});
		}

		// Busca os dados
		const retorno = await this._setForm(req, res, 'Contratacao', itensOpcoes);

		if (req.query['id']) {
			const cursos: any = await this.model('ContratacoesEscolaridades').ORM.findAll({
				where: <any>{ ContratacaoId: req.query['id'] },
				raw: true
			});
			retorno['Contratacao']['Escolaridades'] = [];
			for (const curso of cursos) {
				retorno['Contratacao']['Escolaridades'].push(curso.CursoId);
			}
		}

		if (visualizacao) {
			let opcoes: any = (this.model(this._modelName) as Contratacoes).PCD_OPCOES;
			retorno['Contratacao']['Pcd'] = opcoes[retorno['Contratacao']['Pcd']] || null;

			opcoes = (this.model(this._modelName) as Contratacoes).STATUS_OPCOES;
			retorno['Contratacao']['Status'] = opcoes[retorno['Contratacao']['Status']] || null;

			const cargo: any = await this.model('Cargos').ORM.findById(retorno['Contratacao']['CargoId']);
			retorno['Contratacao']['Cargo'] = cargo['Cargo'];
			delete retorno['Contratacao']['CargoId'];

			let usuario: any = await this.model('Usuarios').ORM.findById(retorno['Contratacao']['UsuarioSolicitanteId']);
			retorno['Contratacao']['UsuarioSolicitante'] = usuario['dataValues']['Nome'];

			if (retorno['Contratacao']['UsuarioAvaliadorId']) {
				usuario = await this.model('Usuarios').ORM.findById(retorno['Contratacao']['UsuarioAvaliadorId']);
				retorno['Contratacao']['UsuarioAvaliador'] = usuario['dataValues']['Nome'];
				delete retorno['Contratacao']['UsuarioAvaliadorId'];
			}

			retorno['Escolaridades'] = await this.model('ContratacoesEscolaridades').ORM.findAll({
				where: <any>{ ContratacaoId: req.query['id'] },
				raw: true
			});

			if (retorno['Escolaridades']) {
				const cursosOpcoes: { [key: number]: { CursoId: number; Curso: string; GrauEscolaridade: string } } = {};

				for (const curso of retorno['CursosOpcoes']) {
					cursosOpcoes[curso.key] = {
						CursoId: curso.key,
						Curso: curso.value,
						GrauEscolaridade: curso.GrauEscolaridade.GrauEscolaridade
					};
				}

				for (const escolaridade of retorno['Escolaridades']) {
					escolaridade['Curso'] = cursosOpcoes[escolaridade['CursoId']].Curso;
					escolaridade['GrauEscolaridade'] = cursosOpcoes[escolaridade['CursoId']].GrauEscolaridade;

					delete escolaridade['ContratacaoId'];
					delete escolaridade['CandidatoEscolaridadeId'];
					delete escolaridade['CursoId'];
				}
			}
			delete retorno['CursosOpcoes'];
		}

		retorno['Habilidades'] = [];

		for (const i in retorno['HabilidadesOpcoes']) {
			const habilidade = retorno['HabilidadesOpcoes'][i];

			if (habilidade.ContratacaoRequisito && habilidade.ContratacaoRequisito.dataValues) {
				habilidade.Nivel = habilidade.ContratacaoRequisito.dataValues.Nivel;
				habilidade.TipoRequisito = habilidade.ContratacaoRequisito.dataValues.Tipo;

				if (visualizacao) {
					habilidade.Tipo = habilidade.TipoRequisito;
					habilidade.TipoRequisito = (this.model('ContratacoesRequisitos') as ContratacoesRequisitos).TIPO_OPCOES[
						habilidade.TipoRequisito
					];
				}
				retorno['Habilidades'].push(i);
			}

			delete habilidade.ContratacaoRequisito;
		}

		retorno['Testes'] = [];
		retorno['MediaTestes'] = 0;

		for (const i in retorno['TestesOpcoes']) {
			const teste = retorno['TestesOpcoes'][i];

			if (teste.ContratacaoTeste && teste.ContratacaoTeste.dataValues) {
				teste.MinimoPontos = teste.ContratacaoTeste.dataValues.MinimoPontos;
				retorno['MediaTestes'] += teste.MinimoPontos;
				retorno['Testes'].push(i);
			}

			delete teste.ContratacaoTeste;
		}

		if (retorno['TestesOpcoes']) {
			retorno['MediaTestes'] = retorno['MediaTestes'] / retorno['TestesOpcoes'].length;
		}

		if (visualizacao) {
			delete retorno['Habilidades'];
			delete retorno['Testes'];
		}

		if (visualizacao && (retorno['Contratacao']['Status'] === 'Análise Pendente' || retorno['Contratacao']['Status'] === 'Rejeitada')) {
			const listaRequisitos: IContratacaoRequisito[] = [];
			for (const requisito of retorno['HabilidadesOpcoes']) {
				listaRequisitos.push({
					HabilidadeId: requisito.key,
					Tipo: requisito.Tipo,
					Nivel: requisito.Nivel
				});
			}

			retorno['Triagem'] = await this.__encontrarCandidatos(
				req.query['id'],
				listaRequisitos,
				retorno['Contratacao']['Pcd'] === 'Sim',
				retorno['Contratacao']['Contratacoes'],
				retorno['Contratacao']['Escolaridades']
			);
		}

		// Retorna
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const contratacao: any = {
			id: req.body['id'],
			QuadroVagaId: req.body['QuadroVagaId'],
			CargoId: req.body['CargoId'],
			UsuarioAvaliadorId: null,
			Avaliacao: null,
			Descricao: req.body['Descricao'],
			Motivo: req.body['Motivo'],
			Rejeicao: null,
			Salario: req.body['Salario'],
			Contratacoes: req.body['Contratacoes'],
			PontosTriagem: req.body['PontosTriagem'],
			Pcd: req.body['Pcd'],
			// Análise pendente
			Status: 'P'
		};

		if (!contratacao.id) {
			contratacao.UsuarioSolicitanteId = req.headers['usuario_id'];
			contratacao.Solicitacao = new Date();
		}

		// Executa o save
		try {
			if (contratacao.id) {
				const dados: any = await this.model(this._modelName).ORM.findById(contratacao.id);
				if (!dados) {
					throw new Error('Contratação inválida');
				}
				if (dados.Status !== 'P' && dados.Status !== 'R') {
					throw new Error('Esta contratação não pode ser alterada');
				}
			}

			const data = await this._save(contratacao, req);

			const contratacaoId = data.id;

			await this.__atualizaEscolaridades(req, contratacaoId);
			await this.__atualizaHabilidades(req, contratacaoId);
			await this.__atualizaTestes(req, contratacaoId);

			this.showSuccess(data, res);
		} catch (error) {
			this.showError(error, res);
		}
	}

	private async __atualizaHabilidades(req: express.Request, contratacaoId: number) {
		try {
			const habilidades = req.body['Habilidades'];

			if (habilidades === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('ContratacoesRequisitos').ORM.destroy({
					where: <any>{ ContratacaoId: contratacaoId }
				});
			}

			const contratacaoHabilidades = [];

			for (const habilidadeId in habilidades) {
				if (habilidades[habilidadeId]) {
					contratacaoHabilidades.push({
						ContratacaoId: contratacaoId,
						HabilidadeId: habilidadeId,
						Nivel: habilidades[habilidadeId].Nivel,
						Tipo: habilidades[habilidadeId].TipoRequisito
					});
				}
			}

			await this.model('ContratacoesRequisitos').ORM.bulkCreate(contratacaoHabilidades);
		} catch (err) {
			console.error('__atualizaHabilidades error', err);
		}
	}

	private async __atualizaTestes(req: express.Request, contratacaoId: number) {
		try {
			const testes = req.body['Testes'];

			if (testes === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('ContratacoesTestes').ORM.destroy({
					where: <any>{ ContratacaoId: contratacaoId }
				});
			}

			const contratacaoTestes = [];

			for (const testeId in testes) {
				if (testes[testeId]) {
					contratacaoTestes.push({
						ContratacaoId: contratacaoId,
						TesteId: testeId,
						MinimoPontos: testes[testeId].MinimoPontos
					});
				}
			}

			await this.model('ContratacoesTestes').ORM.bulkCreate(contratacaoTestes);
		} catch (err) {
			console.error('__atualizaTestes error', err);
		}
	}

	private async __atualizaEscolaridades(req: express.Request, contratacaoId: number) {
		try {
			const escolaridades = req.body['Escolaridades'];

			if (escolaridades === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('ContratacoesEscolaridades').ORM.destroy({
					where: <any>{ ContratacaoId: contratacaoId }
				});
			}

			const contratacaoEscolaridades = [];

			for (const cursoId of escolaridades) {
				contratacaoEscolaridades.push({
					ContratacaoId: contratacaoId,
					CursoId: cursoId
				});
			}

			await this.model('ContratacoesEscolaridades').ORM.bulkCreate(contratacaoEscolaridades);
		} catch (err) {
			console.error('__atualizaEscolaridades error', err);
		}
	}

	/**
	 * Busca candidatos que atendam algum requisito e calcula os pontos
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param requisitos lista de requisitos de uma contratação
	 */
	private async __encontrarCandidatos(
		contratacaoId: number,
		listaRequisitos: IContratacaoRequisito[],
		pcd: boolean,
		qtdeContratacoes: number,
		escolaridades: number[]
	) {
		// Objeto para conter a associação da habilidade com os requisitos
		const requisitos: {
			[habilidadeId: number]: { Nivel: number; Tipo: 'O' | 'I' | 'D' | 'P' };
		} = {};

		// Percorre os requisitos
		for (const requisito of listaRequisitos) {
			// Recebe na habilidade o nível exigido e tipo de requisito
			requisitos[requisito.HabilidadeId] = {
				Nivel: requisito.Nivel,
				Tipo: requisito.Tipo
			};
		}

		// Busca a lista de candidatos que atenda à algum requisito
		const candidatos = await (this.model('Candidatos') as Candidatos).candidatosRequisitos(Object.keys(requisitos), pcd, escolaridades);

		let candidatosTriagem: ICandidatoTriagem[] = [];

		// Percorre os candidatos para montar a pontuação
		for (let item of candidatos) {
			item = item['dataValues'];
			const candidato: ICandidatoTriagem = {};
			candidato.CandidatoId = item.CandidatoId;
			candidato.Nome = item.Nome;
			candidato.Email = item.Email;
			candidato.Pcd = (this.model('Candidatos') as Candidatos).PCD_OPCOES[item.Pcd] || 'Não';
			candidato.Idade = new Date().getFullYear() - new Date(item.Nascimento).getFullYear();

			// Inicializa a pontuação do candidato
			candidato.Pontos = 0;

			// Inicializa a lista de pontos recebidos
			candidato.CandidatoPontos = [];

			// Inicializa a lista de habilidades
			candidato.Habilidades = [];

			// Percorre as habilidades do candidato
			for (let candHabilidade of item.CandidatosHabilidades) {
				// Candidato habilidade
				candHabilidade = candHabilidade['dataValues'];

				// Habilidade do candidato
				const habilidade = {
					HabilidadeId: candHabilidade.HabilidadeId,
					Habilidade: candHabilidade.Habilidade['dataValues'].Habilidade,
					Nivel: candHabilidade.Nivel
				};

				candidato.Habilidades.push(habilidade);

				// Identifica o requisito vinculado à habilidade
				const requisito = requisitos[habilidade.HabilidadeId];

				// Calcula os pontos obtidos
				const pontos = (this.model('ContratacoesCandidatosPontos') as ContratacoesCandidatosPontos).calculaPontos(
					requisito.Tipo,
					requisito.Nivel,
					habilidade.Nivel
				);

				// Recebe o label do tipo de requisito
				const tipoRequisito = (this.model('ContratacoesRequisitos') as ContratacoesRequisitos).TIPO_OPCOES[requisito.Tipo];

				// Adiciona os pontos recebidos por atender o requisito
				candidato.CandidatoPontos.push({
					ContratacaoId: contratacaoId,
					CandidatoId: candidato.CandidatoId,
					UsuarioId: null,
					Pontos: pontos,
					Motivo:
						'Requisito ' +
						tipoRequisito.toLowerCase() +
						' "' +
						habilidade.Habilidade +
						'" atendido com o nível ' +
						habilidade.Nivel +
						' de ' +
						requisito.Nivel +
						' solicitado' +
						(requisito.Nivel > 1 ? 's' : ''),
					Origem: 'S',
					Created: new Date().toISOString()
				});

				candidato.Pontos += pontos;
			}

			// Percorre as escolaridades do candidato
			for (let candEscolaridade of item.CandidatosEscolaridades) {
				// Candidato escolaridade
				candEscolaridade = candEscolaridade['dataValues'];
				const termino = new Date(candEscolaridade['Termino']);
				const dataAtual = new Date();
				const curso = candEscolaridade['Curso']['dataValues']['Curso'];

				let motivo: string;

				// Calcula os pontos obtidos
				let pontos: number;

				if (termino.getTime() < dataAtual.getTime()) {
					pontos = 6;
					motivo = 'Curso "' + curso + '" concluído em ';
				} else {
					pontos = 3;
					motivo = 'Previsão de conclusão do curso "' + curso + '" em ';
				}

				// Adiciona os pontos recebidos por atender o requisito
				candidato.CandidatoPontos.push({
					ContratacaoId: contratacaoId,
					CandidatoId: candidato.CandidatoId,
					UsuarioId: null,
					Pontos: pontos,
					Motivo: motivo + moment(termino).format('D/MM/YYYY'),
					Origem: 'S',
					Created: new Date().toISOString()
				});

				candidato.Pontos += pontos;
			}

			candidato.CandidatoPontos = candidato.CandidatoPontos.sort((a, b) => {
				return a.Pontos > b.Pontos ? -1 : a.Pontos === b.Pontos ? 0 : 1;
			});

			candidatosTriagem.push(candidato);
		}

		candidatosTriagem = candidatosTriagem.sort((a, b) => {
			// Decrescente:
			return a.Pontos > b.Pontos ? -1 : a.Pontos === b.Pontos ? 0 : 1;
			// Crescente:
			// return a.Pontos > b.Pontos ? 1 : a.Pontos == b.Pontos ? 0 : -1;
		});

		const quantiaPotencial = (this.model('ContratacoesCandidatosPontos') as ContratacoesCandidatosPontos).calculaQuantiaPotencial(
			qtdeContratacoes
		);

		const candidatosPotenciais: ICandidatoTriagem[] = candidatosTriagem.slice(0, quantiaPotencial);

		return candidatosPotenciais;
	}

	/**
	 * analisar
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async analisar(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];

			if (!contratacaoId) {
				throw 'Parâmetros obrigatórios não enviados';
			}

			const status: 'A' | 'R' = req.body['status'];
			const rejeicao: string = req.body['rejeicao'];

			const contratacao: any = await this.model(this._modelName).ORM.findById(contratacaoId);

			if (!contratacao) {
				throw 'Contratação informada inválida';
			} else if (contratacao.Status !== 'P' && contratacao.Status !== 'R') {
				throw 'Esta contratação não pode ser análisada';
			} else if (+contratacao.UsuarioSolicitanteId === +req.headers['usuario_id']) {
				throw 'O solicitante não pode analisar a contratação';
			}

			if (status === 'R') {
				if (!rejeicao) {
					throw 'A rejeição de rejeição é obrigatória';
				}
				// Atualiza a contratação passando o novo status e usuarioAvaliador
				await this.model(this._modelName).ORM.update(
					<any>{
						UsuarioAvaliadorId: req.headers['usuario_id'],
						Avaliacao: new Date(),
						Rejeicao: rejeicao,
						Status: 'R'
					},
					{ where: <any>{ ContratacaoId: contratacaoId } }
				);
			} else {
				// Atualiza a contratação passando o novo status e usuarioAvaliador
				await this.model(this._modelName).ORM.update(
					<any>{
						UsuarioAvaliadorId: req.headers['usuario_id'],
						Avaliacao: new Date(),
						Rejeicao: null,
						Etapa: 'triagem',
						Status: 'E'
					},
					{ where: <any>{ ContratacaoId: contratacaoId } }
				);

				const habilidades: any[] = await this.model('ContratacoesRequisitos').ORM.findAll({
					attributes: ['HabilidadeId', 'Nivel', 'Tipo'],
					where: <any>{ ContratacaoId: contratacaoId }
				});

				const listaRequisitos: IContratacaoRequisito[] = [];
				for (const requisito of habilidades) {
					listaRequisitos.push({
						HabilidadeId: requisito['dataValues'].HabilidadeId,
						Tipo: requisito['dataValues'].Tipo,
						Nivel: requisito['dataValues'].Nivel
					});
				}

				const cursos: any = await this.model('ContratacoesEscolaridades').ORM.findAll({
					where: <any>{ ContratacaoId: contratacaoId },
					raw: true
				});

				const escolaridades: number[] = [];
				for (const curso of cursos) {
					escolaridades.push(curso.CursoId);
				}

				// Busca os candidatos em potencial
				const candidatosPotenciais = await this.__encontrarCandidatos(
					contratacaoId,
					listaRequisitos,
					contratacao.Pcd === 'S',
					contratacao.Contratacoes,
					escolaridades
				);

				const contratacoesCandidatos = [];
				let contratacoesCandidatosPontos = [];
				const contratacoesCandidatosTriagem = [];

				for (const candidato of candidatosPotenciais) {
					contratacoesCandidatos.push({
						ContratacaoId: contratacaoId,
						CandidatoId: candidato.CandidatoId,
						Etapa: 'triagem',
						Finalizado: 'N',
						Status: null,
						Reprova: null,
						Created: new Date(),
						Modified: new Date()
					});

					contratacoesCandidatosPontos = contratacoesCandidatosPontos.concat(candidato.CandidatoPontos);
					contratacoesCandidatosTriagem.push({
						ContratacaoId: contratacaoId,
						CandidatoId: candidato.CandidatoId
					});
				}

				// Cria o bulkCreate para a lista de contratacoes_candidatos
				await this.model('ContratacoesCandidatos').ORM.bulkCreate(contratacoesCandidatos);
				// Cria o bulkCreate para a lista de pontos
				await this.model('ContratacoesCandidatosPontos').ORM.bulkCreate(contratacoesCandidatosPontos);
				// Cria o bulkCreate para a lista de triagem
				await this.model('ContratacoesCandidatosTriagem').ORM.bulkCreate(contratacoesCandidatosTriagem);
			}

			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}
}
