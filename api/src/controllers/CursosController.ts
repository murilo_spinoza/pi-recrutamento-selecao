/**
 * CursosController
 *
 * Gerenciador de habilidades
 *
 * @author Murilo Spinoza de Arruda
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { FieldInterface } from '../interfaces/BaseControllerInterface';

export class CursosController extends BaseController {
	protected _modelName = 'Cursos';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'CursoId', type: FieldTypeEnum.NUMBER },
			{ name: 'Curso', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC },
			{ name: 'GrauEscolaridade', type: FieldTypeEnum.STRING, join: 'GrauEscolaridade' }
		];

		// Monta os includes
		const includes = [
			{
				model: this.model('GrausEscolaridades').ORM,
				as: 'GrauEscolaridade',
				required: true
			}
		];

		try {
			this.showSuccess(await this._get(req, fields, includes), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * set_form
	 *
	 * Busca e retorna uma pessoa
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async set_form(req: express.Request, res: express.Response) {
		const curso = await this.model('Cursos').ORM.findAll({
			attributes: ['CursoId', 'Curso'],
			// where: <any>{ CursoId: req.query['id'] },
			include: [
				{
					model: this.model('GrausEscolaridades').ORM,
					attributes: ['GrauEscolaridade'],
					as: 'GrauEscolaridade',
					required: true
				}
			],
			order: [['CursoId', 'DESC']]
		});

		// Monta as listas associativas
		const itensOpcoes = {
			GrausEscolaridades: {
				model: 'GrausEscolaridades',
				params: {
					field: 'GrauEscolaridade'
				}
			}
		};

		// Busca os dados
		const retorno = await this._setForm(req, res, 'Curso', itensOpcoes);

		// Sucesso
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const dados = {
			id: req.body['id'],
			GrauEscolaridadeId: req.body['GrauEscolaridadeId'],
			Curso: req.body['Curso']
		};

		// Executa o save
		try {
			const data = await this._save(dados, req);

			// retorna o sucesso
			return this.showSuccess(data, res);
		} catch (err) {
			console.error(err);
			return this.showError(err, res);
		}
	}
}
