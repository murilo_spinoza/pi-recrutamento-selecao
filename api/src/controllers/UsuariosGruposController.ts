/**
 * UsersGroupsController
 *
 * Gerenciador de grupos de usuários
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { Op } from 'sequelize';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { Permissoes } from '../models/Permissoes';
export class UsuariosGruposController extends BaseController {
	/**
	 * _modelName
	 *
	 * @type {string}
	 * @protected
	 */
	protected _modelName = 'UsuariosGrupos';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'UsuarioGrupoId', type: FieldTypeEnum.NUMBER },
			{ name: 'Grupo', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC }
		];

		try {
			this.showSuccess(await this._get(req, fields), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * set_form
	 *
	 * Executa a busca de um item específico e devolve a lista para os campos estrangeiros, por exemplo
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async set_form(req: express.Request, res: express.Response) {
		// Busca os dados
		const retorno = await this._setForm(req, res, 'UsuarioGrupo');

		// Busca as classificações
		const rotinasClassificacoes: any = await this.model('RotinasClassificacoes').ORM.findAll({
			attributes: ['RotinaClassificacaoId', 'Classificacao'],
			order: [['Ordem', 'ASC'], ['Rotinas', 'Ordem', 'ASC']],
			include: [
				{
					model: this.model('Rotinas').ORM,
					as: 'Rotinas',
					required: true,
					attributes: ['RotinaId', 'Rotina'],
					where: { ApareceMenu: 'S', Publico: 'N' },
					include: [
						{
							model: this.model('Permissoes').ORM,
							as: 'Permissao',
							required: false,
							attributes: ['Consulta', 'Insere', 'Edita', 'Exclui'],
							where: { UsuarioGrupoId: req.query['id'] }
						}
					]
				}
			]
		});
		const permissoes = {};
		const permissoesDefault = {};

		// Analisa as classificações
		for (let rotinaClassificacao of rotinasClassificacoes) {
			// Converte os flags para booleanos
			rotinaClassificacao = rotinaClassificacao['dataValues'];
			for (let rotina of rotinaClassificacao.Rotinas) {
				rotina = rotina['dataValues'];
				const permissao = rotina.Permissao[0] && rotina.Permissao[0]['dataValues'] ? rotina.Permissao[0]['dataValues'] : {};
				permissoes[rotina.RotinaId] = {
					RotinaId: rotina.RotinaId,
					Consulta: permissao.Consulta === 'S',
					Edita: permissao.Edita === 'S',
					Exclui: permissao.Exclui === 'S',
					Insere: permissao.Insere === 'S'
				};
				permissoesDefault[rotina.RotinaId] = {
					RotinaId: rotina.RotinaId,
					Consulta: false,
					Edita: false,
					Exclui: false,
					Insere: false
				};
				delete rotina.Permissao;
			}
		}

		// Associa as permissões
		retorno['UsuarioGrupo']['Permissoes'] = permissoes;
		retorno['PermissoesDefault'] = permissoesDefault;
		retorno['RotinasClassificacoes'] = rotinasClassificacoes;

		// Sucesso
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const dados = {
			id: req.body['id'],
			Grupo: req.body['Grupo']
		};
		const permissoes = req.body['Permissoes'];

		// Executa o save
		try {
			const data = await this._save(dados, req);

			// executa o save de permissões
			((await this.model('Permissoes')) as Permissoes).salvaPermissoesGrupo(data.id, permissoes);

			// retorna o sucesso
			return this.showSuccess(data, res);
		} catch (err) {
			console.error(err);
			return this.showError(err, res);
		}
	}
}
