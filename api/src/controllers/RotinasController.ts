/**
 * RoutinesController
 *
 * Gerenciador de rotinas
 *
 * @author Murilo Spinoza de Arruda
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { Rotinas } from '../models/Rotinas';
export class RotinasController extends BaseController {
	/**
	 * lista
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async lista(req: express.Request, res: express.Response, filter?: boolean) {
		try {
			// find the menu
			const response = await (this.model('Rotinas') as Rotinas).lista_rotinas(Number(req.headers['grupo_id']), filter);
			// its ok
			this.showSuccess(response, res);
		} catch (err) {
			this.showError(err, res);
		}
	}
}
