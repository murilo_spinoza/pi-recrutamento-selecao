/**
 * UsersController
 *
 * Gerenciador de usuários
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import crypto = require('crypto');
import Email from '../libraries/Email';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { Usuarios } from '../models/Usuarios';
import { Logs } from '../models/Logs';
import { QuadroVagas } from '../models/QuadroVagas';
export class UsuariosController extends BaseController {
	/**
	 * _modelName
	 *
	 * @type {string}
	 * @protected
	 */
	protected _modelName = 'Usuarios';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'UsuarioId', type: FieldTypeEnum.NUMBER },
			{ name: 'Nome', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC },
			{ name: 'Email', type: FieldTypeEnum.STRING },
			{ name: 'Grupo', type: FieldTypeEnum.STRING, join: 'Grupo' },
			{ name: 'Cargo', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC, join: 'Cargo' }
		];

		// Monta os includes
		const includes = [
			{
				model: this.model('UsuariosGrupos').ORM,
				as: 'Grupo',
				required: true
			},
			{
				model: this.model('Cargos').ORM,
				as: 'Cargo',
				attributes: ['Cargo'],
				required: false
			}
		];

		try {
			this.showSuccess(await this._get(req, fields, includes), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * login
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async login(req: express.Request, res: express.Response) {
		// Recebe os dados
		const email = req.body['Email'];
		const senha = req.body['Senha'];
		const source: string = req.headers.source + '';

		// Valida se as informações foram recebidas
		if (!email || !senha) { return this.showError('Parâmetros obrigatórios não informados', res); }

		try {
			// Realiza o login
			const usuario: any = await (this.model(this._modelName) as Usuarios).login(
				email,
				crypto
					.createHash('md5')
					.update(senha)
					.digest('hex'),
				source
			);

			// Busca as informações do grupo do usuário
			const grupo: any = await this.model('UsuariosGrupos').ORM.findById(usuario['UsuarioGrupoId']);

			// Monta o link da requisição
			const fullUrl = req.protocol + ':// ' + req.get('host') + req.originalUrl;

			// Registra o acesso do usuário
			(this.model('Logs') as Logs).registra_log('O usuário "' + email + '" logou com sucesso', 'AC', fullUrl, usuario['UsuarioId']);

			const retorno = {
				Token: usuario.Token,
				UsuarioId: usuario.UsuarioId,
				Nome: usuario.Nome,
				Grupo: grupo.Grupo,
				UsuarioGrupoId: usuario.UsuarioGrupoId,
				Email: email,
				Telefone: usuario.Telefone
			};

			// Devolve as informações da sessão
			this.showSuccess(retorno, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * logout
	 *
	 * logout method
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async logout(req: express.Request, res: express.Response) {
		// get user's id
		const usuario_id = req.headers['usuario_id'];

		try {
			// clean token
			const update = { [req.headers.source == 'app' ? 'TokenApp' : 'Token']: null };

			// find the user
			const user: any = await this.model(this._modelName).ORM.findById(+usuario_id);

			// Logout
			await user.updateAttributes(update);

			// registrate the user logout
			(this.model('Logs') as Logs).registra_log('O usuário "' + user.Email + '" deslogou com sucesso', 'AC');

			// its ok
			return this.showSuccess({ logout: true }, res);
		} catch (e) {
			// its ok
			return this.showError(e, res);
		}
	}

	/**
	 * set_form
	 *
	 * Executa a busca de um item específico e devolve a lista para os campos estrangeiros, por exemplo
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async set_form(req: express.Request, res: express.Response) {
		// Monta as listas associativas
		const itensOpcoes = {
			UsuarioGrupoOpcoes: {
				model: 'UsuariosGrupos',
				params: {
					field: 'Grupo'
				}
			},
			AtivoOpcoes: {
				model: this._modelName,
				custom: 'associativeListAttr',
				params: 'ATIVO_OPCOES'
			},
			CargoOpcoes: {
				model: 'Cargos',
				params: { field: 'Cargo' }
			}
		};

		// Busca os dados
		const retorno = await this._setForm(req, res, 'Usuario', itensOpcoes);
		delete retorno['Usuario']['Senha'];

		// Retorna
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param req the express request object
	 * @param res the express response object
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const usuario: any = {
			id: req.body['id'],
			Nome: req.body['Nome'],
			Email: req.body['Email'],
			Ativo: req.body['Ativo'],
			UsuarioGrupoId: req.body['UsuarioGrupoId'],
			CargoId: req.body['CargoId'] || null,
			Telefone: req.body['Telefone']
		};

		console.log(usuario);

		// Verifica se precisa tratar a senha
		if (req.body['Senha']) {
			// Tenta buscar o usuário atual (se necessário)
			try {
				let usuarioAtual: any;
				if (usuario['id']) {
					usuarioAtual = await this.model(this._modelName).ORM.findById(usuario['id']);

					// retorna erro se usuário não existir
					if (!usuarioAtual) { return this.showError({ Geral: 'Parâmetros obrigatórios não enviados' }, res); }
				}

				// verifica se existe senha atual
				if (req.body['SenhaAtual']) {
					// recupera a senha antiga e confirma validação com a senha recebida
					const senhaAtual = crypto
						.createHash('md5')
						.update(req.body['SenhaAtual'])
						.digest('hex');
					if (senhaAtual == usuarioAtual.Senha) {
						usuario['Senha'] = crypto
							.createHash('md5')
							.update(req.body['Senha'])
							.digest('hex');
					} else {
						// retorna erro se senha diferente
						return this.showError({ Geral: 'Senha inválida' }, res);
					}
				} else {
					// confirma se senha é a mesma de confirma senha
					if (req.body['Senha'] == req.body['ConfirmaSenha']) {
						usuario['Senha'] = crypto
							.createHash('md5')
							.update(req.body['Senha'])
							.digest('hex');
					} else { return this.showError({ Geral: 'Senha inválida' }, res); }
				}
			} catch (e) {
				return this.showError({ Geral: 'Senha inválida' }, res);
			}
		}

		// Executa o save
		try {
			// Faz envio de email de boas vindas
			if (!usuario.id && usuario.id) {
				const sender = new Email();
				sender.new_user(usuario.Email, usuario.Nome, null);
			}

			const data = await this._save(usuario, req);

			await (this.model('QuadroVagas') as QuadroVagas).atualizaSaldo();

			this.showSuccess(data, res);
		} catch (error) {
			this.showError(error, res);
		}
	}

	/**
	 * forgot_password
	 *
	 * Forgot password method
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public forgot_password(req: express.Request, res: express.Response): void {
		// Recebe o e-mail
		const email = req.body['Email'];

		// Valida
		if (!email) { this.showError('Parâmetros obrigatórios não enviados', res); }

		// Gera a nova senha
		(this.model(this._modelName) as Usuarios)
			.nova_senha(email)
			.then((response: any) => {
				// Faz o envio
				const sender = new Email();
				sender.forgot_password(email, response.Nome, response.NovaSenha);

				// Armazena o log
				(this.model('Logs') as Logs).registra_log('O usuário "' + email + '" solicitou uma nova senha', 'AC');

				// Sucesso
				this.showSuccess(response, res);
			})
			.catch(err => {
				this.showError(err, res);
			});
	}

	/**
	 * contact
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async contact(req: express.Request, res: express.Response) {
		const dados = {
			Assunto: req.body['Assunto'],
			Nome: req.body['Nome'],
			Email: req.body['Email'],
			Mensagem: req.body['Mensagem']
		};

		// Valida
		if (!dados.Assunto || !dados.Nome || !dados.Email || !dados.Mensagem) { this.showError('Parâmetros obrigatórios não enviados', res); }

		const send = new Email();
		send.sendContact(dados);

		return this.showSuccess({}, res);
	}
}
