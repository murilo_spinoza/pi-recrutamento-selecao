/**
 * GrausEscolaridadesController
 *
 * Gerenciador de graus de escolaridades
 *
 * @author Murilo Spinoza de Arruda
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { FieldInterface } from '../interfaces/BaseControllerInterface';

export class GrausEscolaridadesController extends BaseController {
	/**
	 * _modelName
	 */
	protected _modelName = 'GrausEscolaridades';

	/**
	 * Executa a listagem dos dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'GrauEscolaridadeId', type: FieldTypeEnum.NUMBER },
			{ name: 'GrauEscolaridade', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC }
		];

		try {
			this.showSuccess(await this._get(req, fields), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * Busca e retorna uma pessoa
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async set_form(req: express.Request, res: express.Response) {
		// Busca os dados
		const retorno = await this._setForm(req, res, 'GrauEscolaridade');

		// Sucesso
		return this.showSuccess(retorno, res);
	}

	/**
	 * Salva os dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const dados = {
			id: req.body['id'],
			GrauEscolaridade: req.body['GrauEscolaridade']
		};

		// Executa o save
		try {
			const data = await this._save(dados, req);

			// retorna o sucesso
			return this.showSuccess(data, res);
		} catch (err) {
			console.error(err);
			return this.showError(err, res);
		}
	}
}
