/**
 * LogsController
 *
 * Gerenciador de logs
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { Op } from 'sequelize';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';

export class LogsController extends BaseController {
	/**
	 * _modelName
	 *
	 * @type {string}
	 * @protected
	 */
	protected _modelName = 'Logs';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'LogId', type: FieldTypeEnum.NUMBER, orderDefault: OrderEnum.DESC },
			{ name: 'Nome', type: FieldTypeEnum.STRING, join: 'Usuario' },
			{ name: 'Mensagem', type: FieldTypeEnum.STRING },
			{ name: 'Link', type: FieldTypeEnum.STRING },
			{ name: 'Tipo', type: FieldTypeEnum.STRING },
			{ name: 'Alteracoes', type: FieldTypeEnum.STRING },
			{ name: 'Created', type: FieldTypeEnum.STRING }
		];

		// Monta os includes
		const includes = [
			{
				model: this.model('Usuarios').ORM,
				attributes: ['Nome'],
				as: 'Usuario',
				required: true
			}
		];

		try {
			this.showSuccess(await this._get(req, fields, includes), res);
		} catch (err) {
			this.showError(err, res);
		}
	}
}
