/**
 * TestesController
 *
 * Gerenciador de testes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { Op } from 'sequelize';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';

export class TestesController extends BaseController {
	/**
	 * _modelName
	 *
	 * @type {string}
	 * @protected
	 */
	protected _modelName = 'Testes';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'TesteId', type: FieldTypeEnum.NUMBER },
			{ name: 'Teste', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC }
		];

		try {
			this.showSuccess(await this._get(req, fields), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * set_form
	 *
	 * Busca e retorna uma pessoa
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async set_form(req: express.Request, res: express.Response) {
		// Busca os dados
		const retorno = await this._setForm(req, res, 'Teste');

		// Sucesso
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @return {void}
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const dados = {
			id: req.body['id'],
			Teste: req.body['Teste']
		};

		// Executa o save
		try {
			const data = await this._save(dados, req);

			// retorna o sucesso
			return this.showSuccess(data, res);
		} catch (err) {
			console.error(err);
			return this.showError(err, res);
		}
	}
}
