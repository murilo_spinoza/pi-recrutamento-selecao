import { ContratacoesController } from './ContratacoesController';
import express = require('express');
import { Candidatos } from '../models/Candidatos';
import { ContratacoesCandidatosTestes } from '../models/ContratacoesCandidatosTestes';
import { Op } from 'sequelize';

/**
 * ProcessosSeletivosController
 *
 * Gerenciador de processos seletivos
 *
 * @author Murilo Spinoza de Arruda
 */
export class ProcessosSeletivosController extends ContratacoesController {
	protected _isProcessoSeletivo = true;

	/**
	 * contratacaoCandidatosTriagem
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async contratacaoCandidatosTriagem(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.query['contratacaoId'];

			const data: any[] = await this.model('Candidatos').ORM.findAll({
				attributes: ['CandidatoId', 'Nome', 'Email', 'Nascimento', 'Pcd', 'Endereco'],
				include: [
					{
						model: this.model('ContratacoesCandidatos').ORM,
						as: 'Contratacao',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					},
					{
						model: this.model('ContratacoesCandidatosPontos').ORM,
						as: 'ContratacaoPontos',
						required: false,
						where: <any>{ ContratacaoId: contratacaoId }
					},
					{
						model: this.model('ContratacoesCandidatosTriagem').ORM,
						as: 'ContratacaoTriagem',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					}
				]
			});

			let candidatos: any[] = [];

			for (let item of data) {
				item = item['dataValues'];
				const candidato: any = {
					CandidatoId: item.CandidatoId,
					Nome: item.Nome,
					Email: item.Email,
					Endereco: item.Endereco,
					Idade: new Date().getFullYear() - new Date(item.Nascimento).getFullYear(),
					Pcd: (this.model('Candidatos') as Candidatos).PCD_OPCOES[item.Pcd] || 'Não',
					Pontos: 0,
					CandidatoPontos: []
				};

				candidato.Analise = item.ContratacaoTriagem['dataValues'].Analise;
				candidato.Status = item.ContratacaoTriagem['dataValues'].Status;
				candidato.Reprova = item.Contratacao['dataValues'].Reprova;

				for (let candidatoPonto of item.ContratacaoPontos) {
					candidatoPonto = candidatoPonto['dataValues'];

					candidato.CandidatoPontos.push({
						CandidatoId: item.CandidatoId,
						ContratacaoId: contratacaoId,
						Created: candidatoPonto.Created,
						Motivo: candidatoPonto.Motivo,
						Origem: candidatoPonto.Origem,
						UsuarioId: candidatoPonto.UsuarioId,
						Pontos: +candidatoPonto.Pontos
					});

					candidato.Pontos += +candidatoPonto.Pontos;
				}

				candidatos.push(candidato);
			}

			candidatos = candidatos.sort((a, b) => {
				// Decrescente:
				return a.Pontos > b.Pontos ? -1 : a.Pontos === b.Pontos ? 0 : 1;
				// Crescente:
				// return a.Pontos > b.Pontos ? 1 : a.Pontos == b.Pontos ? 0 : -1;
			});

			this.showSuccess(candidatos, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * aprovaTriagem
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async aprovaTriagem(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Etapa: 'testes' }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza a triagem do candidato
			await this.model('ContratacoesCandidatosTriagem').ORM.update(<any>{ Status: 'A', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Insere nos testes
			await this.model('ContratacoesCandidatosTestes').ORM.create(<any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId });

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * reprovaTriagem
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async reprovaTriagem(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];
			const reprova = req.body['reprova'];

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'R', Reprova: reprova }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza a triagem do candidato
			await this.model('ContratacoesCandidatosTriagem').ORM.update(<any>{ Status: 'R', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * concluiTriagem
	 *
	 * Define que a etapa de triagem foi finalizada
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async concluiTriagem(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const reprova = 'Etapa finalizada';

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'R', Reprova: reprova }, {
				where: <any>{ ContratacaoId: contratacaoId, Status: { [Op.is]: null }, Etapa: 'triagem' }
			});

			// Atualiza a triagem do candidato
			await this.model('ContratacoesCandidatosTriagem').ORM.update(<any>{ Status: 'R', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, Status: null }
			});

			const existeTestes: any = await this.model('ContratacoesCandidatosTestes').ORM.findOne({
				where: <any>{ ContratacaoId: contratacaoId },
				raw: true
			});

			const update = <any>{ Etapa: existeTestes ? 'testes' : 'aprovados', Status: existeTestes ? 'E' : 'F' };

			await this.model('Contratacoes').ORM.update(update, { where: <any>{ ContratacaoId: contratacaoId } });

			// Atualiza o objeto do candidato
			this.showSuccess({ Finalizado: !existeTestes }, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * contratacaoCandidatosTestes
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async contratacaoCandidatosTestes(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.query['contratacaoId'];

			const data: any[] = await this.model('Candidatos').ORM.findAll({
				attributes: ['CandidatoId', 'Nome', 'Email', 'Nascimento', 'Pcd', 'Endereco'],
				include: [
					{
						model: this.model('ContratacoesCandidatos').ORM,
						as: 'Contratacao',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					},
					{
						model: this.model('ContratacoesCandidatosTestes').ORM,
						as: 'ContratacaoTestes',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					}
				],
				order: [['ContratacaoTestes', 'Pontuacao', 'DESC']]
			});

			const candidatos = [];

			for (let item of data) {
				item = item['dataValues'];

				const candidato: any = {
					CandidatoId: item.CandidatoId,
					Nome: item.Nome,
					Email: item.Email,
					Endereco: item.Endereco,
					Idade: new Date().getFullYear() - new Date(item.Nascimento).getFullYear(),
					Pcd: (this.model('Candidatos') as Candidatos).PCD_OPCOES[item.Pcd] || 'Não'
				};

				candidato.Pontuacao = item.ContratacaoTestes['dataValues'].Pontuacao;

				candidato.Analise = item.ContratacaoTestes['dataValues'].Analise;
				candidato.Status = item.ContratacaoTestes['dataValues'].Status;
				candidato.Reprova = item.Contratacao['dataValues'].Reprova;

				candidatos.push(candidato);
			}

			// candidatos = candidatos.sort((a, b) => {
			// 	// Decrescente:
			// 	return a.Pontos > b.Pontos ? -1 : a.Pontos === b.Pontos ? 0 : 1;
			// 	// Crescente:
			// 	// return a.Pontos > b.Pontos ? 1 : a.Pontos == b.Pontos ? 0 : -1;
			// });

			this.showSuccess(candidatos, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * aprovaTestes
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async aprovaTestes(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Etapa: 'entrevistas' }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza os testes do candidato
			await this.model('ContratacoesCandidatosTestes').ORM.update(<any>{ Status: 'A', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Insere nas entrevistas
			await this.model('ContratacoesCandidatosEntrevistas').ORM.create(<any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId });

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * notaTestes
	 *
	 * Salva a pontuação do candidato na fase
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async notaTestes(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];
			const nota = req.body['nota'];

			// Atualiza os testes do candidato
			await this.model('ContratacoesCandidatosTestes').ORM.update(<any>{ Pontuacao: nota }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * notaEntrevistas
	 *
	 * Salva a pontuação do candidato na fase
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async notaEntrevistas(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];
			const nota = req.body['nota'];

			// Atualiza os testes do candidato
			await this.model('ContratacoesCandidatosEntrevistas').ORM.update(<any>{ Pontuacao: nota, UsuarioId: req.headers['usuario_id'] }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * reprovaTestes
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async reprovaTestes(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];
			const reprova = req.body['reprova'];

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'R', Reprova: reprova }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza os testes do candidato
			await this.model('ContratacoesCandidatosTestes').ORM.update(<any>{ Status: 'R', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * concluiTestes
	 *
	 * Define que a etapa de testes foi finalizada
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async concluiTestes(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const reprova = 'Etapa finalizada';

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'R', Reprova: reprova }, {
				where: <any>{ ContratacaoId: contratacaoId, Status: { [Op.is]: null }, Etapa: 'testes' }
			});

			// Atualiza os testes do candidato
			await this.model('ContratacoesCandidatosTestes').ORM.update(<any>{ Status: 'R', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, Status: null, Pontuacao: 0 }
			});

			const existemEntrevistas: any = await this.model('ContratacoesCandidatosEntrevistas').ORM.findOne({
				where: <any>{ ContratacaoId: contratacaoId },
				raw: true
			});

			const update = <any>{ Etapa: existemEntrevistas ? 'entrevistas' : 'aprovados', Status: existemEntrevistas ? 'E' : 'F' };

			await this.model('Contratacoes').ORM.update(update, { where: <any>{ ContratacaoId: contratacaoId } });

			// Atualiza o objeto do candidato
			this.showSuccess({ Finalizado: !existemEntrevistas }, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * contratacaoCandidatosEntrevistas
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async contratacaoCandidatosEntrevistas(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.query['contratacaoId'];

			const data: any[] = await this.model('Candidatos').ORM.findAll({
				attributes: ['CandidatoId', 'Nome', 'Email', 'Nascimento', 'Pcd', 'Endereco'],
				include: [
					{
						model: this.model('ContratacoesCandidatos').ORM,
						as: 'Contratacao',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					},
					{
						model: this.model('ContratacoesCandidatosEntrevistas').ORM,
						as: 'ContratacaoEntrevistas',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					}
				],
				order: [['ContratacaoEntrevistas', 'Pontuacao', 'DESC']]
			});

			const candidatos = [];

			for (let item of data) {
				item = item['dataValues'];

				const candidato: any = {
					CandidatoId: item.CandidatoId,
					Nome: item.Nome,
					Email: item.Email,
					Endereco: item.Endereco,
					Idade: new Date().getFullYear() - new Date(item.Nascimento).getFullYear(),
					Pcd: (this.model('Candidatos') as Candidatos).PCD_OPCOES[item.Pcd] || 'Não'
				};

				candidato.Pontuacao = item.ContratacaoEntrevistas['dataValues'].Pontuacao;

				candidato.Analise = item.ContratacaoEntrevistas['dataValues'].Analise;
				candidato.Status = item.ContratacaoEntrevistas['dataValues'].Status;
				candidato.Reprova = item.Contratacao['dataValues'].Reprova;

				candidatos.push(candidato);
			}

			// candidatos = candidatos.sort((a, b) => {
			// 	// Decrescente:
			// 	return a.Pontos > b.Pontos ? -1 : a.Pontos === b.Pontos ? 0 : 1;
			// 	// Crescente:
			// 	// return a.Pontos > b.Pontos ? 1 : a.Pontos == b.Pontos ? 0 : -1;
			// });

			this.showSuccess(candidatos, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * aprovaEntrevistas
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async aprovaEntrevistas(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'A', Etapa: 'aprovados' }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza a entrevista do candidato
			await this.model('ContratacoesCandidatosEntrevistas').ORM.update(<any>{ Status: 'A', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Insere nas aprovados
			await this.model('ContratacoesCandidatosAprovados').ORM.create(<any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId });

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * reprovaEntrevistas
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async reprovaEntrevistas(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const candidatoId = req.body['candidatoId'];
			const reprova = req.body['reprova'];

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'R', Reprova: reprova }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza os testes do candidato
			await this.model('ContratacoesCandidatosEntrevistas').ORM.update(<any>{ Status: 'R', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, CandidatoId: candidatoId }
			});

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * concluiEntrevistas
	 *
	 * Define que a etapa de entrevistas foi finalizada
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async concluiEntrevistas(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];
			const reprova = 'Etapa finalizada';

			// Atualiza o contratacao_candidato
			await this.model('ContratacoesCandidatos').ORM.update(<any>{ Finalizado: 'S', Status: 'R', Reprova: reprova }, {
				where: <any>{ ContratacaoId: contratacaoId, Status: { [Op.is]: null }, Etapa: 'entrevistas' }
			});

			// Atualiza os entrevistas do candidato
			await this.model('ContratacoesCandidatosEntrevistas').ORM.update(<any>{ Status: 'R', Analise: new Date() }, {
				where: <any>{ ContratacaoId: contratacaoId, Status: null }
			});

			const existemAprovados: any = await this.model('ContratacoesCandidatosAprovados').ORM.findOne({
				where: <any>{ ContratacaoId: contratacaoId },
				raw: true
			});

			const update = <any>{ Etapa: 'aprovados', Status: existemAprovados ? 'E' : 'F' };

			await this.model('Contratacoes').ORM.update(update, { where: <any>{ ContratacaoId: contratacaoId } });

			// Atualiza o objeto do candidato
			this.showSuccess({ Finalizado: !existemAprovados }, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * contratacaoCandidatosAprovados
	 *
	 * Salva se a contratação foi aprovada ou não
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async contratacaoCandidatosAprovados(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.query['contratacaoId'];

			const data: any[] = await this.model('Candidatos').ORM.findAll({
				attributes: ['CandidatoId', 'Nome', 'Email', 'Nascimento', 'Pcd', 'Endereco'],
				include: [
					{
						model: this.model('ContratacoesCandidatos').ORM,
						as: 'Contratacao',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					},
					{
						model: this.model('ContratacoesCandidatosAprovados').ORM,
						as: 'ContratacaoAprovados',
						required: true,
						where: <any>{ ContratacaoId: contratacaoId }
					}
				]
			});

			const candidatos = [];

			for (let item of data) {
				item = item['dataValues'];

				const candidato = {
					CandidatoId: item.CandidatoId,
					Nome: item.Nome,
					Email: item.Email,
					Endereco: item.Endereco,
					Idade: new Date().getFullYear() - new Date(item.Nascimento).getFullYear(),
					Pcd: (this.model('Candidatos') as Candidatos).PCD_OPCOES[item.Pcd] || 'Não'
				};

				candidatos.push(candidato);
			}

			// candidatos = candidatos.sort((a, b) => {
			// 	// Decrescente:
			// 	return a.Pontos > b.Pontos ? -1 : a.Pontos === b.Pontos ? 0 : 1;
			// 	// Crescente:
			// 	// return a.Pontos > b.Pontos ? 1 : a.Pontos == b.Pontos ? 0 : -1;
			// });

			this.showSuccess(candidatos, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * concluiAprovados
	 *
	 * Define que a etapa de aprovados foi finalizada
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async concluiAprovados(req: express.Request, res: express.Response) {
		try {
			const contratacaoId = req.body['contratacaoId'];

			await this.model('Contratacoes').ORM.update(<any>{ Status: 'F' }, { where: <any>{ ContratacaoId: contratacaoId } });

			// Atualiza o objeto do candidato
			this.showSuccess({}, res);
		} catch (err) {
			this.showError(err, res);
		}
	}
}
