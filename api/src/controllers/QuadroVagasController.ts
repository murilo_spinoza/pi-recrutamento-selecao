/**
 * QuadroVagasController
 *
 * Gerenciador de quadroVaga
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { Op } from 'sequelize';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { QuadroVagas } from '../models/QuadroVagas';
import { Logs } from '../models/Logs';
export class QuadroVagasController extends BaseController {
	/**
	 * _modelName
	 *
	 * @type {string}
	 * @protected
	 */
	protected _modelName = 'QuadroVagas';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'QuadroVagaId', type: FieldTypeEnum.NUMBER },
			{ name: 'Cargo', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC, join: 'Cargo' },
			{ name: 'Salario', type: FieldTypeEnum.NUMBER },
			{ name: 'Vagas', type: FieldTypeEnum.NUMBER },
			{ name: 'Saldo', type: FieldTypeEnum.NUMBER }
		];

		// Monta os includes
		const includes = [
			{
				model: this.model('Cargos').ORM,
				as: 'Cargo',
				attributes: ['Cargo'],
				required: true
			}
		];

		try {
			this.showSuccess(await this._get(req, fields, includes), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * set_form
	 *
	 * Executa a busca de um item específico e devolve a lista para os campos estrangeiros, por exemplo
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 */
	public async set_form(req: express.Request, res: express.Response) {
		// Monta as listas associativas
		const itensOpcoes = {
			TipoRequisitoOpcoes: {
				model: 'QuadroVagasRequisitos',
				custom: 'associativeListAttr',
				params: 'TIPO_OPCOES'
			},
			HabilidadesOpcoes: {
				model: 'Habilidades',
				params: {
					field: 'Habilidade',
					includes: [
						{
							model: this.model('QuadroVagasRequisitos').ORM,
							as: 'QuadroVagaRequisito',
							required: false,
							where: req.query['id'] ? { QuadroVagaId: req.query['id'] } : {},
							attributes: ['Nivel', 'Tipo']
						}
					]
				}
			},
			CargosOpcoes: {
				model: 'Cargos',
				params: { field: 'Cargo' }
			}
		};

		// Busca os dados
		const retorno = await this._setForm(req, res, 'QuadroVaga', itensOpcoes);

		retorno['Habilidades'] = [];

		for (const i in retorno['HabilidadesOpcoes']) {
			const habilidade = retorno['HabilidadesOpcoes'][i];

			if (habilidade.QuadroVagaRequisito && habilidade.QuadroVagaRequisito.dataValues) {
				habilidade.Nivel = habilidade.QuadroVagaRequisito.dataValues.Nivel;
				habilidade.TipoRequisito = habilidade.QuadroVagaRequisito.dataValues.Tipo;
				retorno['Habilidades'].push(i);
			}

			delete habilidade.QuadroVagaRequisito;
		}

		// Retorna
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const quadroVaga: any = {
			id: req.body['id'],
			CargoId: req.body['CargoId'],
			Descricao: req.body['Descricao'],
			Salario: req.body['Salario'],
			Vagas: req.body['Vagas']
		};

		// Executa o save
		try {
			const data = await this._save(quadroVaga, req);

			const quadroVagaId = data.id;

			await (this.model(this._modelName) as QuadroVagas).atualizaSaldo();

			await this.__atualizaHabilidades(req, quadroVagaId);

			this.showSuccess(data, res);
		} catch (error) {
			this.showError(error, res);
		}
	}

	private async __atualizaHabilidades(req: express.Request, quadroVagaId: number) {
		try {
			const habilidades = req.body['Habilidades'];

			if (habilidades === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('QuadroVagasRequisitos').ORM.destroy({
					where: <any>{ QuadroVagaId: quadroVagaId }
				});
			}

			const quadroVagaHabilidades = [];

			for (const habilidadeId in habilidades) {
				if (habilidades[habilidadeId]) {
					quadroVagaHabilidades.push({
						QuadroVagaId: quadroVagaId,
						HabilidadeId: habilidadeId,
						Nivel: habilidades[habilidadeId].Nivel,
						Tipo: habilidades[habilidadeId].TipoRequisito
					});
				}
			}

			await this.model('QuadroVagasRequisitos').ORM.bulkCreate(quadroVagaHabilidades);
		} catch (err) {
			console.error('__atualizaHabilidades error', err);
		}
	}
}
