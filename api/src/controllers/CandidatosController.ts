/**
 * CandidatosController
 *
 * Gerenciador de candidatos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as express from 'express';
import { BaseController } from '../core/BaseController';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import Email from '../libraries/Email';
import { Candidatos } from '../models/Candidatos';
import { CandidatosExperiencias } from '../models/CandidatosExperiencias';
import { Logs } from '../models/Logs';
import crypto = require('crypto');
export class CandidatosController extends BaseController {
	protected _modelName = 'Candidatos';

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async get(req: express.Request, res: express.Response) {
		// Monta os campos
		const fields: Array<FieldInterface> = [
			{ name: 'CandidatoId', type: FieldTypeEnum.NUMBER },
			{ name: 'Nome', type: FieldTypeEnum.STRING, orderDefault: OrderEnum.ASC },
			{ name: 'Email', type: FieldTypeEnum.STRING }
		];

		// Monta os includes
		const includes = [];

		try {
			this.showSuccess(await this._get(req, fields, includes), res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * login
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 */
	public async login(req: express.Request, res: express.Response) {
		// Recebe os dados
		const email = req.body['Email'];
		const senha = req.body['Senha'];
		const source: string = req.headers.source + '';

		// Valida se as informações foram recebidas
		if (!email || !senha) {
			return this.showError('Parâmetros obrigatórios não informados', res);
		}

		try {
			// Realiza o login
			const candidato: any = await (this.model(this._modelName) as Candidatos).login(
				email,
				crypto
					.createHash('md5')
					.update(senha)
					.digest('hex'),
				source
			);

			// Busca as informações do grupo do usuário
			const grupo: any = await this.model('UsuariosGrupos').ORM.findById(candidato['UsuarioGrupoId']);

			// Monta o link da requisição
			const fullUrl = req.protocol + ':// ' + req.get('host') + req.originalUrl;

			// Registra o acesso do usuário
			(this.model('Logs') as Logs).registra_log('O usuário "' + email + '" logou com sucesso', 'AC', fullUrl, candidato['UsuarioId']);

			const retorno = {
				Token: candidato.Token,
				UsuarioId: candidato.UsuarioId,
				Nome: candidato.Nome,
				Grupo: grupo.Grupo,
				UsuarioGrupoId: candidato.UsuarioGrupoId,
				Email: email,
				Telefone: candidato.Telefone
			};

			// Devolve as informações da sessão
			this.showSuccess(retorno, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * logout
	 *
	 * logout method
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async logout(req: express.Request, res: express.Response) {
		// get user's id
		const candidato_id = req.headers['candidato_id'];

		try {
			// clean token
			const update = { [req.headers.source === 'app' ? 'TokenApp' : 'Token']: null };

			// find the user
			const user: any = await this.model(this._modelName).ORM.findById(+candidato_id);

			// Logout
			await user.updateAttributes(update);

			// registrate the user logout
			(this.model('Logs') as Logs).registra_log('O candidato "' + user.Email + '" deslogou com sucesso', 'AC');

			// its ok
			return this.showSuccess({ logout: true }, res);
		} catch (e) {
			// its ok
			return this.showError(e, res);
		}
	}

	/**
	 * set_form
	 *
	 * Executa a busca de um item específico e devolve a lista para os campos estrangeiros, por exemplo
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async set_form(req: express.Request, res: express.Response) {
		const visualizacao = req.query['finalidade'] === 'visualizar';

		// Monta as listas associativas
		const itensOpcoes = {
			RedesSociaisOpcoes: {
				model: 'RedesSociais',
				params: {
					field: 'RedeSocial',
					includes: [
						{
							model: this.model('CandidatosRedesSociais').ORM,
							as: 'CandidatoRedeSocial',
							required: visualizacao,
							where: req.query['id'] ? { CandidatoId: req.query['id'] } : {},
							attributes: ['Link']
						}
					]
				}
			},
			HabilidadesOpcoes: {
				model: 'Habilidades',
				params: {
					field: 'Habilidade',
					includes: [
						{
							model: this.model('CandidatosHabilidades').ORM,
							as: 'CandidatoHabilidade',
							required: visualizacao,
							where: req.query['id'] ? { CandidatoId: req.query['id'] } : {},
							attributes: ['Nivel']
						}
					]
				}
			},
			CargosOpcoes: {
				model: 'Cargos',
				params: { field: 'Cargo' }
			},
			CursosOpcoes: {
				model: 'Cursos',
				params: {
					field: 'Curso',
					includes: [
						{
							model: this.model('GrausEscolaridades').ORM,
							as: 'GrauEscolaridade',
							required: true,
							attributes: ['GrauEscolaridade']
						}
					]
				}
			}
		};

		if (!visualizacao) {
			Object.assign(itensOpcoes, {
				AtivoOpcoes: {
					model: this._modelName,
					custom: 'associativeListAttr',
					params: 'ATIVO_OPCOES'
				},
				SexoOpcoes: {
					model: this._modelName,
					custom: 'associativeListAttr',
					params: 'SEXO_OPCOES'
				},
				PcdOpcoes: {
					model: this._modelName,
					custom: 'associativeListAttr',
					params: 'PCD_OPCOES'
				},
				PeriodoExperienciaOpcoes: {
					model: 'CandidatosExperiencias',
					custom: 'associativeListAttr',
					params: 'PERIODO_OPCOES'
				},
				ValidadoOpcoes: {
					model: this._modelName,
					custom: 'associativeListAttr',
					params: 'VALIDADO_OPCOES'
				}
			});
		}

		// Busca os dados
		const retorno = await this._setForm(req, res, 'Candidato', itensOpcoes);
		delete retorno['Candidato']['Senha'];

		if (visualizacao) {
			const candidato = retorno['Candidato'];
			const candidatosModel = this.model(this._modelName) as Candidatos;
			candidato['Ativo'] = candidatosModel.ATIVO_OPCOES[candidato['Ativo']];
			candidato['Sexo'] = candidatosModel.SEXO_OPCOES[candidato['Sexo']];
			candidato['Pcd'] = candidatosModel.PCD_OPCOES[candidato['Pcd']];
			candidato['Validado'] = candidatosModel.VALIDADO_OPCOES[candidato['Validado']];
		}

		retorno['Habilidades'] = [];

		for (const i in retorno['HabilidadesOpcoes']) {
			const habilidade = retorno['HabilidadesOpcoes'][i];

			if (habilidade.CandidatoHabilidade && habilidade.CandidatoHabilidade.dataValues) {
				habilidade.Nivel = habilidade.CandidatoHabilidade.dataValues.Nivel;
				retorno['Habilidades'].push(i);
			}

			delete habilidade.CandidatoHabilidade;
		}
		if (visualizacao) {
			delete retorno['Habilidades'];
		}

		if (!visualizacao) {
			retorno['RedesSociais'] = {};
		}

		for (const redeSocial of retorno['RedesSociaisOpcoes']) {
			if (!visualizacao && redeSocial.CandidatoRedeSocial && redeSocial.CandidatoRedeSocial.dataValues) {
				retorno['RedesSociais'][redeSocial.key] = redeSocial.CandidatoRedeSocial.dataValues.Link;
			}

			if (visualizacao && redeSocial.CandidatoRedeSocial && redeSocial.CandidatoRedeSocial.dataValues) {
				redeSocial['Link'] = redeSocial.CandidatoRedeSocial.dataValues.Link;
			}

			delete redeSocial.CandidatoRedeSocial;
		}

		if (req.query['id']) {
			retorno['Escolaridades'] = await this.model('CandidatosEscolaridades').ORM.findAll({
				where: <any>{ CandidatoId: req.query['id'] },
				raw: true
			});

			if (visualizacao) {
				if (retorno['Escolaridades']) {
					const cursosOpcoes: { [key: number]: { CursoId: number; Curso: string; GrauEscolaridade: string } } = {};

					const periodoOpcoes = (this.model('CandidatosExperiencias') as CandidatosExperiencias).PERIODO_OPCOES;

					for (const curso of retorno['CursosOpcoes']) {
						cursosOpcoes[curso.key] = {
							CursoId: curso.key,
							Curso: curso.value,
							GrauEscolaridade: curso.GrauEscolaridade.GrauEscolaridade
						};
					}

					for (const escolaridade of retorno['Escolaridades']) {
						escolaridade['Periodo'] = periodoOpcoes[escolaridade['Periodo']];
						escolaridade['Curso'] = cursosOpcoes[escolaridade['CursoId']].Curso;
						escolaridade['GrauEscolaridade'] = cursosOpcoes[escolaridade['CursoId']].GrauEscolaridade;

						delete escolaridade['CandidatoId'];
						delete escolaridade['CandidatoEscolaridadeId'];
						delete escolaridade['CursoId'];
					}
				}
				delete retorno['CursosOpcoes'];
			}

			retorno['Experiencias'] = await this.model('CandidatosExperiencias').ORM.findAll({
				where: <any>{ CandidatoId: req.query['id'] },
				raw: true
			});

			if (visualizacao) {
				if (retorno['Experiencias']) {
					const cargosOpcoes: { [key: number]: { CargoId: number; Cargo: string } } = {};

					const periodoOpcoes = (this.model('CandidatosExperiencias') as CandidatosExperiencias).PERIODO_OPCOES;

					for (const cargo of retorno['CargosOpcoes']) {
						cargosOpcoes[cargo.key] = { CargoId: cargo.key, Cargo: cargo.value };
					}

					for (const escolaridade of retorno['Experiencias']) {
						escolaridade['Periodo'] = periodoOpcoes[escolaridade['Periodo']];
						escolaridade['Cargo'] = cargosOpcoes[escolaridade['CargoId']].Cargo;

						delete escolaridade['CandidatoId'];
						delete escolaridade['CandidatoExperienciaId'];
						delete escolaridade['CargoId'];
					}
				}
				delete retorno['CargosOpcoes'];
			}
		}

		// Retorna
		return this.showSuccess(retorno, res);
	}

	/**
	 * save
	 *
	 * Salva os dados
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public async save(req: express.Request, res: express.Response) {
		// Recebe os dados
		const candidato: any = {
			id: req.body['id'],
			Nome: req.body['Nome'],
			Sexo: req.body['Sexo'],
			Email: req.body['Email'],
			Telefone: req.body['Telefone'],
			Cpf: req.body['Cpf'],
			Rg: req.body['Rg'],
			Endereco: req.body['Endereco'],
			Pcd: req.body['Pcd'],
			Deficiencia: req.body['Deficiencia'],
			Nascimento: req.body['Nascimento'],
			Observacoes: req.body['Observacoes'],
			Complemento: req.body['Complemento'],
			Ativo: req.body['Ativo'],
			Validado: req.body['Validado']
		};

		// Verifica se precisa tratar a senha
		if (req.body['Senha']) {
			// Tenta buscar o usuário atual (se necessário)
			try {
				let candidatoAtual: any;
				if (candidato['id']) {
					candidatoAtual = await this.model(this._modelName).ORM.findById(candidato['id']);

					// retorna erro se usuário não existir
					if (!candidatoAtual) {
						return this.showError({ Geral: 'Parâmetros obrigatórios não enviados' }, res);
					}
				}

				// confirma se senha é a mesma de confirma senha
				if (req.body['Senha'] === req.body['ConfirmaSenha']) {
					candidato['Senha'] = crypto
						.createHash('md5')
						.update(req.body['Senha'])
						.digest('hex');
				} else {
					throw 'Senha inválida';
				}
			} catch (e) {
				return this.showError({ Geral: 'Senha inválida' }, res);
			}
		}

		// Executa o save
		try {
			// Faz envio de email de boas vindas
			// TODO
			if (!candidato.id && candidato.id) {
				const sender = new Email();
				sender.new_user(candidato.Email, candidato.Nome, null);
			}

			const data = await this._save(candidato, req);

			const candidatoId = data.id;

			await this.__atualizaRedesSociais(req, candidatoId);
			await this.__atualizaHabilidades(req, candidatoId);
			await this.__atualizaEscolaridades(req, candidatoId);
			await this.__atualizaExperiencias(req, candidatoId);

			this.showSuccess(data, res);
		} catch (error) {
			this.showError(error, res);
		}
	}

	private async __atualizaRedesSociais(req: express.Request, candidatoId: number) {
		try {
			const redesSociais = req.body['RedesSociais'];

			if (redesSociais === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('CandidatosRedesSociais').ORM.destroy({
					where: <any>{ CandidatoId: candidatoId }
				});
			}

			const candidatosRedesSociais = [];

			for (const redeSocialId in redesSociais) {
				if (redesSociais[redeSocialId]) {
					candidatosRedesSociais.push({
						CandidatoId: candidatoId,
						RedeSocialId: redeSocialId,
						Link: redesSociais[redeSocialId]
					});
				}
			}

			await this.model('CandidatosRedesSociais').ORM.bulkCreate(candidatosRedesSociais);
		} catch (err) {
			console.error('__atualizaRedesSociais error', err);
		}
	}

	private async __atualizaHabilidades(req: express.Request, candidatoId: number) {
		try {
			const habilidades = req.body['Habilidades'];

			if (habilidades === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('CandidatosHabilidades').ORM.destroy({
					where: <any>{ CandidatoId: candidatoId }
				});
			}

			const candidatosHabilidades = [];

			for (const habilidadeId in habilidades) {
				if (habilidades[habilidadeId]) {
					candidatosHabilidades.push({
						CandidatoId: candidatoId,
						HabilidadeId: habilidadeId,
						Nivel: habilidades[habilidadeId]
					});
				}
			}

			await this.model('CandidatosHabilidades').ORM.bulkCreate(candidatosHabilidades);
		} catch (err) {
			console.error('__atualizaHabilidades error', err);
		}
	}

	private async __atualizaEscolaridades(req: express.Request, candidatoId: number) {
		try {
			const escolaridades = req.body['Escolaridades'];

			if (escolaridades === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('CandidatosEscolaridades').ORM.destroy({
					where: <any>{ CandidatoId: candidatoId }
				});
			}

			const candidatosEscolaridades = [];

			for (const escolaridade of escolaridades) {
				candidatosEscolaridades.push({
					CandidatoId: candidatoId,
					CursoId: escolaridade.CursoId,
					Instituicao: escolaridade.Instituicao,
					Periodo: escolaridade.Periodo,
					Inicio: escolaridade.Inicio,
					Termino: escolaridade.Termino
				});
			}

			await this.model('CandidatosEscolaridades').ORM.bulkCreate(candidatosEscolaridades);
		} catch (err) {
			console.error('__atualizaEscolaridades error', err);
		}
	}

	private async __atualizaExperiencias(req: express.Request, candidatoId: number) {
		try {
			const experiencias = req.body['Experiencias'];

			if (experiencias === undefined) {
				return false;
			}

			if (req.body['id']) {
				await this.model('CandidatosExperiencias').ORM.destroy({
					where: <any>{ CandidatoId: candidatoId }
				});
			}

			const candidatosExperiencias = [];

			for (const experiencia of experiencias) {
				candidatosExperiencias.push({
					CandidatoId: candidatoId,
					CargoId: experiencia.CargoId,
					Empresa: experiencia.Empresa,
					Periodo: experiencia.Periodo,
					Jornada: experiencia.Jornada,
					Entrada: experiencia.Entrada,
					Saida: experiencia.Atualmente ? null : experiencia.Saida
				});
			}

			await this.model('CandidatosExperiencias').ORM.bulkCreate(candidatosExperiencias);
		} catch (err) {
			console.error('__atualizaExperiencias error', err);
		}
	}

	public async salvaObservacoes(req: express.Request, res: express.Response) {
		try {
			const candidato = {
				id: req.body['candidatoId'],
				Observacoes: req.body['observacoes']
			};

			const data = await this._save(candidato, req);

			this.showSuccess(data, res);
		} catch (error) {
			this.showError(error, res);
		}
	}

	/**
	 * forgot_password
	 *
	 * Forgot password method
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 */
	public forgot_password(req: express.Request, res: express.Response): void {
		// Recebe o e-mail
		const email = req.body['Email'];

		// Valida
		if (!email) {
			this.showError('Parâmetros obrigatórios não enviados', res);
		}

		// Gera a nova senha
		(this.model(this._modelName) as Candidatos)
			.nova_senha(email)
			.then((response: any) => {
				// Faz o envio
				const sender = new Email();
				sender.forgot_password(email, response.Nome, response.NovaSenha);

				// Armazena o log
				(this.model('Logs') as Logs).registra_log('O usuário "' + email + '" solicitou uma nova senha', 'AC');

				// Sucesso
				this.showSuccess(response, res);
			})
			.catch(err => {
				this.showError(err, res);
			});
	}
}
