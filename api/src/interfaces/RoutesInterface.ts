/**
 * RoutesInterface
 *
 * Interface for routes configuration
 *
 * @author Murilo Spinoza de Arruda
 */
export interface RoutesInterface {
	route: string;
	controller: string;
	function: string;
	method?: string;
	is_public?: boolean;
}
