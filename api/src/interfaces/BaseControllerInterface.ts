import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';

/**
 * FieldInterface
 *
 * Interface for fields of _get
 *
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 */
export interface FieldInterface {
	/**
	 * Nome do campo no model
	 */
	name: string;

	/**
	 * Nome do model do relacionamento
	 */
	join?: string;

	/**
	 * Alguma função diferente do sequelize que seja necessária, por exemplo.
	 */
	filter?: any;

	/**
	 * Tipo de valor recebido
	 */
	type: FieldTypeEnum;

	/**
	 * Objeto para converter no grid de S|N para Sim|Não, por exemplo.
	 */
	formatter?: any;

	/**
	 * Colocado em um dos campos com a direção padrão
	 */
	orderDefault?: OrderEnum;
}
