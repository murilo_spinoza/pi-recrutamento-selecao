/**
 * URLInterface
 *
 * Interface for URL params
 *
 * @author Murilo Spinoza de Arruda
 */
export interface URLInterface {
	controller: string;
	function: string;
	params: Array<any>;
}
