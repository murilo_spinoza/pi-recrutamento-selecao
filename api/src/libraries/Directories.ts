import * as fs from 'fs';
import * as path from 'path';

/**
 * Directories
 *
 * An static class with directories methods
 *
 * @author Murilo Spinoza de Arruda
 */
export default class Directories {
	// app dir
	public static app_dir: string = global['app_dir'];

	/**
	 * getDir
	 *
	 * get dir from file
	 */
	public static getDir(src_path: string): string {
		// return the app dir
		return path.resolve(Directories.app_dir, src_path);
	}

	/**
	 * getFiles
	 *
	 * get all files from a directory
	 *
	 * @param srcpath path where to get all files from
	 * @param remove_ext extension to remove
	 * @param exclude_items items to remove
	 * @returns an array with de files names
	 */
	public static getFiles(src_path: string, remove_ext?: Array<string>, exclude_items?: Array<string>): Array<string> {
		// read all files from the app path
		const resolved_path = Directories.getDir(src_path);
		let files = new Array<string>();
		if (fs.existsSync(resolved_path)) {
			files = fs.readdirSync(resolved_path).filter(file => {
				// check if current item is a file
				return !fs.statSync(path.join(resolved_path, file)).isDirectory();
			});
		}

		// parse the files
		for (const index in files) {
			// remove extension?
			if (remove_ext) {
				for (const item of remove_ext) {
					files[index] = files[index].replace(`.${item}`, '');
				}
				files[index] = files[index].replace(`.${remove_ext}`, '');
			}

			// exclude?
			if (exclude_items) {
				if (exclude_items.indexOf(files[index]) > -1) {
					files.splice(+index, 1);
				}
			}
		}

		// return the files
		return files;
	}

	/**
	 * getFile
	 *
	 * get one file
	 *
	 * @param srcpath path where to get all files from
	 * @returns the file content
	 */
	public static getFile(src_path: string, file: string): string {
		// return the file
		return fs.readFileSync(Directories.getDir(src_path + file)).toString();
	}
}
