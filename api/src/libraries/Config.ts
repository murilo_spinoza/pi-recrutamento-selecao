/**
 * Config
 *
 * Get the configs
 *
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 */
import * as express from 'express';
import Directories from './Directories';
export default class Config {
	// Storage data
	private __data: Object = {};

	// the current config object
	private __config_items: string;

	/**
	 * constructor
	 *
	 * Read the config files
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @return void
	 */
	constructor(config_items: string) {
		// set the collection for the instance
		this.__config_items = config_items;

		// get all the config files
		const config_files = Directories.getFiles('./config', ['ts', 'js']);

		// get all config objects
		for (const f in config_files) {
			// trying to load the configuration file
			try {
				// get the file content
				const dir = Directories.getDir(`./config/${config_files[f]}`);
				const inner = require(dir)[config_files[f]];

				// check if it is undefined
				if (typeof inner === 'undefined') {
					throw new Error('The exported object and the configuration file must have the same name');
				} else {
					// get the enviroment
					const env: string = express().settings.env;

					// save the data
					this.__data[config_files[f]] = inner[env] ? inner[env] : inner;
				}
			} catch (e) {
				// catch the error
				throw e;
			}
		}
	}

	/**
	 * item
	 *
	 * Get the item data
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @return Object
	 */
	item(key: string): any {
		return this.__data[this.__config_items][key] || {};
	}

	/**
	 * items
	 *
	 * Get all items
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @return Object
	 */
	items(): any {
		return this.__data[this.__config_items] || {};
	}
}
