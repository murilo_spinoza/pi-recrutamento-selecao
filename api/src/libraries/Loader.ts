import Directories from './Directories';
import * as Sequelize from 'sequelize';

/**
 * Loader
 *
 * A class for loading Models and Controllers
 *
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 */
class Loader {
	/**
	 * __controllers
	 *
	 * @private
	 * @type {Array<string>}
	 */
	private __controllers: Array<string>;

	/**
	 * __models
	 *
	 * @private
	 * @type {Array<string>}
	 */
	private __models: Array<string>;

	/**
	 * constructor
	 *
	 * set the controllers and models
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @return {void}
	 */
	constructor() {
		this.__setControllers();
		this.__setModels();
	}

	/**
	 * __setControllers
	 *
	 * read all controllers files
	 *
	 * @private
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 */
	private __setControllers(): void {
		// get all controllers files
		this.__controllers = Directories.getFiles('./controllers', ['js', 'ts'], ['index']);
	}

	/**
	 * __setModels
	 *
	 * read all models files
	 *
	 * @private
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 */
	private __setModels(): void {
		// get all controllers files
		this.__models = Directories.getFiles('./models', ['js', 'ts'], ['index']);
	}

	/**
	 * isController
	 *
	 * check if a string is a valid controller name
	 *
	 * @private
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {string} name the controller name
	 * @return {boolean}
	 */
	public isController(name: string): boolean {
		return this.__controllers.indexOf(name) !== -1;
	}

	/**
	 * isModel
	 *
	 * check if a string is a valid model name
	 *
	 * @private
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {string} name the model name
	 * @return {boolean}
	 */
	public isModel(name: string): boolean {
		return this.__models.indexOf(name) !== -1;
	}

	/**
	 * model
	 *
	 * load the passed model
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {string} name the model name
	 * @return a instance of the model
	 */
	public model(name: string, connector: Sequelize.Sequelize) {
		// check if a model with the informed name exists
		if (this.isModel(name)) {
			// try to load the model
			try {
				// load the model
				const dir = Directories.getDir(`./models/${name}`);
				const model = require(dir)[name];

				// return a new instance of the object
				return new model(connector);
			} catch (e) {
				// if the loading failed
				throw new Error(`Was not possible to load ${name} Model`);
			}
		} else {
			// if the model doesnt exists
			throw new Error(`${name} is not Model`);
		}
	}

	/**
	 * controller
	 *
	 * load the passed controller
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {string} name the controller name
	 * @return a instance of the controller
	 */
	public controller(name: string) {
		// check if a controller with the informed name exists
		if (this.isController(name)) {
			// try to load the controller
			try {
				// load the controller
				const dir = Directories.getDir(`./controllers/${name}`);
				const controller = require(dir)[name];

				// return a new instance of the object
				return new controller();
			} catch (e) {
				// if the loading failed
				console.error(e);
				throw new Error(`Was not possible to load ${name} Controller`);
			}
		} else {
			// if the controller doesnt exists
			throw new Error(`${name} is not Controller`);
		}
	}

	/**
	 * models
	 *
	 * load all models
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @return an array with an instance of each model
	 */
	public models(connector: Sequelize.Sequelize) {
		// an array for each model instance
		const loaded = new Object();

		// for each model
		for (const m in this.__models) {
			// try to load the model
			try {
				// set model as loaded
				loaded[this.__models[m]] = this.model(this.__models[m], connector);
			} catch (err) {
				console.error(err);
			}
		}

		// return the array
		return loaded;
	}
}

// export a new instance of the Loader class
export default new Loader();
