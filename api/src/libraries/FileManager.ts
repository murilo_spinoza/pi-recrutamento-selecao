/**
 * FileManager
 *
 * Manages file uploads
 *
 */
import Config from './Config';
import * as multer from 'multer';
import crypto = require('crypto');
import path = require('path');
export default class FileManager {
	// Config and Multer
	private __config: Config = new Config('files');
	public MulterUpload;

	/**
	 * constructor
	 *
	 * Personalize multer
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @return void
	 */
	constructor(field: string = 'file', pathKey: FileType = FileType.IMAGES) {
		// personalize multer
		const storage = multer.diskStorage({
			// set destination
			destination: (req, file, cb) => {
				const img_dir = this.__config.item(pathKey);
				cb(null, img_dir.toString());
			},
			filename: (req, file, cb) => {
				// make a new file name
				let filename = new Date().valueOf().toString() + Math.random().toString();
				filename = crypto
					.createHash('md5')
					.update(filename)
					.digest('hex');

				// get the file extension
				let ext = path.extname(file.originalname.toLowerCase());

				// there is something beyong the question mark (?)
				const temp = ext.split('?');
				if (temp[0]) { ext = temp[0]; }

				// set the new file name
				cb(null, `${filename}${ext}`);
			}
		});

		// starts the upload
		this.MulterUpload = multer({ storage }).single(field);
	}

	/**
	 * upload
	 *
	 * Execute the upload of the file
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {express.Request}  req the express request object
	 * @param  {express.Response} res the express response object
	 * @param  {boolean} required if the upload is required or not
	 * @return {Promise}
	 */
	public upload(req, res, required) {
		// return a new promise
		return new Promise((resolve, reject) => {
			// try to do the upload
			this.MulterUpload(req, res, err => {
				// check if there is any error
				if (err) {
					reject(err);
					return false;
				}

				// the file did not upload and it is required
				if (!req.file && required) {
					reject('File not sent');
				} else if (req.file) {
					// get the file extension
					let ext = path.extname(req.file.originalname.toLowerCase());

					// there is something beyong the question mark (?)
					const temp = ext.split('?');
					if (temp[0]) { ext = temp[0]; }

					// set the file object
					const metadata = {
						ext,
						name: req.file.filename,
						size: req.file.size,
						originalname: req.file.originalname,
						mimetype: req.file.mimetype
					};

					// return a new file object
					resolve(metadata);
				} else { resolve(true); }
			});
		});
	}
}

/**
 * enum Files
 *
 * @public
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 */
export enum FileType {
	IMAGES = 'images',
	AUDIOS = 'audios',
	DOCS = 'docs'
}
