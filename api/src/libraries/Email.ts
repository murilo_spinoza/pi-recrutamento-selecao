/**
 * Email
 *
 * Biblioteca responsável por fazer envios de e-mails
 *
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 */
import * as nodemailer from 'nodemailer';
import * as Handlebars from 'handlebars';
import * as fs from 'fs';
import Config from './Config';
import Directories from './Directories';
import Translator from './Translator';
export default class Email {
	/**
	 * Armazena o arquivo de configuração
	 */
	private __config = new Config('email');
	private __configUrls = new Config('urls');

	/**
	 * Armazena o transportador
	 */
	private __transporter: nodemailer.Transporter;

	/**
	 * construtor
	 *
	 * Seta o transportador
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 */
	constructor() {
		this.__transporter = nodemailer.createTransport(this.__config.items());
	}

	/**
	 * _send
	 *
	 * Faz o envio
	 *
	 * @access private
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {string}  destinatário
	 * @param  {string}  assunto
	 * @param  {string}  nome do arquivo HTML para template
	 * @param  {Object}  dados enviados ao template
	 * @param  {string}  nome do remetente (se vazio, pegará do arquivo de configuração)
	 * @param  {string}  e-mail do remetente (se vazio, pegará do arquivo de configuração)
	 * @param  {Array<Object>}  anexo, exemplos: https://community.nodemailer.com/using-attachments/
	 */
	protected async _send(to, subject, template, template_data?, from_name?, from_email?, anexo?): Promise<any> {
		try {
			// Compila o HTML
			const source = Directories.getFile('./emails/', template + '.html');
			const hb = Handlebars.compile(source);
			const html = hb(template_data);

			// Busca o remetente
			from_name = from_name ? from_name : this.__config.item('from')['name'];
			from_email = from_email ? from_email : this.__config.item('from')['email'];
			const from = from_name + '<' + from_email + '>';

			// Monta as opções do e-mail
			const options = {
				from: from,
				to: to,
				subject: subject,
				text: html.replace(/<[^>]*>/g, ''),
				html: html,
				attachments: anexo
			};

			// Faz o envio
			const success = await this.__transporter.sendMail(options);
			return success;
		} catch (error) {
			throw error;
		}
	}

	/**
	 * forgot_password
	 *
	 * Faz o envio de uma nova senha
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 */
	public forgot_password(para, nome, senha): Promise<any> {
		// Busca os dados
		const data = {
			hello: Translator.translate('ForgotPassword', 'hello', nome),
			action: Translator.translate('ForgotPassword', 'action'),
			information: Translator.translate('ForgotPassword', 'information'),
			password: senha
		};

		// Retorna o envio
		return this._send(para, Translator.translate('ForgotPassword', 'subject'), 'forgot_password', data);
	}

	/**
	 * new_user
	 *
	 * Faz o envio de email para novos usuários
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 */
	public new_user(para, nome, senha): Promise<any> {
		// Busca os dados
		const data = {
			title: Translator.translate('NewUsers', 'message'),
			email: para,
			nome: nome,
			password: senha,
			url: this.__configUrls.item('sistema_url')
		};

		// Retorna o envio
		return this._send(para, Translator.translate('NewUsers', 'title'), 'new_user', data);
	}

	/**
	 * sendReport
	 *
	 * Envia relatório por email
	 *
	 * @access public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {Object}  contem: nome, email, assunto e mensagem
	 */
	public sendContact(dados): Promise<any> {
		// Retorna o envio
		return this._send(this.__config.item('from')['email'], 'Contato Sistema', 'contact', dados);
	}
}
