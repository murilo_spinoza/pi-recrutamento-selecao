/**
 * Translator
 *
 * Classe para tradução das mensagens
 *
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 *
 */
import { STRINGS } from '../languages';
import * as util from 'util';
import Bootstrap from './Bootstrap';
export default class Translator {
	public static translate(pattern, value, ...args): string {
		// analisa se existem argumentos - caso venha undefined o format escreve undefined no texto
		let str = '';
		if (args.length == 0) { str = util.format(STRINGS[Bootstrap.language][pattern][value]); }
		else { str = util.format(STRINGS[Bootstrap.language][pattern][value], args); }

		// retorna
		return str;
	}
}
