import Loader from './Loader';
import Connectors from '../core/Connectors';
import { CustomModel } from '../core/CustomModel';
export default class Bootstrap {
	/**
	 * bootstraped
	 */
	public static bootstraped = false;

	/**
	 * __modelInstances
	 */
	private static __modelInstances: Object;

	/**
	 * language
	 */
	public static language: string;

	/**
	 * __setModelInstances
	 *
	 * get all instances of each model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	private static __setModelInstances() {
		Bootstrap.__modelInstances = Loader.models(Connectors.sequelize);
	}

	/**
	 * model
	 *
	 * get all instances of each model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static model(name: string): CustomModel {
		return Bootstrap.__modelInstances[name];
	}

	/**
	 * debug
	 *
	 * set debug of a model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static debug(model: string, debug = true) {
		Bootstrap.__modelInstances[model].debug = debug;
	}

	/**
	 * getFields
	 *
	 * get Fields of model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static getFields(model) {
		return Bootstrap.__modelInstances[model].getFields();
	}

	/**
	 * setRelations
	 *
	 * set the relations
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static setRelations() {
		// for each model
		for (const m in Bootstrap.__modelInstances) {
			// get the model instance
			const instance = Bootstrap.__modelInstances[m];

			// check if there is any relations
			if (instance.relations) {
				// for each relation
				for (const r in instance.relations) {
					// set the relation
					const relation = instance.relations[r];
					const type = relation.type ? relation.type : r;

					// get the model of the relation
					const relationTo = Bootstrap.__modelInstances[relation.model];

					// try to add the relation
					try {
						// add the relation to the model
						instance.ORM[type](relationTo.ORM, relation.options);
					} catch (err) {
						console.error('Erro no mapeamento');
						console.error('relacao', relationTo ? 'setado' : relationTo);
						console.error('orm', instance.ORM);
						console.error(m);
					}
				}
			}
		}
	}

	/**
	 * start
	 *
	 * bootstrap all modules
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static start() {
		// set all model instances
		Bootstrap.__setModelInstances();
		Bootstrap.setRelations();

		// Bootstrap has finished
		console.log('Bootstrap finished successfully!');

		// set the module as bootstraped
		Bootstrap.bootstraped = true;
	}
}
