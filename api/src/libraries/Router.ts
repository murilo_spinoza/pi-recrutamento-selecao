import Loader from './Loader';
import Config from './Config';
import { Auth } from '../middlewares/Auth';
import { RoutesInterface } from '../interfaces/RoutesInterface';
import { URLInterface } from '../interfaces/URLInterface';
import * as express from 'express';
import * as cors from 'cors';
import Bootstrap from '../libraries/Bootstrap';

/**
 * Router Class
 *
 * Set the custom and dynamic routes
 *
 */
export default class Router {
	/**
	 * the app object
	 */
	protected _app;

	/**
	 * the config object
	 */
	private __config = new Config('routes');

	/**
	 * the app Auth
	 */
	protected _auth = Auth;

	/**
	 * constructor
	 *
	 * set the routes
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {object}  the app instance
	 * @return {void}
	 */
	constructor(app: express.Express) {
		// start the app
		this._app = app;

		// cors config
		this._corsConfig();

		// get the routes
		const routes = this.__config.item('routes');
		for (const r in routes) { this.setRoute(routes[r]); }

		// set the dynamic routes
		this.__setDynamicRoute();
	}

	protected _corsConfig(): void {
		// get the config
		const config = new Config('urls');

		const whitelist = [config.item('site_url'), config.item('sistema_url')];
		const corsOptions = {
			origin: (origin, callback) => {
				if (whitelist.indexOf(origin) !== -1) {
					callback(null, true);
				}
			},
			credentials: true
		};

		// use cors
		this._app.use(cors(corsOptions));
	}

	/**
	 * __setDynamicRoute
	 *
	 * set all defaults routes
	 *
	 * @private
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {void}
	 * @param {void}
	 */
	private __setDynamicRoute(): void {
		// authentication
		this._app.use('*', (req, res, next) => {
			this._auth.authenticate(req, res, next);
		});

		// dinamic created routes
		this._app.all('*', (req, res) => {
			// get the params and call method
			const params = this.__parseURL(req);
			this.__callMethod(params, req, res);
		});
	}

	/**
	 * setRoute
	 *
	 * set a new explicit route
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {RoutesInterface} a new route
	 * @return {void}
	 */
	public setRoute(route: RoutesInterface): void {
		// set 'all' as default method
		route.method = route.method ? route.method : 'all';

		// check if its public
		if (!route.is_public) {
			this._app.use(route.route, (req, res, next) => {
				this._auth.authenticate(req, res, next);
			});
		}

		// define the explicit routes
		this._app[route.method](route.route, (req, res) => {
			// get the params
			const params = this.__parseURL(req);
			route['params'] = params['params'];
			this.__callMethod(route, req, res);
		});
	}

	/**
	 * __callMethod
	 *
	 * Call the method
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {RoutesInterface}  route
	 * @param {express.Request}  req the requisition
	 * @param {express.Response} response
	 * @return {URLInterface}
	 */
	private __callMethod(route, req, res) {
		// try to load the controller and the method
		try {
			// get and set the language - in Auth.ts too
			const language = req.headers.language ? (req.headers.language as string) : 'pt';
			Bootstrap.language = language;

			// check if is a controller
			if (!Loader.isController(`${route.controller}Controller`)) {
				this.__error(req, res, `Controller ${route.controller} not found`);
				return false;
			}

			// load the controller
			const controller = Loader.controller(`${route.controller}Controller`);

			// check the function
			if (typeof controller[route.function] === 'function' || typeof controller[req.method.toLowerCase()] === 'function') {
				if (typeof controller[route.function] === 'function') {
					controller[route.function](req, res, route['params']);
				} else {
					controller[req.method.toLowerCase()](req, res, route.function);
				}
			} else {
				this.__error(req, res, `Method ${route.function} in ${route.controller}Controller not found`);
			}
		} catch (e) {
			// show the error and load not found page
			console.error(e);
			this.__notFound(req, res);
		}
	}

	/**
	 * __parseURL
	 *
	 * Parse the url
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {express.Request}  req the requisition
	 * @return {URLInterface}
	 */
	private __parseURL(req: express.Request): URLInterface {
		// Create the return
		const url: URLInterface = { controller: '', function: '', params: [] };

		// split params to get controller and method
		if (typeof req.params[0] !== 'undefined') {
			const params = req.params[0].split('/');
			params.splice(0, 1);

			// get controller and function
			url['controller'] = params[0] ? `${params[0].charAt(0).toUpperCase()}${params[0].slice(1)}` : '';
			url['function'] = params[1] ? params[1] : 'index';

			// set the controller in camelcase
			url['controller'] = url['controller'].replace(/_([a-z])/g, g => {
				return g[1].toUpperCase();
			});

			// get the url params
			url['params'] = [];
			if (params.length > 2) {
				for (const i in params) {
					// convert to number and push params in the array
					if (+i > 1) { url['params'].push(params[i]); }
				}
			}
		}

		// Return
		return url;
	}

	/**
	 * __notFound
	 *
	 * default route for not found pages
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {express.Request}  req the requisition
	 * @param {express.Response} res the response
	 * @return {void}
	 */
	private __notFound(req: express.Request, res: express.Response): void {
		res.statusCode = 401;
		res.json('Page Not Found');
	}

	/**
	 * __error
	 *
	 * default route for error
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {express.Request}  req the requisition
	 * @param {express.Response} res the response
	 * @return {void}
	 */
	private __error(req: express.Request, res: express.Response, error: any): void {
		res.statusCode = 500;
		res.json(error);
	}
}
