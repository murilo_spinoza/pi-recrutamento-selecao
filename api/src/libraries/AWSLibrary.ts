/**
 * AWSLibrary
 *
 * Classe para manipulação do sdk do web service da amazon - S3
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 *
 */
import AWS = require('aws-sdk');
import Config from './Config';
import * as fs from 'fs';
export default class AWSLibrary {
	private __s3: AWS.S3;

	/**
	 * the config object
	 */
	private __config = new Config('awsS3');

	/**
	 * constructor
	 *
	 * Set the S3 config
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since  03/2019
	 * @return void
	 */
	constructor() {
		try {
			// Update default config
			AWS.config.update({
				region: this.__config.item('region'),
				credentials: new AWS.Credentials(this.__config.item('accessKey'), this.__config.item('secretKey'), '')
			});

			// Instance
			this.__s3 = new AWS.S3({ apiVersion: this.__config.item('apiVersion') });
		} catch (e) {
			console.log('\n\nERRO NO S3\n\n', e, '\n\n');
		}
	}

	/**
	 * upload
	 *
	 * Move o arquivo para o s3 da amazon
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @param  {string} Pasta no bucket da amazon
	 * @param  {string} nome do arquivo
	 * @param  {string} caminho até o arquivo
	 * @return {void}
	 */
	upload(folder: string, filename: string, path: string) {
		return new Promise((resolve, reject) => {
			// Le o arquivo
			fs.readFile(path + filename, (err, data) => {
				// Se houver algum erro, traz a exceção
				if (err) {
					reject(err);
					return false;
				}

				// Sobe o arquivo para o bucket
				this.__s3.putObject(
					{
						Bucket: this.__config.item('bucket'),
						Key: folder + filename,
						Body: data,
						ACL: 'public-read'
					},
					resp => {
						// Verifica se ocorreu algum erro no s3
						if (resp) { reject(resp); }

						// Apaga o arquivo local
						fs.unlink(path + filename, erro => {
							// Se houver algum erro
							if (erro) {
								// Mostra a mensagem no terminal
								reject(err);
							}
						});
						resolve();
					}
				);
			});
		});
	}

	/**
	 * download
	 *
	 * Recebe um arquivo do S3
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @param  {string} Pasta no bucket da amazon
	 * @param  {string} Nome do arquivo
	 * @return {void}
	 */
	download(folder: string, filename: string): Promise<AWS.S3.GetObjectOutput> {
		return new Promise((resolve, reject) => {
			this.__s3.getObject(
				{
					Bucket: this.__config.item('bucket'),
					Key: folder + filename
				},
				(err, data) => {
					if (err) { reject(err); }
					else { resolve(data); }
				}
			);
		});
	}

	/**
	 * delete
	 *
	 * Apaga um arquivo do s3 da amazon
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @param  {string} Pasta no bucket da amazon
	 * @param  {string} nome do arquivo
	 * @return {void}
	 */
	delete(folder: string, filename: string, path: string) {
		// Apaga o arquivo local
		fs.unlink(path + filename, erro => {
			// Se houver algum erro
			if (erro) { console.log(erro); }
		});

		this.__s3.deleteObject(
			{
				Bucket: this.__config.item('bucket'),
				Key: folder + filename
			},
			(err, data) => {
				if (err) { console.log(err, err.stack); }
			}
		);
	}
}
