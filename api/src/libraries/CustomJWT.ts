/**
 *
 * CustomJWT
 *
 * Classe para manipulação e criação de JsonWebTokens.
 *
 * @author Murilo Spinoza de Arruda
 * @since 03/2019
 *
 */
import * as Token from 'jsonwebtoken';
export default class CustomJWT {
	/**
	 * chave usada na encriptação
	 */
	private __privateKey = 'osnfoHelpperanOJASOjs837doifjaosnKLJNOjas';

	/**
	 * algoritimo de encriptação
	 */
	private __algorithm = 'HS256';

	/**
	 * token gerado
	 */
	private __token: string;

	/**
	 * prazo de validade
	 */
	private __expiration = '';

	/**
	 * dados descriptografados
	 */
	private __decoded: {};

	/**
	 * Método construtor
	 *
	 * @param token token de inicialização ( não obrigatório )
	 */
	constructor(token?: string) {
		// seta o token se ele nao existir
		this.__token = token ? token : '';
	}

	/**
	 * set algoritihm
	 * define qual o algoritimo usado na geração do token
	 *
	 * @param type o algoritimo que será usado
	 */
	set algoritihm(type: string) {
		this.__algorithm = type;
	}

	/**
	 * set token
	 * seta um token qualquer
	 *
	 * @param token um token TokenScript
	 */
	set token(token: string) {
		this.token = token;
	}

	/**
	 * get token
	 * pega o token
	 *
	 * @return token um token TokenScript
	 */
	get token(): string {
		return this.__token;
	}

	/**
	 * get token
	 * pega o token decodificado
	 *
	 * @return token um token TokenScript
	 */
	get decoded(): Object {
		return this.__decoded;
	}

	/**
	 * set privateKey
	 * seta uma chave nova
	 *
	 * @param key uma chave contendo mais de 16 caracteres
	 */
	set privateKey(key: string) {
		this.__privateKey = key;
	}

	/**
	 * set expiration
	 * seta um prazo de validade para o token
	 *
	 * @param expiration um dado que pode ser convertido em data
	 */
	set expiration(expiration: string) {
		this.__expiration = expiration;
	}

	/**
	 * create
	 * Cria um novo TokenScript
	 *
	 * @param data um objeto que será guardado no token gerado
	 */
	public create(data: {}): string {
		// verifica se uma chave foi informada
		if (this.__privateKey.length === 0) {
			throw new Error('Nenhuma chave privada foi informada');
		} else if (this.__privateKey.length < 16) {
			throw new Error('A chave privada precisa ter mais de 16 caracters');
		}

		// objeto de config
		const config = {
			algorithm: this.__algorithm
		};

		// verifica se tem prazo de validade
		if (this.__expiration) {
			config['expiresIn'] = this.__expiration;
		}

		// cria o token
		this.__token = Token.sign(data, this.__privateKey, config);

		// volta o token
		return this.__token;
	}

	/**
	 * valid
	 * Informa se um TokenScript é válido ou não, de acordo com as regras definidas
	 */
	public valid(): boolean {
		// Tenta decodificar o token
		try {
			// pega os dados
			this.__decoded = Token.verify(this.__token, this.__privateKey);

			// Tudo ok
			return true;
		} catch (err) {
			// mostra o erro e retorna false
			console.error(err.message);
			return false;
		}
	}

	/**
	 * item
	 * Retorna o item informado do token decodificado
	 *
	 * @param key o item a ser recuperado
	 */
	public item(key: string): string {
		// verifica se a chave esta definida
		return typeof this.__decoded[key] === 'undefined' ? '' : this.__decoded[key];
	}
}
