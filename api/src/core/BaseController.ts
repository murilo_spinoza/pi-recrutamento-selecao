/**
 * BaseController
 *
 * Classe abstrata com as funções comuns a todos os controllers
 *
 * @author Murilo Spinoza de Arruda
 */
import * as express from 'express';
import Bootstrap from '../libraries/Bootstrap';
import { Op } from 'sequelize';
import { FieldInterface } from '../interfaces/BaseControllerInterface';
import { CustomModel } from './CustomModel';
import { FieldTypeEnum, OrderEnum } from '../enum/BaseControllerEnum';
import { Logs } from '../models/Logs';
export class BaseController {
	/**
	 * _modelName
	 */
	protected _modelName = '';

	/**
	 * index
	 *
	 * Define a função conforme o método
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param params Informações adicionais para o método
	 */
	public index(req: express.Request, res: express.Response, params?: Object): void {
		// Set the function
		const types = { GET: 'get', POST: 'save', PUT: 'save', DELETE: 'delete' };
		const local_function = types[req.method.toUpperCase()] || 'get';
		this[local_function](req, res, params);
	}

	/**
	 * showSuccess
	 *
	 * Exibe um retorno de sucesso
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public showSuccess(data: Object, res: express.Response) {
		// Adiciona o sucesso nos dados
		const ret = {};
		ret['status'] = true;
		ret['error'] = false;
		ret['data'] = data;

		// Exibe o retorno
		res.json(ret);
	}

	/**
	 * showError
	 *
	 * Exibe um retorno de erro
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public showError(error: any, res: express.Response) {
		console.log('\n\nErro:\n\n', error, '\n\n\n');

		// Adiciona erro nos dados
		const data = {};
		data['status'] = false;
		data['error'] = error;

		// Exibe o retorno
		res.json(data);
	}

	/**
	 * showAccessDenied
	 *
	 * Exibe um retorno de acesso negado
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public showAccessDenied(req: express.Request, res: express.Response) {
		// Adiciona erro nos dados
		const data = {};
		data['status'] = false;
		data['error'] = 'Acesso negado';

		// Exibe o retorno
		res.statusCode = 401;
		res.json(data);
	}

	/**
	 * showForbidden
	 *
	 * Exibe um retorno de acesso proibido
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public showForbidden(req: express.Request, res: express.Response) {
		// Adiciona erro nos dados
		const data = {};
		data['status'] = false;
		data['error'] = 'Acesso proíbido';

		// Exibe o retorno
		res.statusCode = 403;
		res.json(data);
	}

	/**
	 * model
	 *
	 * get an instance of a model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	protected model(name): CustomModel {
		return Bootstrap.model(name);
	}

	/**
	 * debug
	 *
	 * set debug of a model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public debug(model, debug) {
		Bootstrap.debug(model, debug);
	}

	/**
	 * _get
	 *
	 * Default get
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param  req the express request object
	 * @param  campos usados nos filtros
	 * @param  joins usados na busca
	 */
	protected async _get(req, fields: Array<FieldInterface>, includes?, whereDefault?, additionalParams = {}) {
		// Busca os parâmetros
		const params = {
			page: req.query['_page'] ? +req.query['_page'] : 1,
			limit: +req.query['_limit'],
			order: req.query['_sort'] ? [req.query['_sort'], req.query['_order']] : null
		};

		// Monta o where
		const where = {};
		const attributes = {};
		let location = 'default';

		if (whereDefault) {
			where[location] = whereDefault;
		}

		let hasEnum: any = false;

		for (const field of fields) {
			// Existe join?
			if (includes && field.join) {
				location = field.join;
			} else {
				location = 'default';
			}

			// Se possuir um concat, por exemplo
			const filter = field.filter || field.name;

			// Campo enumerado no sistema
			if (field.type === FieldTypeEnum.ENUM) {
				if (!hasEnum) {
					hasEnum = {};
				}

				hasEnum[field.name] = field.formatter;
			}

			// Se o campo for de ordenação padrão e não haver ordem
			if (field.orderDefault && !params.order) {
				params.order = [field.name, field.orderDefault || 'ASC'];
				// Existe join?
				if (includes && field.join) {
					params.order.unshift(field.join);
				}
			}

			// Existe join?
			if (includes && field.join) {
				// Existe join no sort?
				if (field.name === req.query['_sort']) {
					params['order'] = [field.join, field.name, req.query['_order'] || (field.orderDefault || 'ASC')];
				}
			}

			// Seta o where
			if (!where[location]) {
				where[location] = {};
			}

			// Seta os atribuutos
			if (!attributes[location]) {
				attributes[location] = [];
			}

			// Adiciona o campo
			const atribute = field.filter ? [field.filter, field.name] : field.name;
			attributes[location].push(atribute);

			// Seta o valor de filtro
			const value = req.query[field.name + '_like'];

			if (value) {
				// Variável que conterá o valor do where para um campo
				let whereValue: any;

				// De acordo com o tipo de campo, monta o valor do where
				switch (field.type) {
					case FieldTypeEnum.STRING:
						whereValue = { [Op.like]: '%' + value + '%' };
						break;
					case FieldTypeEnum.NUMBER:
					case FieldTypeEnum.ENUM:
						whereValue = value;
						break;
				}

				// Para a atribuição acontecer em um lugar só, invés de ser em cada case.
				where[location][field.name] = whereValue;
			}
		}

		// Monta os includes
		const joins = [];
		if (includes) {
			for (const include of includes) {
				if (where[include.as]) {
					include['where'] = where[include.as];
					include['attributes'] = attributes[include.as];
					joins.push(include.as);
				}
			}
		}

		// Monta a query
		let query = {
			attributes: attributes['default'],
			where: where['default'],
			offset: params['limit'] > 0 ? (params['page'] - 1) * params['limit'] : null,
			limit: params['limit'] > 0 ? params['limit'] : null,
			order: params['order'] ? [params['order']] : null,
			include: includes
		};

		query = Object.assign(query, additionalParams);

		// Executa a query
		const result = await this.model(this._modelName).ORM.findAndCountAll(query);

		// monta o retorno
		const tableData = {
			data: this._serializeDataSet(result.rows, joins),
			'x-total-count': result.count,
			formatter: hasEnum
		};

		// Retorna
		return tableData;
	}

	/**
	 * get
	 *
	 * Executa a listagem dos dados
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async get(req: express.Request, res: express.Response) {
		return this.showError('Get não implementado', res);
	}

	/**
	 * data_xls
	 *
	 * Executa a listagem dos dados para o XLS
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async data_xls(req: express.Request, res: express.Response) {
		return this.get(req, res);
	}

	/**
	 * _serializeDataSet
	 *
	 * Transforma um dataset do banco de dados
	 * Em um retorno serializado confonforme o exemplo abaixo:
	 *
	 *   {
	 *      Usuario: 'Fulano',
	 *      Grupo: {
	 *          GrupoId: '1',
	 *          Grupo: 'Teste'
	 *      }
	 *  }
	 *
	 *  =
	 *
	 *  {
	 *      Usuario: 'Fulano',
	 *      GrupoId: '1',
	 *      Grupo: 'Teste'
	 *  }
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	protected _serializeDataSet(rows, joins): Array<any> {
		// Inicia o retorno e valida
		const result = [];

		// Analisa o resultado
		for (const row of rows) {
			// Seta os dados
			const item = row['dataValues'];

			// Analisa os joins
			for (const join of joins) {
				// Verifica se existe
				if (item[join]) {
					// Faz todas as associações
					item[join] = Object.assign({}, item[join]['dataValues']);
					for (const key in item[join]) {
						item[key] = item[join][key];
					}

					// Elimina o item
					if (typeof item[join] === 'object') {
						delete item[join];
					}
				}
			}

			// Adiciona ao resultado final
			result.push(item);
		}

		// Sucesso
		return result;
	}

	/**
	 * _save
	 *
	 * Default save
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param data of save
	 */
	protected async _save(data: any, req: express.Request, otherIdField?: string) {
		const idField = otherIdField ? otherIdField : 'id';

		// Remove campos vazios (Necessário para postgresql que não salva null por padrão nos campos vazios)
		for (const key in data) {
			if (data[key] === undefined) {
				delete data[key];
			}
		}

		// get the changes
		const changes = await this.__searchChanges(data, idField);

		// call the save
		try {
			const success = await this.model(this._modelName).customSave(data);
			data.id = success;

			// changes?
			if (changes) {
				// create a new Logs object
				const log = Bootstrap.model('Logs') as Logs;

				// registrate the user log
				log.registra_log(
					'Ação executada pelo usuário no registro de id: ' + data.id,
					req.method,
					req.originalUrl,
					+req.headers['usuario_id'],
					changes
				);
			}

			// success
			return data;
		} catch (error) {
			throw error;
		}
	}

	/**
	 * __searchChanges
	 *
	 * Search for changes in data before of save
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	private async __searchChanges(data, idField) {
		// Busca os dados atuais
		let dadosAtuais = false;
		if (data[idField]) {
			dadosAtuais = await this.model(this._modelName).ORM.findById(data[idField]);
		}

		// Busca os campos
		let changes = '';
		const fieldsModel = Bootstrap.getFields(this._modelName);

		// Analisa os campos do model
		for (const field in fieldsModel) {
			// Analisa se vieram dados e remove a primary key da lista
			if ((!dadosAtuais[field] && !data[field]) || fieldsModel[field].primaryKey) {
				continue;
			}

			// Adicionando?
			if (!data[idField]) {
				if (data[field]) {
					// Remove eventuais campos que nao são monitorados: Senhas ou
					// data de modificação gerada automaticamente
					if (fieldsModel[field]['notLog']) {
						continue;
					}

					// Armazena as mudanças
					changes +=
						(fieldsModel[field]['label'] ? fieldsModel[field]['label'] : field) + ': "' + (data[field] ? data[field] : '') + '";';
				}
			} else {
				// Recebe os dados
				let dadoAtual = dadosAtuais[field];
				let dadoNovo = data[field];

				// Remove eventuais campos que nao são monitorados: Senhas ou
				// data de modificação gerada automaticamente
				if (fieldsModel[field]['notLog']) {
					continue;
				}

				// Precisa de alguma formatação para a exibição?
				if (fieldsModel[field]['formatLog']) {
					dadoAtual = fieldsModel[field]['formatLog'](dadoAtual);
					dadoNovo = fieldsModel[field]['formatLog'](dadoNovo);
				}

				// Busca a mudança
				if (dadoAtual !== dadoNovo) {
					changes +=
						(fieldsModel[field]['label'] ? fieldsModel[field]['label'] : field) +
						': "' +
						(dadoAtual ? dadoAtual : '') +
						'" => "' +
						(dadoNovo ? dadoNovo : '') +
						'";';
				}
			}
		}

		// Retorna as mudanças
		return changes;
	}

	/**
	 * delete
	 *
	 * Default delete
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async delete(req: express.Request, res: express.Response, id?: number) {
		try {
			const deleted = await this._delete(req, id);
			this.showSuccess(deleted, res);
		} catch (err) {
			this.showError(err, res);
		}
	}

	/**
	 * delete
	 *
	 * Default delete
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	protected async _delete(req: express.Request, id?: number) {
		// valida
		if (!id) {
			throw 'Parâmetros obrigatórios não enviados';
		}

		// tenta apagar
		try {
			// verifica as restrições
			const restrictions = await this.model(this._modelName).checkRestrictions(id);
			if (!restrictions) {
				// executa o delete
				const deleted = await this.model(this._modelName).customDel(id);

				// create a new Logs object
				const log = Bootstrap.model('Logs') as Logs;

				// registrate the user log
				log.registra_log('Usuário excluiu o registro de id: ' + id, req.method, null, +req.headers['usuario_id']);

				// sucesso
				return deleted;
			} else {
				throw restrictions;
			}
		} catch (e) {
			throw e;
		}
	}

	/**
	 * _setForm
	 *
	 * Busca dados do formulário
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param             nome do item padrão
	 * @param             itens para lista de opções
	 */
	protected async _setForm(req: express.Request, res: express.Response, itemDefault: string, itensOpcoes?: Object) {
		// Recebe os dados
		const id = req.query['id'];
		let item: any = false;

		// Verifica se deve buscar
		if (id) {
			item = await this.model(this._modelName).ORM.findById(id);
			item = item ? item['dataValues'] : false;
		}

		// Reinicializa (se necessário)
		if (!item) {
			item = {};
		}

		// Inicia o retorno
		const retorno = {};
		retorno[itemDefault] = item;

		// Verifica se deve buscar listas associativas
		if (itensOpcoes) {
			for (const key in itensOpcoes) {
				// Busca a função - definindo a associativeList como padrão
				const custom = itensOpcoes[key]['custom'] ? itensOpcoes[key]['custom'] : 'associativeList';
				retorno[key] = await this.model(itensOpcoes[key]['model'])[custom](itensOpcoes[key]['params']);
			}
		}

		// Tudo ok
		return retorno;
	}
}
