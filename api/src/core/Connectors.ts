import * as Sequelize from 'sequelize';
import Config from '../libraries/Config';
import Validator from './Validator';
export default class Connectors {
	/**
	 * __config
	 */
	public static config: Config = new Config('database');

	/**
	 * sequelize
	 */
	public static sequelize: Sequelize.Sequelize;

	/**
	 * setSequelize
	 *
	 * set the sequelize object
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	private static setSequelize() {
		// create the custom validators
		Validator.customize();

		// get the sql configs
		const sql = Connectors.config.item('sql');
		sql['operatorsAliases'] = Sequelize.Op;

		// try do connect to the database
		try {
			// set se sequelize configurations
			Connectors.sequelize = new Sequelize(sql['database'], sql['username'], sql['password'], sql);
		} catch (e) {
			// show the error
			console.error(e);
		}
	}

	/**
	 * use
	 *
	 * set all connectors
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static use(connectors) {
		// check the connectors to use
		if (connectors['sequelize']) {
			Connectors.setSequelize();
		}
	}
}
