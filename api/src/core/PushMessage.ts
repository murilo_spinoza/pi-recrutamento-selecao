/**
 * PushMessage
 *
 * class for PushMessage
 *
 * @author Murilo Spinoza de Arruda
 */
export default class PushMessage {
	private __userId: number;
	private __title: string;
	private __message: string;
	private __params: Object;
	private __router: string;

	constructor() {}

	public get userId(): number {
		return this.__userId;
	}

	public set userId(userId: number) {
		this.__userId = userId;
	}

	public get title(): string {
		return this.__title;
	}

	public set title(title: string) {
		this.__title = title;
	}

	public get router(): string {
		return this.__router;
	}

	public set router(router: string) {
		this.__router = router;
	}

	public get message(): string {
		return this.__message;
	}

	public set message(message: string) {
		this.__message = message;
	}

	public get params(): Object {
		return this.__params;
	}

	public set params(params: Object) {
		this.__params = params;
	}
}
