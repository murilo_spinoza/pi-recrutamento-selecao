/**
 * CustomModel
 *
 * Abstract class with all custom models methods
 *
 * @author Murilo Spinoza de Arruda
 */
import { BaseModel } from './BaseModel';
import Bootstrap from '../libraries/Bootstrap';
import Translator from '../libraries/Translator';
import { Op } from 'sequelize';
import { DefineAttributeColumnOptions } from 'sequelize';

export interface MyDefineAttributeColumnOptions extends DefineAttributeColumnOptions {
	notLog?: boolean;
}

/**
 * Interface for Attributes provided for a column
 */
export interface IAtributos {
	/**
	 * The description of a database column
	 */
	[name: string]: MyDefineAttributeColumnOptions;
}

export abstract class CustomModel extends BaseModel {
	/**
	 * customSave
	 *
	 * Save the data
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async customSave(data) {
		// try to update or create
		try {
			// check the unique
			const uniques = Object.keys(this._fields).filter(field => {
				return this._fields[field].unique === true;
			});

			// loop of uniques
			for (const unique of uniques) {
				if (data[unique]) {
					const check = await this.__isUnique(unique, data[unique], data['id']);
					if (!check) {
						// make the error
						const error = {};
						error[unique] = 'O registro "' + data[unique] + '" já existe em nosso banco de dados';
						throw error;
					}
				}
			}

			// update or save?
			if (data['id']) {
				const where = { [this._primary_key]: data['id'] };
				const id = data['id'];

				delete data['id'];

				const update = await this.ORM.update(data, { where: where });
				return id;
			} else {
				const create: any = await this.ORM.create(data);
				return create[this._primary_key];
			}
		} catch (err) {
			// Understand the error
			let errors = err;
			if (err['errors'] && err['errors'].length > 0 && err['name'] && err['name'] === 'SequelizeValidationError') {
				errors = {};
				for (const item of err['errors']) {
					// Translate the message
					errors[item['path']] = Translator.translate('Validator', item['validatorKey'], item['path']);
				}
			}
			throw errors;
		}
	}

	/**
	 * customDel
	 *
	 * Delete the data
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async customDel(id: number) {
		// try delete
		try {
			const destroy = await this.ORM.destroy({ where: { [this._primary_key]: id } });
			if (destroy && destroy > 0) {
				return true;
			} else {
				throw 'Erro ao deletar o registro';
			}
		} catch (err) {
			throw err;
		}
	}

	/**
	 * checkRestrictions
	 *
	 * Valida as restrições de exclusão de um registro
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param  {number} Id a ser excluído
	 */
	public async checkRestrictions(id) {
		try {
			if (this.relations) {
				for (const relation of this.relations) {
					if ((relation.type == 'hasMany' || relation.type == 'hasOne') && relation.depends) {
						const foreignRegisters = await Bootstrap.model(relation.model).ORM.findAll({
							where: { [relation.options.foreignKey]: id } as any
						});

						if (foreignRegisters && foreignRegisters.length > 0) {
							return Translator.translate('Errors', 'restrictions', relation.label || relation.model);
						}
					}
				}
			}

			return false;
		} catch (e) {
			console.log('\n\n\ncheckRestritions - E R R O\n\n\n', e);
			throw Translator.translate('Errors', 'no_restrictions');
		}
	}

	/**
	 * __isUnique
	 *
	 * Check if value is unique for field
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	private async __isUnique(field: string, value: string, id?: number): Promise<boolean> {
		// Initialize
		const where: any = {};

		// Set the primary key and where
		if (id) {
			where[this._primary_key] = { [Op.not]: id };
		}

		// Check in database
		where[field] = value;
		const unique: any = await this.ORM.findOne({
			attributes: [this._primary_key],
			where: where
		});

		// Its ok
		return !unique || unique.length <= 0;
	}

	/**
	 * associativeList
	 *
	 * Find the associative list in database
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param  {Object} params with:
	 *                  {string} - field name
	 *                  {Object} - where
	 *                  {string} - field index (default is a primary key)
	 *                  {Object} - includes for join
	 * @return {Array}  associative list
	 */
	public async associativeList(params): Promise<Array<any>> {
		// set the vars
		const field = params['field'];
		const where = params['where'];
		const field_index = params['field_index'] ? params['field_index'] : this._primary_key;
		const order = params['order'] ? params['order'] : [[field, 'ASC']];
		const includes = params['includes'];

		// call the query
		const items: any = await this.ORM.findAll({
			attributes: [field_index, field],
			where: where,
			order: order,
			include: includes
		});

		// initialize the return
		const ret = [];

		// parse the data
		if (items) {
			for (const item of items) {
				const index = ret.length;
				ret.push({ key: item[field_index], value: item[field] });
				if (includes && includes.length) {
					for (const include of includes) {
						ret[index][include.as] = item[include.as];
					}
				}
			}
		}

		// its ok
		return ret;
	}

	/**
	 * associativeListAttr
	 *
	 * Create an associative list of one attribute of model
	 *
	 * @author Murilo Spinoza de Arruda
	 * @param  {string} attribute: ATIVO_OPCOES
	 */
	public async associativeListAttr(attr: string): Promise<Array<any>> {
		const opcoes = [];

		for (const key in this[attr]) {
			opcoes.push({ key: key, value: this[attr][key] });
		}

		return opcoes;
	}
}
