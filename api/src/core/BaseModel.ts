/**
 * BaseModel
 *
 * Abstract class with all common models methods
 *
 * @author Murilo Spinoza de Arruda
 */
import * as Mongoose from 'mongoose';
import Config from '../libraries/Config';
import * as Sequelize from 'sequelize';

export abstract class BaseModel {
	/**
	 * ORM
	 *
	 * An instance of current ORM
	 */
	public ORM: Sequelize.Model<null, null>;

	public relations;

	/**
	 * _is_sql indicates if the current model is sql or not
	 */
	protected _is_sql = true;

	/**
	 * _timestamp use timestamp? (dt_modified and dt_created)
	 */
	protected _timestamp: { created?: boolean; modified?: boolean } = { created: false, modified: false };

	/**
	 * _name the name of the model
	 */
	protected _name: string;

	/**
	 * _fields a MongoDB Schema
	 * _fields a Sequelize model object
	 */
	protected _fields: {};

	/**
	 * _primary_key the name of primary key
	 */
	protected _primary_key: string;

	/**
	 * __config an instance of Config object
	 */
	private __config: Config = new Config('database');

	/**
	 * __connector the connector used on the model
	 */
	public connector: Sequelize.Sequelize;

	constructor(connector: Sequelize.Sequelize) {
		this.connector = connector;
	}

	/**
	 * _setSequelize
	 *
	 * set the sequelize ORM
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	protected _setSequelize(): void {
		// try do connect to the database
		try {
			// set the timestamp
			const options: Sequelize.DefineOptions<null> = {};
			if (this._timestamp['created'] || this._timestamp['modified']) {
				options['timestamps'] = true;
				options['createdAt'] = this._timestamp['created'] ? 'dt_created' : false;
				options['updatedAt'] = this._timestamp['modified'] ? 'dt_modified' : false;
			}

			// set the primary key
			this._primary_key = Object.keys(this._fields)
				.filter(field => {
					return this._fields[field].primaryKey;
				})
				.toString();

			// define a new model
			this.ORM = this.connector.define(this._name, this._fields as any, options);
		} catch (e) {
			// show the error
			console.error(e);
		}
	}

	/**
	 * debug
	 *
	 * set the debug
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	set debug(debug: boolean) {
		// Esta no lugar errado, isso é setado no connectors
		// this.ORM.options.sequelize.options['logging'] = debug ? console.log : debug;
		Mongoose.set('debug', debug);
	}

	/**
	 * getFields
	 *
	 * get Fields of model
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public getFields() {
		return this._fields;
	}

	/**
	 * setMongoose
	 *
	 * set the mongoose ORM
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	protected setMongoose(): void {
		// VALIDAR COMO TRABALHAR COM MONGOSE

		// get the no-sql configs
		const config = this.__config.item('no_sql');

		// a new instance of the MongoDB Schema
		const schema = new Mongoose.Schema(this._fields, { versionKey: '' });

		// try to connect
		try {
			// connection string
			const url = `mongodb://${config['host']}/${config['database']}`;

			// connect to database
			Mongoose.connect(url, config);

			// define the promise object
			(<any>Mongoose).Promise = global.Promise;
		} catch (e) {
			// show the error, if exists
			if (e.message !== 'Trying to open unclosed connection.') {
				console.error(e.message);
			}
		}

		// trying to create a new MongoDB model
		try {
			// check if the model already exists
			// if (Mongoose.model(this._name)) this.ORM = Mongoose.model(this._name);
		} catch (e) {
			// if model doesnt exists
			// if (e.name === 'MissingSchemaError') this.ORM = Mongoose.model(this._name, schema);
		}
	}
}
