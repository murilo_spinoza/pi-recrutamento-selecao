/**
 * Validator
 *
 * Extends Sequelize Validator
 *
 * @author Murilo Spinoza de Arruda
 */
import * as Sequelize from 'sequelize';
import Bootstrap from '../libraries/Bootstrap';
import CustomJWT from '../libraries/CustomJWT';
export default class Validator {
	/**
	 * customize
	 *
	 * Set the custom functions
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public static customize() {
		/**
		 * Monkey patch issue causing deprecation warning when customizing allowNull validation error
		 *
		 * See https://github.com/sequelize/sequelize/issues/1500
		 */
		Sequelize.Validator['notNull'] = item => {
			return !Sequelize.Validator.equals(item, null);
		};

		// check if JWT is Valid
		Sequelize.Validator.extend('isValidJWT', value => {
			return value && new CustomJWT(value).valid();
		});
	}
}
