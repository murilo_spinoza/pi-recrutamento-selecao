import CustomJWT from '../libraries/CustomJWT';
import { BaseController } from '../core/BaseController';
import * as express from 'express';
import Bootstrap from '../libraries/Bootstrap';
import { Usuarios } from '../models/Usuarios';
import { Permissoes } from '../models/Permissoes';
import { Rotinas } from '../models/Rotinas';
export abstract class Auth {
	/**
	 * authenticate
	 *
	 * Check if the current user is Authenticate
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @param {express.Response}     res the response for the request
	 * @param {express.Request}      req  the request object
	 * @param {express.NextFunction} net a call for the next function
	 * @return {boolean} if the user is Authenticate or not
	 */
	public static authenticate(req: express.Request, res: express.Response, next: express.NextFunction): boolean {
		// get the token
		let token = null;
		let source: string;

		if (req.headers.source) { source = req.headers.source.toString(); }

		if (req.headers.authorization) {
			const header = (req.headers.authorization as string).split(' ');
			token = header[1];
		}

		// get and set the language - in Router.ts too
		const language = req.headers.language ? (req.headers.language as string) : 'pt';
		Bootstrap.language = language;

		// create a new CustomJWT
		const jwt: CustomJWT = new CustomJWT(token);

		// create a new user model instance
		const userModel = Bootstrap.model('Usuarios') as Usuarios;

		// create a new base instance
		const base = new BaseController();

		// check if jwt is valid
		if (jwt.valid()) {
			// get the acesso and if of the sent token
			const email = jwt.item('email');
			const userID = jwt.item('usuario_id');

			// compare the header data with JWT
			if (req.headers['usuario_id'] === userID.toString()) {
				// try to Authenticate the user
				userModel
					.autentica(email, userID, jwt.token, source)
					.then(response => {
						// ok?
						if (!response) {
							console.log('\n\n==== Acesso negado ====> Não autenticado <====\n\n');
							base.showAccessDenied(req, res);
							return false;
						}

						// get the group_id
						userModel.ORM.findById(userID)
							.then((user: any) => {
								let link = req.originalUrl;
								// store the controller and method
								const parts = link.split('/');
								if (parts[0] == '') { parts.shift(); }
								link = parts[0] + (parts[1] ? '/' + parts[1] : '');

								const linkCompleto = link;

								// get or delete?
								const methods = ['GET', 'DELETE'];
								if (methods.indexOf(req.method.toUpperCase()) > -1) { link = parts[0]; }

								req.headers['grupo_id'] = user['UsuarioGrupoId'];

								// find the permission
								(Bootstrap.model('Permissoes') as Permissoes).allowed(link, user['UsuarioGrupoId'], req.method).then(allowed => {
									(Bootstrap.model('Rotinas') as Rotinas).metodo_livre(linkCompleto.split('?')[0]).then(isPublic => {
										// its ok?
										if (allowed || isPublic) { next(); }
										else {
											// show Forbidden message
											console.log('\n\n==== Acesso proibido ====\n\n');
											base.showForbidden(req, res);
											return false;
										}
									});
								});
							})
							.catch(err => {
								// show Access Denied message
								console.log('\n\n==== Acesso negado ====> Falha na busca do usuário <====', err, '\n\n');
								base.showAccessDenied(req, res);
								return false;
							});
					})
					.catch(err => {
						// show Access Denied message
						console.log('\n\n==== Acesso negado ====> Falha de autenticação <====', err, '\n\n');
						base.showAccessDenied(req, res);
						return false;
					});
			} else {
				// show Access Denied message
				console.log('\n\n==== Acesso negado ====> Sem id no header <====\n\n');
				base.showAccessDenied(req, res);
				return false;
			}
		} else {
			// show Access Denied message
			console.log('\n\n==== Acesso negado ====> JWT invalido <====\n\n');
			base.showAccessDenied(req, res);
			return false;
		}
	}
}
