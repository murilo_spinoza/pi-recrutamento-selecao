/**
 * FieldTypeEnum
 *
 * Enum for type of fields
 *
 * @author Murilo Spinoza de Arruda
 * @since 06/2018
 */
export enum FieldTypeEnum {
	STRING = 'string',
	ENUM = 'enum',
	NUMBER = 'number'
}

/**
 * OrderEnum
 *
 * Enum for order
 *
 * @author Murilo Spinoza de Arruda
 * @since 06/2018
 */
export enum OrderEnum {
	ASC = 'ASC',
	DESC = 'DESC'
}
