/**
 * STRINGS
 *
 * Contém todos os textos usados no projeto
 *
 * Atenção: Para as mensagens de validação, sempre que aparecer uma não tratada,
 * devemos ir no método _save do CustomModel e verificar qual 'validatorKey' o sequelize retorna.
 * Aí colocamos ela como chave abaixo de 'Validator' aqui e a mensagem aparecerá.
 * Funciona também para validações customizadas, vide o exemplo do isValidJWT
 *
 * @author Murilo Spinoza de Arruda
 */
export const STRINGS = {
	// PORTUGUES
	pt: {
		General: {
			none: 'Nenhum',
			wrong_password: 'Senha incorreta'
		},
		Logs: {
			login: 'O usuário "%s" logou com sucesso',
			logout: 'O usuário "%s" deslogou com sucesso',
			action: 'Ação executada pelo usuário no registro de id: %s',
			delete: 'Usuário excluiu o registro de id: %s',
			forgot_password: 'O usuário %s fez um pedido de nova senha'
		},
		Validator: {
			is_null: 'O campo "%s" é obrigatório',
			isEmail: 'O campo "%s" não é um endereço válido de e-mail',
			isValidJWT: 'O campo "%s" não contém um Token JWT válido',
			many_empty: 'Parâmetros obrigatórios não enviados',
			unique: 'O registro "%s" já existe em nosso banco de dados'
		},
		Errors: {
			not_found: '%s não encontrado',
			access_denied: 'Acesso negado',
			delete: 'Erro ao deletar o registro',
			restrictions: 'Item relacionado ao módulo "%s"',
			no_restrictions: 'Não foi possível validar os relacionamentos do registro.'
		},
		ForgotPassword: {
			subject: 'Reenvio de senha',
			hello: 'Olá, %s',
			action: 'Você solicitou a redefinição de sua senha.',
			information: 'Sua nova senha de acesso é:'
		},
		NewUsers: {
			title: 'Acesso ao sistema',
			message: 'Basta acessar o sistema com seu email'
		}
	},

	// ESPANHOL
	es: {
		General: {
			none: 'Ninguno',
			wrong_password: 'Contraseña incorrecta'
		},
		Logs: {
			login: 'El usuario "%s" logró con éxito',
			logout: 'O usuário "%s" deslogó con éxito',
			action: 'Acción ejecutada por el usuario en el registro de id: %s',
			delete: 'El usuario eliminó el registro de id: %s',
			forgot_password: 'El usuario %s ha solicitado una nueva contraseña'
		},
		Validator: {
			is_null: 'El campo "%s" es obligatorio',
			isEmail: 'El campo "%s" no es una dirección de correo electrónico válida',
			isValidJWT: 'El campo "%s" no contiene un Token JWT válido',
			many_empty: 'Parámetros obligatorios no enviados',
			unique: 'El registro "%s" ya existe en nuestra base de datos'
		},
		Errors: {
			not_found: '%s no encontrado',
			access_denied: 'Acceso denegado',
			delete: 'Error al borrar el registro',
			restrictions: 'Este registro tiene relación con el módulo "%s" no puede ser excluido',
			no_restrictions: 'No se pudo validar las relaciones del registro.'
		},
		ForgotPassword: {
			subject: 'Reenvío de contraseña',
			hello: '¡Hola, %s',
			action: 'Ha solicitado el restablecimiento de su contraseña.',
			information: 'Su nueva contraseña de acceso es:'
		},
		NewUsers: {
			title: 'Acceso al sistema',
			message: 'Sólo tienes que acceder al sistema con tu correo electrónico'
		}
	},

	// Ingles
	en: {
		General: {
			none: 'None',
			wrong_password: 'Wrong password'
		},
		Logs: {
			login: 'User "%s" successfully logged in',
			logout: 'User "%s" successfully logged out',
			action: 'Action performed by the user on the id record: %s',
			delete: 'User deleted the id record: %s',
			forgot_password: 'User %s has requested a new password'
		},
		Validator: {
			is_null: 'Field "%s" is required',
			isEmail: 'The field "%s" is not a valid email address',
			isValidJWT: 'The field "%s" does not contain a valid JWT Token',
			many_empty: 'Mandatory parameters not sent',
			unique: 'The record "%s" already exists in our database'
		},
		Errors: {
			not_found: '%s not found',
			access_denied: 'Access denied',
			delete: 'Error deleting the registry',
			restrictions: 'This record is related to the module "%s" and can not be deleted',
			no_restrictions: 'Could not validate the registry relationships.'
		},
		ForgotPassword: {
			subject: 'Password Resend',
			hello: 'Hello, %s',
			action: 'You have requested to reset your password.',
			information: 'Your new password is:'
		},
		NewUsers: {
			title: 'System access',
			message: 'Just log in with your email'
		}
	}
};
