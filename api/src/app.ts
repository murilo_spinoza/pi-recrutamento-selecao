global['app_dir'] = __dirname;

/**
 * Module dependencies
 */
import * as express from 'express';
import * as https from 'https';
import * as http from 'http';
import * as fs from 'fs';
import * as bodyParser from 'body-parser';
// import * as passport   from 'passport';
// import * as FacebookTokenStrategy from 'passport-facebook-token';
import Router from './libraries/Router';
import Bootstrap from './libraries/Bootstrap';
import Connectors from './core/Connectors';

// set the use of the Connectors
Connectors.use({ sequelize: true });

// Bootstrap the application
Bootstrap.start();

// Call the Express
const app: express.Express = express();

// Get the env
let env: string = app.settings.env;

let server_http = http.createServer(app);

// Default settings
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

// Start the routes
new Router(app);

// Start the server
server_http.listen(4000);
server_http.on('listening', () => {
	console.log('Rodando na porta %d e no modo %s', server_http.address().port, app.settings.env);
});
server_http.on('error', error => {
	console.error(error);
});

// // Configura login facebook
// passport.use(new FacebookTokenStrategy(
// 	{
// 		clientID: '2170472823194178', // Facebook app id
// 		clientSecret: '0be11590a7a06699a0c188f3f94c5817' // APP SECRET
// 	},
// 	(accessToken, refreshToken, profile, done) => { console.log(profile.id); return done(null, profile);}
// ));

// And here we go
export default app;
