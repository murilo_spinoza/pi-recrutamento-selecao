/**
 * Model para interação com a tabela: candidatos_redes_sociais
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class CandidatosRedesSociais extends CustomModel {
	protected _name = 'candidatos_redes_sociais';

	protected _fields: IAtributos = {
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			primaryKey: true,
			allowNull: false,
			field: 'candidato_id'
		},
		RedeSocialId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'rede_social_id'
		},
		Link: {
			type: Sequelize.STRING(200),
			allowNull: false,
			field: 'str_link'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		},
		{
			type: 'belongsTo',
			model: 'RedesSociais',
			options: {
				foreignKey: 'RedeSocialId',
				as: 'RedeSocial'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
