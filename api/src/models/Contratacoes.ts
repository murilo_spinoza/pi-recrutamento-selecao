/**
 * Model para interação com a tabela: contratacoes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import { CustomModel, IAtributos } from '../core/CustomModel';
import * as Sequelize from 'sequelize';

export class Contratacoes extends CustomModel {
	protected _name = 'contratacoes';

	protected _fields: IAtributos = {
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'contratacao_id'
		},
		QuadroVagaId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: false,
			primaryKey: false,
			allowNull: false,
			field: 'quadro_vaga_id'
		},
		CargoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: false,
			primaryKey: false,
			allowNull: false,
			field: 'cargo_id'
		},
		UsuarioSolicitanteId: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			field: 'usuario_solicitante_id'
		},
		UsuarioAvaliadorId: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			field: 'usuario_avaliador_id'
		},
		Descricao: {
			type: Sequelize.STRING(500),
			allowNull: false,
			field: 'txt_descricao'
		},
		Motivo: {
			type: Sequelize.STRING(255),
			allowNull: true,
			field: 'str_motivo'
		},
		Rejeicao: {
			type: Sequelize.STRING(255),
			allowNull: true,
			field: 'str_rejeicao'
		},
		Salario: {
			type: Sequelize.DECIMAL(8, 2),
			allowNull: false,
			field: 'dec_salario'
		},
		Contratacoes: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'num_contratacoes'
		},
		PontosTriagem: {
			type: Sequelize.DECIMAL(8, 2),
			allowNull: false,
			field: 'dec_pts_triagem'
		},
		Pcd: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			defaultValue: 'N',
			allowNull: false,
			field: 'flg_pcd'
		},
		Status: {
			type: Sequelize.ENUM,
			values: ['A', 'R', 'P', 'E', 'F'],
			defaultValue: 'P',
			allowNull: false,
			field: 'flg_status'
		},
		Solicitacao: {
			type: Sequelize.DATE,
			allowNull: true,
			defaultValue: null,
			field: 'dt_solicitacao'
		},
		Avaliacao: {
			type: Sequelize.DATE,
			allowNull: true,
			defaultValue: null,
			field: 'dt_avaliacao'
		},
		Etapa: {
			type: Sequelize.ENUM,
			values: ['triagem', 'testes', 'entrevistas', 'aprovados'],
			defaultValue: null,
			allowNull: true,
			field: 'alias_etapa'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'QuadroVagas',
			options: {
				foreignKey: 'QuadroVagaId',
				as: 'QuadroVaga'
			}
		},
		{
			type: 'belongsTo',
			model: 'Cargos',
			options: {
				foreignKey: 'CargoId',
				as: 'Cargo'
			}
		},
		{
			type: 'belongsTo',
			model: 'Usuarios',
			options: {
				foreignKey: 'UsuarioSolicitanteId',
				as: 'UsuarioSolicitante'
			}
		},
		{
			type: 'belongsTo',
			model: 'Usuarios',
			options: {
				foreignKey: 'UsuarioAvaliadorId',
				as: 'UsuarioAvaliador'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesEscolaridades',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesEscolaridades'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesRequisitos',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesRequisitos'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesTestes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesTestes'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatos',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatos'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosAprovados',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosAprovados'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosEntrevistas',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosEntrevistas'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosPontos',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosPontos'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosTestes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosTestes'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosTriagem',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosTriagem'
			}
		}
	];

	public readonly STATUS_OPCOES = {
		R: 'Rejeitada',
		P: 'Análise Pendente',
		E: 'Em andamento',
		F: 'Finalizado'
	};

	public readonly PCD_OPCOES = {
		S: 'Sim',
		N: 'Não'
	};

	public readonly ETAPA_OPCOES = {
		triagem: 'Triagem',
		testes: 'Testes',
		entrevistas: 'Entrevistas',
		aprovados: 'Aprovados'
	};

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
