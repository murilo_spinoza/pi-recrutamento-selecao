/**
 * Model para interação com a tabela: contratacoes_candidatos_pontos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export const REQUISITOS_PESOS = {
	O: 2,
	I: 1.5,
	D: 1,
	P: 0.5
};

export class ContratacoesCandidatosPontos extends CustomModel {
	protected _name = 'contratacoes_candidatos_pontos';

	protected _timestamp = { created: true };

	protected _fields: IAtributos = {
		ContratacaoCandidatoPontoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			field: 'cc_ponto_id'
		},
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'contratacao_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'candidato_id'
		},
		UsuarioId: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			field: 'usuario_id'
		},
		Pontos: {
			type: Sequelize.NUMERIC(10, 2),
			allowNull: true,
			field: 'dec_pontos'
		},
		Motivo: {
			type: Sequelize.STRING(100),
			allowNull: false,
			field: 'str_motivo'
		},
		Origem: {
			type: Sequelize.ENUM,
			values: ['S', 'F'],
			allowNull: false,
			field: 'flg_origem'
		},
		Created: {
			type: Sequelize.DATE,
			field: 'dt_created'
		}
	};

	public readonly STATUS_OPCOES = {
		F: 'Funcionário',
		S: 'Sistema'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Contratacoes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		},
		{
			type: 'belongsTo',
			model: 'Usuarios',
			options: {
				foreignKey: 'UsuarioId',
				as: 'Usuario'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}

	public calculaPontos(tipoRequisito: 'O' | 'I' | 'D' | 'P', nivelMinimo: number, nivelCandidato: number) {
		// Identifica o peso do requisito
		const peso = REQUISITOS_PESOS[tipoRequisito];

		// Identifica a dificuldade pelo nível exigido
		const dificuldade = nivelMinimo / 5;

		// Define quanto vale cada nível
		const ponto = peso * dificuldade;

		// Calcula a pontuação do candidato de acordo com o nível e o valor do ponto
		return nivelCandidato * ponto;
	}

	public calculaQuantiaPotencial(qtdeContratacoes: number) {
		let quantiaPotencial = 0;

		const marcadores = {
			5: 20,
			10: 25,
			15: 30,
			20: 40,
			25: 50
		};

		for (const marcador in marcadores) {
			if (qtdeContratacoes <= +marcador) {
				quantiaPotencial = marcadores[marcador];
				break;
			}
		}

		if (!quantiaPotencial) {
			quantiaPotencial = qtdeContratacoes + qtdeContratacoes / 2;
		}

		return quantiaPotencial;
	}
}
