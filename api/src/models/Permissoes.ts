/**
 * Model para interação com a tabela: permissoes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';
import Bootstrap from '../libraries/Bootstrap';

export class Permissoes extends CustomModel {
	protected _name = 'permissoes';

	protected _fields: IAtributos = {
		UsuarioGrupoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			primaryKey: true,
			field: 'usuario_grupo_id'
		},

		RotinaId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			primaryKey: true,
			field: 'rotina_id'
		},

		Consulta: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'N',
			primaryKey: false,
			field: 'flg_consulta'
		},

		Edita: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'N',
			primaryKey: false,
			field: 'flg_edita'
		},

		Insere: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'N',
			primaryKey: false,
			field: 'flg_insere'
		},

		Exclui: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'N',
			primaryKey: false,
			field: 'flg_exclui'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'UsuariosGrupos',
			options: {
				foreignKey: 'UsuarioGrupoId',
				as: 'UsuarioGrupo'
			}
		},
		{
			type: 'belongsTo',
			model: 'Rotinas',
			options: {
				foreignKey: 'RotinaId',
				as: 'Rotina'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}

	/**
	 * allowed
	 *
	 * Check if user group is allowed
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async allowed(link, group_id, method) {
		// get the type permission
		const types = { GET: { Consulta: 'S' }, POST: { Insere: 'S' }, PUT: { Edita: 'S' }, DELETE: { Exclui: 'S' } };

		// Set the where
		const where = types[method.toUpperCase()] || { Consulta: 'S' };
		where['UsuarioGrupoId'] = group_id;

		// call the query
		const allowed = await this.ORM.findOne({
			attributes: ['RotinaId'],
			where: where,
			include: [
				{
					model: Bootstrap.model('Rotinas').ORM,
					as: 'Rotina',
					required: true,
					where: {
						Link: link
					}
				}
			]
		});

		// return
		return <boolean>allowed;
	}

	/**
	 * salvaPermissoesGrupo
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async salvaPermissoesGrupo(usuarioGrupoId: number, permissoes): Promise<any> {
		// Try save
		try {
			if (permissoes) {
				// Loop in permissions
				for (const permissaoId in permissoes) {
					const permissao = permissoes[permissaoId];

					// Dictionary
					const acoes = ['Consulta', 'Edita', 'Insere', 'Exclui'];
					for (const acao of acoes) {
						permissao[acao] = permissao[acao] ? 'S' : 'N';
					}

					// Save
					permissao['UsuarioGrupoId'] = usuarioGrupoId;
					if (permissao['RotinaId']) {
						await this.ORM.upsert(permissao);
					}
				}
			}
			return 'Ok';
		} catch (e) {
			throw e;
		}
	}
}
