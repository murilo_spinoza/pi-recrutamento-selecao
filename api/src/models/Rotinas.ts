/**
 * Model para interação com a tabela: rotinas
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';
import Bootstrap from '../libraries/Bootstrap';

export class Rotinas extends CustomModel {
	protected _name = 'rotinas';

	protected _fields: IAtributos = {
		RotinaId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'rotina_id'
		},

		RotinaClassificacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			primaryKey: false,
			field: 'rotina_classificacao_id'
		},

		Rotina: {
			type: Sequelize.STRING,
			allowNull: false,
			primaryKey: false,
			field: 'str_rotina'
		},

		Link: {
			type: Sequelize.STRING,
			allowNull: true,
			defaultValue: null,
			primaryKey: false,
			field: 'str_link'
		},

		ApareceMenu: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'S',
			primaryKey: false,
			field: 'flg_aparece_menu'
		},

		Publico: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'N',
			primaryKey: false,
			field: 'flg_publico'
		},

		Ordem: {
			type: Sequelize.INTEGER(11),
			allowNull: false,
			defaultValue: '999',
			primaryKey: false,
			field: 'num_ordem'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'RotinasClassificacoes',
			options: {
				foreignKey: 'RotinaClassificacaoId',
				as: 'RotinaClassificacao'
			}
		},
		{
			type: 'hasMany',
			model: 'Permissoes',
			options: {
				foreignKey: 'RotinaId',
				as: 'Permissao'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}

	/**
	 * lista_rotinas
	 *
	 * Monta a lista de rotinas disponíveis no menu
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async lista_rotinas(usuario_grupo_id, filter?: boolean) {
		// Set the where
		const where: any = {};

		if (filter === true) {
			where['ApareceMenu'] = 'S';
		}

		// Return the query
		return await this.ORM.findAll({
			attributes: ['RotinaId', 'RotinaClassificacaoId', 'Rotina', 'Link', 'ApareceMenu'],
			where: <any>where,
			order: [['RotinaClassificacao', 'Ordem', 'ASC'], ['Ordem', 'ASC']],
			include: [
				{
					model: Bootstrap.model('Permissoes').ORM,
					as: 'Permissao',
					required: true,
					where: {
						[Sequelize.Op.or]: [{ Consulta: 'S' }, { Insere: 'S' }],
						UsuarioGrupoId: usuario_grupo_id
					}
				},
				{
					model: Bootstrap.model('RotinasClassificacoes').ORM,
					as: 'RotinaClassificacao',
					required: true,
					attributes: ['Classificacao', 'Icone', 'RotinaClassificacaoId']
				}
			]
		});
	}

	/**
	 * metodo_livre
	 *
	 * Indica se o método pode ser acessado por qualquer um logado
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async metodo_livre(link) {
		// Monta o where
		const where: any = { Link: link, Publico: 'S' };

		// Busca a rotina
		const publico = await this.ORM.findOne({
			attributes: ['RotinaId'],
			where: <any>where
		});

		// Devolve indicando se é livre ou não
		return <boolean>publico;
	}
}
