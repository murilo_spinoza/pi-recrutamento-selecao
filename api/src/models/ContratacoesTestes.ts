/**
 * Model para interação com a tabela: contratacoes_testes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class ContratacoesTestes extends CustomModel {
	protected _name = 'contratacoes_testes';

	protected _fields: IAtributos = {
		ContratacaoTesteId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			field: 'contratacao_teste_id'
		},
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'contratacao_id'
		},
		TesteId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'teste_id'
		},
		MinimoPontos: {
			type: Sequelize.INTEGER(4),
			allowNull: false,
			field: 'num_min_pontos'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Contratacoes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'belongsTo',
			model: 'Testes',
			options: {
				foreignKey: 'TesteId',
				as: 'Teste'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
