/**
 * Model para interação com a tabela: contratacoes_candidatos_aprovados
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class ContratacoesCandidatosAprovados extends CustomModel {
	protected _name = 'contratacoes_candidatos_aprovados';

	protected _fields: IAtributos = {
		ContratacaoCandidatoAprovadoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			field: 'cc_aprovado_id'
		},
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'contratacao_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'candidato_id'
		},
		Contato: {
			type: Sequelize.DATE,
			field: 'dt_contato'
		},
		InicioEmprego: {
			type: Sequelize.DATE,
			field: 'dt_inicio_emprego'
		}
	};

	public readonly STATUS_OPCOES = {
		A: 'Aprovado',
		R: 'Reprovado'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Contratacoes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
