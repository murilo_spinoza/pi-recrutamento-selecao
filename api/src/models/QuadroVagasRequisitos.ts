/**
 * Model para interação com a tabela: quadro_vagas_requisitos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class QuadroVagasRequisitos extends CustomModel {
	protected _name = 'quadro_vagas_requisitos';

	protected _fields: IAtributos = {
		QuadroVagaRequisitoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			field: 'quadro_vaga_requisito_id'
		},
		QuadroVagaId: {
			type: Sequelize.INTEGER(10),
			primaryKey: false,
			allowNull: false,
			field: 'quadro_vaga_id'
		},
		HabilidadeId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'habilidade_id'
		},
		Nivel: {
			type: Sequelize.INTEGER(3),
			allowNull: false,
			field: 'num_nivel'
		},
		Tipo: {
			type: Sequelize.ENUM,
			values: ['O', 'I', 'D', 'P'],
			allowNull: false,
			field: 'flg_tipo'
		}
	};

	public readonly TIPO_OPCOES = {
		O: 'Obrigatório',
		I: 'Importante',
		D: 'Diferencial',
		P: 'Opcional'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'QuadroVagas',
			options: {
				foreignKey: 'QuadroVagaId',
				as: 'QuadroVaga'
			}
		},
		{
			type: 'belongsTo',
			model: 'Habilidades',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'Habilidade'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
