/**
 * Model para interação com a tabela: cursos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class Cursos extends CustomModel {
	protected _name = 'cursos';

	protected _fields: IAtributos = {
		CursoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'curso_id'
		},
		GrauEscolaridadeId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: false,
			allowNull: false,
			primaryKey: false,
			field: 'grau_escolaridade_id'
		},
		Curso: {
			type: Sequelize.STRING(50),
			allowNull: false,
			primaryKey: false,
			field: 'str_curso'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'GrausEscolaridades',
			options: {
				foreignKey: 'GrauEscolaridadeId',
				as: 'GrauEscolaridade'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
