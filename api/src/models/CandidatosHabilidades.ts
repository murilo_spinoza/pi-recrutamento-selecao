/**
 * Model para interação com a tabela: candidatos_habilidades
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class CandidatosHabilidades extends CustomModel {
	protected _name = 'candidatos_habilidades';

	protected _fields: IAtributos = {
		CandidatoHabilidadeId: {
			type: Sequelize.INTEGER(10),
			primaryKey: true,
			autoIncrement: true,
			allowNull: false,
			field: 'candidato_habilidade_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			primaryKey: false,
			allowNull: false,
			field: 'candidato_id'
		},
		HabilidadeId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'habilidade_id'
		},
		Nivel: {
			type: Sequelize.INTEGER(3),
			allowNull: false,
			field: 'num_nivel'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		},
		{
			type: 'belongsTo',
			model: 'Habilidades',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'Habilidade'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
