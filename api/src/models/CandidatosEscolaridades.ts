/**
 * Model para interação com a tabela: candidatos_escolaridades
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class CandidatosEscolaridades extends CustomModel {
	protected _name = 'candidatos_escolaridades';

	protected _fields: IAtributos = {
		CandidatoEscolaridadeId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'candidato_escolaridade_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'candidato_id'
		},
		CursoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'curso_id'
		},
		Instituicao: {
			type: Sequelize.STRING(100),
			allowNull: false,
			primaryKey: false,
			field: 'str_instituicao'
		},
		Periodo: {
			type: Sequelize.ENUM,
			values: ['M', 'T', 'I', 'N'],
			allowNull: false,
			field: 'flg_periodo'
		},
		Inicio: {
			type: Sequelize.DATEONLY,
			allowNull: false,
			field: 'dt_inicio'
		},
		Termino: {
			type: Sequelize.DATEONLY,
			allowNull: true,
			field: 'dt_termino'
		}
	};

	public readonly PERIODO_OPCOES = {
		M: 'Manhã',
		T: 'Tarde',
		I: 'Integral',
		N: 'Noite'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		},
		{
			type: 'belongsTo',
			model: 'Cursos',
			options: {
				foreignKey: 'CursoId',
				as: 'Curso'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
