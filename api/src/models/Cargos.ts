/**
 * Model para interação com a tabela: cargos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import { CustomModel, IAtributos } from '../core/CustomModel';
import * as Sequelize from 'sequelize';

export class Cargos extends CustomModel {
	protected _name = 'cargos';

	protected _fields: IAtributos = {
		CargoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'cargo_id'
		},
		Cargo: {
			type: Sequelize.STRING(50),
			allowNull: false,
			primaryKey: false,
			field: 'str_cargo',
			unique: true
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'QuadroVagas',
			options: {
				foreignKey: 'CargoId',
				as: 'QuadroVagas'
			}
		},
		{
			type: 'hasMany',
			model: 'Contratacoes',
			options: {
				foreignKey: 'CargoId',
				as: 'Contratacoes'
			}
		},
		{
			type: 'hasMany',
			model: 'CandidatosExperiencias',
			options: {
				foreignKey: 'CargoId',
				as: 'Experiencias'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
