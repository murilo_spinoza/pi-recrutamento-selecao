/**
 * Model para interação com a tabela: contratacoes_escolaridades
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class ContratacoesEscolaridades extends CustomModel {
	protected _name = 'contratacoes_escolaridades';

	protected _fields: IAtributos = {
		ContratacaoEscolaridadeId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'contratacao_escolaridade_id'
		},
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'contratacao_id'
		},
		CursoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'curso_id'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Contratacoes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'belongsTo',
			model: 'Cursos',
			options: {
				foreignKey: 'CursoId',
				as: 'Curso'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
