/**
 * Model para interação com a tabela: usuarios
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as moment from 'moment';
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';
import CustomJWT from '../libraries/CustomJWT';
import crypto = require('crypto');

export class Usuarios extends CustomModel {
	protected _name = 'usuarios';

	protected _timestamp = { created: true, modified: true };

	protected _fields: IAtributos = {
		UsuarioId: {
			type: Sequelize.INTEGER(10),
			primaryKey: true,
			autoIncrement: true,
			field: 'usuario_id'
		},

		UsuarioGrupoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			primaryKey: false,
			field: 'usuario_grupo_id'
		},

		CargoId: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			primaryKey: false,
			field: 'cargo_id'
		},

		Nome: {
			type: Sequelize.STRING(80),
			allowNull: false,
			field: 'str_nome'
		},

		Telefone: {
			type: Sequelize.STRING(20),
			allowNull: false,
			field: 'str_telefone'
		},

		Email: {
			type: Sequelize.STRING(70),
			allowNull: false,
			unique: true,
			field: 'str_email',
			validate: {
				isEmail: true
			}
		},

		Senha: {
			type: Sequelize.CHAR(32),
			allowNull: false,
			field: 'hash_senha',
			notLog: true,
			validate: {
				isMD5: true
			}
		},

		Token: {
			type: Sequelize.TEXT,
			allowNull: true,
			field: 'txt_autentica',
			notLog: true,
			validate: {
				isValidJWT: true
			}
		},

		TokenApp: {
			type: Sequelize.TEXT,
			allowNull: true,
			field: 'txt_autentica_app',
			notLog: true,
			validate: {
				isValidJWT: true
			}
		},

		NovaSenha: {
			type: Sequelize.CHAR(32),
			allowNull: true,
			field: 'hash_nova_senha',
			notLog: true,
			validate: {
				isMD5: true
			}
		},

		ValidadeNovaSenha: {
			type: Sequelize.DATE,
			allowNull: true,
			notLog: true,
			field: 'dt_validade_nova_senha'
		},

		Ativo: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			field: 'flg_ativo'
		},

		UltimoAcesso: {
			type: Sequelize.DATE,
			allowNull: true,
			notLog: true,
			field: 'dt_ultimo_acesso'
		},

		Modified: {
			type: Sequelize.DATE,
			notLog: true,
			field: 'dt_modified'
		},

		Created: {
			type: Sequelize.DATE,
			notLog: true,
			field: 'dt_created'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'UsuariosGrupos',
			options: {
				foreignKey: 'UsuarioGrupoId',
				as: 'Grupo'
			}
		},
		{
			type: 'belongsTo',
			model: 'Cargos',
			options: {
				foreignKey: 'CargoId',
				as: 'Cargo'
			}
		},
		{
			type: 'hasMany',
			model: 'Logs',
			options: {
				foreignKey: 'UsuarioId',
				as: 'Log'
			}
		}
	];

	public readonly ATIVO_OPCOES = {
		S: 'Sim',
		N: 'Não'
	};

	constructor(connector: Sequelize.Sequelize) {
		super(connector);
		this._setSequelize();
	}

	/**
	 * login
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param  {string}
	 * @param  {string}
	 * @return {Promise<Object>} the usuario data
	 */
	public async login(email: string, senha: string, source: string): Promise<Object> {
		// Monta o where para o email recebido
		const where: any = {
			Email: email,
			Ativo: 'S'
		};

		// Inclui o OR da senha atual ou da nova e sua expiração
		where[Sequelize.Op.or] = {
			Senha: senha,
			[Sequelize.Op.and]: {
				NovaSenha: senha,
				ValidadeNovaSenha: {
					[Sequelize.Op.gte]: moment().format()
				}
			}
		};

		// Busca as informações sobre o usuário
		const usuario: any = await this.ORM.findOne({
			attributes: ['UsuarioId', 'Nome', 'Email', 'Token', 'UsuarioGrupoId'],
			where: where
		});

		// Verifica se o usuário foi encontrado
		if (!usuario) {
			throw 'Usuário ou senha inválidos';
		}

		// Controi um novo JWT
		const token = new CustomJWT().create({ email: usuario.Email, usuario_id: usuario.UsuarioId, time: new Date().getTime() });

		// Monta o update do usuário que está se logando
		const update: any = {
			UltimoAcesso: new Date(),
			[source == 'app' ? 'TokenApp' : 'Token']: token
		};

		// Atualiza o token do usuário
		try {
			await usuario.updateAttributes(update);
		} catch (err) {
			console.log(err);
			throw 'Não foi possível atualizar o token';
		}

		return { UsuarioId: usuario.UsuarioId, Nome: usuario.Nome, UsuarioGrupoId: usuario.UsuarioGrupoId, Token: token };
	}

	/**
	 * autentica
	 *
	 * Verifica se as informações recebidas conferem
	 *
	 * @public
	 * @async
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {string}
	 * @param {number}
	 * @param {token}
	 * @return {boolean}
	 */
	public async autentica(email: string, usuarioId: string, token: string, source: string): Promise<boolean> {
		// Busca as informações do usuário pelo id
		const usuario: any = await this.ORM.findById(usuarioId);

		// Verifica se o usuário foi encontrado
		if (!usuario) {
			throw 'Usuário não encontrado';
		}

		// Verifica se o email não confere
		if (usuario.Email != email) {
			throw 'E-mail informado inválido';
		}

		// Verifica se o token é inválido de acordo com a origem
		if ((source == 'app' && usuario.TokenApp != token) || (source != 'app' && usuario.Token != token)) {
			throw 'Token inválido';
		}

		// Indica que foi autenticado com sucesso
		return true;
	}

	/**
	 * nova_senha
	 *
	 * Gera uma nova senha para o usuário
	 *
	 * @public
	 * @author Murilo Spinoza de Arruda
	 * @since 03/2019
	 * @param {string} email
	 * @return {boolean}
	 */
	public async nova_senha(email: string): Promise<Object> {
		try {
			// Encontra o usuário pelo email
			const usuario: any = await this.ORM.findOne({
				where: { Email: email } as any,
				attributes: ['Nome', 'UsuarioId']
			});

			// Verifica se o usuário foi encontrado
			if (!usuario) {
				throw 'E-mail não encontrado';
			}

			// Gera uma nova senha como MD5(timestamp)
			let nova_senha = crypto
				.createHash('md5')
				.update(new Date().toString())
				.digest('hex');

			// Desses 32 caracteres, pega 5
			nova_senha = nova_senha.substring(0, 5);

			// Atualiza o registro do usuário com a nova senha
			await usuario.updateAttributes({
				NovaSenha: crypto
					.createHash('md5')
					.update(nova_senha)
					.digest('hex'),
				ValidadeNovaSenha: moment().add(2, 'hours') // 2 horas de validade
			});

			// Retorna os dados
			return { Nome: usuario['Nome'], NovaSenha: nova_senha };
		} catch (error) {
			throw error;
		}
	}
}
