/**
 * Model para interação com a tabela: graus_escolaridades
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class GrausEscolaridades extends CustomModel {
	protected _name = 'graus_escolaridades';

	protected _fields: IAtributos = {
		GrauEscolaridadeId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'grau_escolaridade_id'
		},
		GrauEscolaridade: {
			type: Sequelize.STRING(50),
			allowNull: false,
			primaryKey: false,
			field: 'str_grau_escolaridade',
			unique: true
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'Cursos',
			depends: true,
			options: {
				foreignKey: 'GrauEscolaridadeId',
				as: 'Cursos'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
