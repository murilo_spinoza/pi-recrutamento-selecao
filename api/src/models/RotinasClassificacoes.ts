/**
 * Model para interação com a tabela: rotinas_classificacoes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import { CustomModel, IAtributos } from '../core/CustomModel';
import * as Sequelize from 'sequelize';

export class RotinasClassificacoes extends CustomModel {
	protected _name = 'rotinas_classificacoes';

	protected _fields: IAtributos = {
		RotinaClassificacaoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'rotina_classificacao_id'
		},
		Classificacao: {
			type: Sequelize.STRING,
			allowNull: false,
			primaryKey: false,
			field: 'str_classificacao'
		},
		Ordem: {
			type: Sequelize.INTEGER(3),
			allowNull: false,
			defaultValue: '999',
			primaryKey: false,
			field: 'num_ordem'
		},
		Icone: {
			type: Sequelize.STRING,
			allowNull: false,
			defaultValue: 'nb-gear',
			primaryKey: false,
			field: 'str_icone'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'Rotinas',
			options: {
				foreignKey: 'RotinaClassificacaoId',
				as: 'Rotinas'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
