/**
 * Model para interação com a tabela: usuarios_grupos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class UsuariosGrupos extends CustomModel {
	protected _name = 'usuarios_grupos';

	protected _fields: IAtributos = {
		UsuarioGrupoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'usuario_grupo_id'
		},

		Grupo: {
			type: Sequelize.STRING,
			allowNull: false,
			primaryKey: false,
			field: 'str_usuario_grupo'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'Usuarios',
			label: 'Usuários',
			depends: true,
			options: {
				foreignKey: 'UsuarioGrupoId',
				as: 'Usuarios'
			}
		},
		{
			type: 'hasMany',
			model: 'Permissoes',
			label: 'Permissões',
			depends: false,
			options: {
				foreignKey: 'UsuarioGrupoId',
				as: 'Permissoes'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
