/**
 * Model para interação com a tabela: logs
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class Logs extends CustomModel {
	protected _name = 'logs';

	protected _timestamp = { created: true };

	protected _fields: IAtributos = {
		LogId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'log_id'
		},

		UsuarioId: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			defaultValue: null,
			field: 'usuario_id'
		},

		Mensagem: {
			type: Sequelize.STRING,
			allowNull: false,
			field: 'str_mensagem'
		},

		Tipo: {
			type: Sequelize.ENUM,
			values: ['CR', 'AT', 'EX', 'AC'],
			allowNull: false,
			field: 'flg_tipo'
		},

		Link: {
			type: Sequelize.STRING,
			allowNull: true,
			defaultValue: null,
			field: 'str_link'
		},

		Alteracoes: {
			type: Sequelize.TEXT,
			allowNull: true,
			defaultValue: null,
			field: 'txt_alteracoes'
		},

		Created: {
			type: Sequelize.DATE,
			field: 'dt_created'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Usuarios',
			label: 'Usuários',
			options: {
				foreignKey: 'UsuarioId',
				as: 'Usuario'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}

	/**
	 * registra_log
	 *
	 * Salva um novo log
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async registra_log(
		mensagem: string,
		tipo: string,
		link: string = null,
		usuarioId: number = null,
		alteracoes: string = null
	): Promise<Object> {
		try {
			// Monta os tipos de erros a parir do método da requisição
			const tipos = { GET: 'AC', POST: 'CR', PUT: 'AT', DELETE: 'EX' };

			// Monta o novo registro
			const data: any = {
				Mensagem: mensagem,
				Tipo: tipos[tipo.toUpperCase()] || 'AC',
				Link: link,
				Alteracoes: alteracoes,
				UsuarioId: usuarioId
			};

			// Salva o log
			const log = await this.ORM.create(data);

			// Devolve o log criado
			return log;
		} catch (err) {
			throw new Error(err);
		}
	}
}
