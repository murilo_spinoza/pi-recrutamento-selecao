/**
 * Model para interação com a tabela: testes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class Testes extends CustomModel {
	protected _name = 'testes';

	protected _fields: IAtributos = {
		TesteId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'teste_id'
		},
		Teste: {
			type: Sequelize.STRING(50),
			allowNull: false,
			primaryKey: false,
			field: 'str_teste',
			unique: true
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'ContratacoesTestes',
			depends: true,
			label: 'Testes de Contratação',
			options: {
				foreignKey: 'TesteId',
				as: 'ContratacoesTestes'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesTestes',
			depends: false,
			options: {
				foreignKey: 'TesteId',
				as: 'ContratacaoTeste'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
