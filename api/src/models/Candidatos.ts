/**
 * Model para interação com a tabela: candidatos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as moment from 'moment';
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';
import Bootstrap from '../libraries/Bootstrap';
import CustomJWT from '../libraries/CustomJWT';
import crypto = require('crypto');

export interface ICandidatoPontos {
	ContratacaoId?: number;
	CandidatoId?: number;
	UsuarioId?: number;
	Pontos?: number;
	Motivo?: string;
	Origem?: 'S' | 'F';
	Created: string;
}

export interface ICandidatoTriagem {
	CandidatoId?: number;
	Nome?: string;
	Email?: string;
	Endereco?: string;
	Idade?: number;
	Pcd?: string;
	Habilidades?: {
		HabilidadeId: number;
		Habilidade: string;
		Nivel: number;
	}[];
	Pontos?: number;
	CandidatoPontos?: ICandidatoPontos[];
}

export class Candidatos extends CustomModel {
	protected _name = 'candidatos';

	protected _timestamp = { created: true, modified: true };

	protected _fields: IAtributos = {
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			primaryKey: true,
			autoIncrement: true,
			field: 'candidato_id'
		},

		Nome: {
			type: Sequelize.STRING(40),
			allowNull: false,
			field: 'str_nome'
		},

		Cpf: {
			type: Sequelize.STRING(15),
			allowNull: false,
			field: 'str_cpf'
		},

		Rg: {
			type: Sequelize.STRING(9),
			allowNull: false,
			field: 'str_rg'
		},

		Telefone: {
			type: Sequelize.STRING(12),
			allowNull: false,
			field: 'str_telefone'
		},

		Email: {
			type: Sequelize.STRING(70),
			allowNull: false,
			unique: true,
			field: 'str_email',
			validate: {
				isEmail: true
			}
		},

		Endereco: {
			type: Sequelize.STRING(255),
			allowNull: false,
			field: 'str_endereco'
		},

		Sexo: {
			type: Sequelize.ENUM,
			values: ['F', 'M'],
			allowNull: false,
			field: 'flg_sexo'
		},

		Pcd: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			defaultValue: 'N',
			allowNull: false,
			field: 'flg_pcd'
		},

		Deficiencia: {
			type: Sequelize.STRING(255),
			field: 'str_deficiencia'
		},

		Nascimento: {
			type: Sequelize.DATEONLY,
			allowNull: false,
			field: 'dt_nascimento'
		},

		Observacoes: {
			type: Sequelize.TEXT,
			allowNull: true,
			field: 'txt_observacoes'
		},

		Complemento: {
			type: Sequelize.TEXT,
			allowNull: true,
			field: 'txt_complemento'
		},

		Senha: {
			type: Sequelize.CHAR(32),
			field: 'hash_senha',
			notLog: true,
			validate: {
				isMD5: true
			}
		},

		Token: {
			type: Sequelize.TEXT,
			allowNull: true,
			field: 'txt_autentica',
			notLog: true,
			validate: {
				isValidJWT: true
			}
		},

		TokenApp: {
			type: Sequelize.TEXT,
			allowNull: true,
			field: 'txt_autentica_app',
			notLog: true,
			validate: {
				isValidJWT: true
			}
		},

		NovaSenha: {
			type: Sequelize.CHAR(32),
			allowNull: true,
			field: 'hash_nova_senha',
			notLog: true,
			validate: {
				isMD5: true
			}
		},

		ValidadeNovaSenha: {
			type: Sequelize.DATE,
			allowNull: true,
			notLog: true,
			field: 'dt_validade_nova_senha'
		},

		Validado: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'N',
			field: 'flg_validado'
		},

		Ativo: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			allowNull: false,
			defaultValue: 'S',
			field: 'flg_ativo'
		},

		UltimoAcesso: {
			type: Sequelize.DATE,
			allowNull: true,
			notLog: true,
			field: 'dt_ultimo_acesso'
		},

		Modified: {
			type: Sequelize.DATE,
			notLog: true,
			field: 'dt_modified'
		},

		Created: {
			type: Sequelize.DATE,
			notLog: true,
			field: 'dt_created'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			depends: true,
			label: 'Experiências',
			model: 'CandidatosExperiencias',
			options: {
				foreignKey: 'CandidatoId',
				as: 'CandidatosExperiencias'
			}
		},
		{
			type: 'hasMany',
			depends: true,
			label: 'Formação acadêmica',
			model: 'CandidatosEscolaridades',
			options: {
				foreignKey: 'CandidatoId',
				as: 'CandidatosEscolaridades'
			}
		},
		{
			type: 'hasMany',
			depends: true,
			label: 'Habilidades',
			model: 'CandidatosHabilidades',
			options: {
				foreignKey: 'CandidatoId',
				as: 'CandidatosHabilidades'
			}
		},
		{
			type: 'hasMany',
			depends: true,
			label: 'Redes Sociais',
			model: 'CandidatosRedesSociais',
			options: {
				foreignKey: 'CandidatoId',
				as: 'CandidatosRedesSociais'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatos',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatos'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosAprovados',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosAprovados'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosEntrevistas',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosEntrevistas'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosPontos',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosPontos'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosTestes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosTestes'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosTriagem',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'ContratacoesCandidatosTriagem'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesCandidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesCandidatosAprovados',
			options: {
				foreignKey: 'CandidatoId',
				as: 'ContratacaoAprovados'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesCandidatosEntrevistas',
			options: {
				foreignKey: 'CandidatoId',
				as: 'ContratacaoEntrevistas'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesCandidatosPontos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'ContratacaoPontos'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesCandidatosTestes',
			options: {
				foreignKey: 'CandidatoId',
				as: 'ContratacaoTestes'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesCandidatosTriagem',
			options: {
				foreignKey: 'CandidatoId',
				as: 'ContratacaoTriagem'
			}
		}
	];

	public readonly ATIVO_OPCOES = {
		S: 'Sim',
		N: 'Não'
	};

	public readonly SEXO_OPCOES = {
		F: 'Feminino',
		M: 'Masculino'
	};

	public readonly PCD_OPCOES = {
		S: 'Sim',
		N: 'Não'
	};

	public readonly VALIDADO_OPCOES = {
		S: 'Sim',
		N: 'Não'
	};

	constructor(connector: Sequelize.Sequelize) {
		super(connector);
		this._setSequelize();
	}

	/**
	 * login
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async login(email: string, senha: string, source: string): Promise<Object> {
		// Monta o where para o email recebido
		const where: any = {
			Email: email,
			Ativo: 'S'
		};

		// Inclui o OR da senha atual ou da nova e sua expiração
		where[Sequelize.Op.or] = {
			Senha: senha,
			[Sequelize.Op.and]: {
				NovaSenha: senha,
				ValidadeNovaSenha: {
					[Sequelize.Op.gte]: moment().format()
				}
			}
		};

		// Busca as informações sobre o usuário
		const usuario: any = await this.ORM.findOne({
			attributes: ['CandidatoId', 'Nome', 'Email', 'Token'],
			where: where
		});

		// Verifica se o usuário foi encontrado
		if (!usuario) {
			throw 'Usuário ou senha inválidos';
		}

		// Controi um novo JWT
		const token = new CustomJWT().create({ email: usuario.Email, usuario_id: usuario.CandidatoId, time: new Date().getTime() });

		// Monta o update do usuário que está se logando
		const update: any = {
			UltimoAcesso: new Date(),
			[source === 'app' ? 'TokenApp' : 'Token']: token
		};

		// Atualiza o token do usuário
		try {
			await usuario.updateAttributes(update);
		} catch (err) {
			console.log(err);
			throw 'Não foi possível atualizar o token';
		}

		return { CandidatoId: usuario.CandidatoId, Nome: usuario.Nome, Token: token };
	}

	/**
	 * autentica
	 *
	 * Verifica se as informações recebidas conferem
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async autentica(email: string, CandidatoId: string, token: string, source: string): Promise<boolean> {
		// Busca as informações do usuário pelo id
		const usuario: any = await this.ORM.findById(CandidatoId);

		// Verifica se o usuário foi encontrado
		if (!usuario) {
			throw 'Usuário não encontrado';
		}

		// Verifica se o email não confere
		if (usuario.Email !== email) {
			throw 'E-mail informado inválido';
		}

		// Verifica se o token é inválido de acordo com a origem
		if ((source === 'app' && usuario.TokenApp !== token) || (source !== 'app' && usuario.Token !== token)) {
			throw 'Token inválido';
		}

		// Indica que foi autenticado com sucesso
		return true;
	}

	/**
	 * nova_senha
	 *
	 * Gera uma nova senha para o usuário
	 *
	 * @author Murilo Spinoza de Arruda
	 */
	public async nova_senha(email: string): Promise<Object> {
		try {
			// Encontra o usuário pelo email
			const usuario: any = await this.ORM.findOne({
				where: { Email: email } as any,
				attributes: ['Nome', 'CandidatoId']
			});

			// Verifica se o usuário foi encontrado
			if (!usuario) {
				throw 'E-mail não encontrado';
			}

			// Gera uma nova senha como MD5(timestamp)
			let nova_senha = crypto
				.createHash('md5')
				.update(new Date().toString())
				.digest('hex');

			// Desses 32 caracteres, pega 5
			nova_senha = nova_senha.substring(0, 5);

			// Atualiza o registro do usuário com a nova senha
			await usuario.updateAttributes({
				NovaSenha: crypto
					.createHash('md5')
					.update(nova_senha)
					.digest('hex'),
				ValidadeNovaSenha: moment().add(2, 'hours') // 2 horas de validade
			});

			// Retorna os dados
			return { Nome: usuario['Nome'], NovaSenha: nova_senha };
		} catch (error) {
			throw error;
		}
	}

	public async candidatosRequisitos(habilidades: string[], pcd: boolean, escolaridades: number[]) {
		const where = <any>{ Ativo: 'S' };

		if (pcd) {
			where.Pcd = 'S';
		}

		const dados: any = await this.ORM.findAll({
			attributes: ['CandidatoId', 'Nome', 'Email', 'Nascimento', 'Pcd'],
			where,
			include: [
				{
					model: Bootstrap.model('CandidatosHabilidades').ORM,
					as: 'CandidatosHabilidades',
					attributes: ['HabilidadeId', 'Nivel'],
					where: <any>{ HabilidadeId: habilidades },
					required: true,
					include: [
						{
							model: Bootstrap.model('Habilidades').ORM,
							as: 'Habilidade',
							attributes: ['Habilidade'],
							required: true
						}
					]
				},
				{
					model: Bootstrap.model('CandidatosEscolaridades').ORM,
					as: 'CandidatosEscolaridades',
					attributes: ['CursoId', 'Termino'],
					where: <any>{ CursoId: escolaridades },
					required: false,
					include: [
						{
							model: Bootstrap.model('Cursos').ORM,
							as: 'Curso',
							attributes: ['Curso'],
							required: true
						}
					]
				}
			]
		});

		return dados;
	}
}
