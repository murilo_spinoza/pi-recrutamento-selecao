/**
 * Model para interação com a tabela: redes_sociais
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class RedesSociais extends CustomModel {
	protected _name = 'redes_sociais';

	protected _fields: IAtributos = {
		RedeSocialId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'rede_social_id'
		},
		RedeSocial: {
			type: Sequelize.STRING(50),
			allowNull: false,
			primaryKey: false,
			field: 'str_rede_social',
			unique: true
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'CandidatosRedesSociais',
			options: {
				foreignKey: 'RedeSocialId',
				as: 'CandidatosRedesSociais'
			}
		},
		{
			type: 'hasOne',
			model: 'CandidatosRedesSociais',
			options: {
				foreignKey: 'RedeSocialId',
				as: 'CandidatoRedeSocial'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
