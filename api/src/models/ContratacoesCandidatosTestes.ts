/**
 * Model para interação com a tabela: contratacoes_candidatos_testes
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class ContratacoesCandidatosTestes extends CustomModel {
	protected _name = 'contratacoes_candidatos_testes';

	protected _fields: IAtributos = {
		ContratacaoCandidatoTesteId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			field: 'cc_teste_id'
		},
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'contratacao_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'candidato_id'
		},
		Pontuacao: {
			type: Sequelize.NUMERIC(10, 2),
			allowNull: true,
			field: 'dec_pontuacao'
		},
		Status: {
			type: Sequelize.ENUM,
			values: ['A', 'R'],
			defaultValue: null,
			allowNull: true,
			field: 'flg_status'
		},
		Analise: {
			type: Sequelize.DATE,
			field: 'dt_analise'
		}
	};

	public readonly STATUS_OPCOES = {
		A: 'Aprovado',
		R: 'Reprovado'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Contratacoes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
