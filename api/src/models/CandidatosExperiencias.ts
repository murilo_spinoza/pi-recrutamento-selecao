/**
 * Model para interação com a tabela: candidatos_experiencias
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class CandidatosExperiencias extends CustomModel {
	protected _name = 'candidatos_experiencias';

	protected _fields: IAtributos = {
		CandidatoExperienciaId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'candidato_experiencia_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'candidato_id'
		},
		CargoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'cargo_id'
		},
		Empresa: {
			type: Sequelize.STRING(100),
			allowNull: false,
			primaryKey: false,
			field: 'str_empresa'
		},
		Periodo: {
			type: Sequelize.ENUM,
			values: ['M', 'T', 'I', 'N'],
			allowNull: false,
			field: 'flg_periodo'
		},
		Jornada: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'num_jornada'
		},
		Entrada: {
			type: Sequelize.DATEONLY,
			allowNull: false,
			field: 'dt_entrada'
		},
		Saida: {
			type: Sequelize.DATEONLY,
			allowNull: true,
			field: 'dt_saida'
		}
	};

	public readonly PERIODO_OPCOES = {
		M: 'Manhã',
		T: 'Tarde',
		I: 'Integral',
		N: 'Noite'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		},
		{
			type: 'belongsTo',
			model: 'Cargos',
			options: {
				foreignKey: 'CargoId',
				as: 'Cargo'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
