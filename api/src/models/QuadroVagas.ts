/**
 * Model para interação com a tabela: quadro_vagas
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class QuadroVagas extends CustomModel {
	protected _name = 'quadro_vagas';

	protected _fields: IAtributos = {
		QuadroVagaId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'quadro_vaga_id'
		},
		CargoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'cargo_id',
			unique: true
		},
		Descricao: {
			type: Sequelize.STRING(500),
			allowNull: false,
			field: 'txt_descricao'
		},
		Salario: {
			type: Sequelize.DECIMAL(8, 2),
			allowNull: false,
			field: 'dec_salario'
		},
		Vagas: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			defaultValue: 0,
			field: 'num_vagas'
		},
		Saldo: {
			type: Sequelize.INTEGER(10),
			allowNull: true,
			defaultValue: 0,
			field: 'num_saldo'
		}
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Cargos',
			options: {
				foreignKey: 'CargoId',
				as: 'Cargo'
			}
		},
		{
			type: 'hasMany',
			model: 'QuadroVagasRequisitos',
			options: {
				foreignKey: 'QuadroVagaId',
				as: 'QuadroVagasRequisitos'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}

	/**
	 * Conta quantos funcionários estão ocupando o cargo de cada vaga
	 *
	 * @author Murilo Spinoza de Arruda
	 * @since 05/2019
	 */
	public async atualizaSaldo() {
		await this.connector.query(
			'UPDATE quadro_vagas qv SET num_saldo = qv.num_vagas - (SELECT COUNT(*) FROM usuarios u WHERE u.cargo_id = qv.cargo_id)'
		);
	}
}
