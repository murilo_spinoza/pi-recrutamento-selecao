/**
 * Model para interação com a tabela: habilidades
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class Habilidades extends CustomModel {
	protected _name = 'habilidades';

	protected _fields: IAtributos = {
		HabilidadeId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			field: 'habilidade_id'
		},
		Habilidade: {
			type: Sequelize.STRING(50),
			allowNull: false,
			primaryKey: false,
			field: 'str_habilidade',
			unique: true
		}
	};

	public relations: Array<Object> = [
		{
			type: 'hasMany',
			model: 'CandidatosHabilidades',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'CandidatosHabilidades'
			}
		},
		{
			type: 'hasOne',
			model: 'CandidatosHabilidades',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'CandidatoHabilidade'
			}
		},
		{
			type: 'hasMany',
			model: 'QuadroVagasRequisitos',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'QuadroVagasRequisitos'
			}
		},
		{
			type: 'hasOne',
			model: 'QuadroVagasRequisitos',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'QuadroVagaRequisito'
			}
		},
		{
			type: 'hasMany',
			model: 'ContratacoesRequisitos',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'ContratacoesRequisitos'
			}
		},
		{
			type: 'hasOne',
			model: 'ContratacoesRequisitos',
			options: {
				foreignKey: 'HabilidadeId',
				as: 'ContratacaoRequisito'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
