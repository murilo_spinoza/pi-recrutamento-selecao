/**
 * Model para interação com a tabela: contratacoes_candidatos
 *
 * @author Murilo Spinoza de Arruda
 * @since  03/2019
 */
import * as Sequelize from 'sequelize';
import { CustomModel, IAtributos } from '../core/CustomModel';

export class ContratacoesCandidatos extends CustomModel {
	protected _name = 'contratacoes_candidatos';

	protected _timestamp = { created: true, modified: true };

	protected _fields: IAtributos = {
		ContratacaoCandidatoId: {
			type: Sequelize.INTEGER(10),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			field: 'contratacao_candidato_id'
		},
		ContratacaoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'contratacao_id'
		},
		CandidatoId: {
			type: Sequelize.INTEGER(10),
			allowNull: false,
			field: 'candidato_id'
		},
		Etapa: {
			type: Sequelize.ENUM,
			values: ['triagem', 'testes', 'entrevistas', 'aprovados'],
			defaultValue: 'triagem',
			allowNull: false,
			field: 'alias_etapa'
		},
		Finalizado: {
			type: Sequelize.ENUM,
			values: ['S', 'N'],
			defaultValue: 'N',
			allowNull: false,
			field: 'flg_finalizado'
		},
		Status: {
			type: Sequelize.ENUM,
			values: ['A', 'R'],
			defaultValue: null,
			allowNull: true,
			field: 'flg_status'
		},
		Reprova: {
			type: Sequelize.STRING(500),
			allowNull: true,
			field: 'txt_reprova'
		},
		Modified: {
			type: Sequelize.DATE,
			notLog: true,
			field: 'dt_modified'
		},
		Created: {
			type: Sequelize.DATE,
			notLog: true,
			field: 'dt_created'
		}
	};

	public readonly FINALIZADO_OPCOES = {
		S: 'Sim',
		N: 'Não'
	};

	public readonly STATUS_OPCOES = {
		A: 'Aprovado',
		R: 'Reprovado'
	};

	public relations: Array<Object> = [
		{
			type: 'belongsTo',
			model: 'Contratacoes',
			options: {
				foreignKey: 'ContratacaoId',
				as: 'Contratacao'
			}
		},
		{
			type: 'belongsTo',
			model: 'Candidatos',
			options: {
				foreignKey: 'CandidatoId',
				as: 'Candidato'
			}
		}
	];

	constructor(connector) {
		super(connector);
		this._setSequelize();
	}
}
