import { RoutesInterface } from '../interfaces/RoutesInterface';
// declare your custom routes
const declaration: Array<RoutesInterface> = [
	{
		route: '/usuarios/login',
		controller: 'Usuarios',
		function: 'login',
		method: 'post',
		is_public: true
	},
	{
		route: '/usuarios/forgot_password',
		controller: 'Usuarios',
		function: 'forgot_password',
		method: 'post',
		is_public: true
	}
];

export const routes = { routes: declaration };
