export const database = {
	development: {
		sql: {
			username: '',
			password: '',
			database: '',
			host: 'localhost',
			dialect: 'mysql',
			logging: console.log,
			timezone: '-03:00',
			define: {
				timestamps: false,
				freezeTableName: true
			}
		},

		no_sql: {
			username: '',
			password: '',
			database: '',
			host: '',
			dialect: ''
		}
	},
	stage: {
		sql: {
			username: '',
			password: null,
			database: '',
			host: '',
			dialect: '',
			logging: console.log,
			define: {
				timestamps: false,
				freezeTableName: true
			}
		}
	},
	production: {
		sql: {
			username: '',
			password: '',
			database: '',
			host: '',
			dialect: 'mysql',
			logging: false,
			timezone: '-03:00',
			define: {
				timestamps: false,
				freezeTableName: true
			}
		},
		no_sql: {
			username: '',
			password: '',
			database: '',
			host: '',
			dialect: ''
		}
	}
};
