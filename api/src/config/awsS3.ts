export const awsS3 = {
	development: {
		accessKey: '',
		secretKey: '',
		user: 's3-user',
		bucket: 'recrutamento_selecao',
		region: 'us-west-2',
		apiVersion: 'latest'
	},
	production: {
		accessKey: '',
		secretKey: '',
		user: 's3-user',
		bucket: 'recrutamento_selecao',
		region: 'us-west-2',
		apiVersion: 'latest'
	}
};
