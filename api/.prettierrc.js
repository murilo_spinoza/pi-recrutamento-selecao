module.exports = {
	singleQuote: true,
	printWidth: 140,
	tabWidth: 3,
	useTabs: true
};
